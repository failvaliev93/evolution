﻿

function main(p, dl, da, dp, hTerra) {
   var p1 = 0 ;
   var p2 = 0 ;
   var p3 = 0 ;
   var Piena = 0 ;
   var sps = 0 ;
   var Tel = 0 ;
   var a = 0 ;
   var ff = 0 ;
   var cc = 0 ;
   var a2 = 0 ;
   var ff2 = 0 ;
   var x1 = 0 ;
   var x2 = 0 ;
   var q = 0 ;
   var h = 0 ;
   var k = 0 ;
   var c1 = 0 ;
   var lato = 0 ;
   var v =[]; v = Resize(v, 26);
   var ll = 0 ;
   var aa = 0 ;
   var pp = 0 ;
   var orient = 0 ;
   var dst = 0 ;
   var dsty = 0 ;
   var nome3ds = 0 ;
   var ang = 0 ;
   var pa = 0 ;
   var trx = 0 ;
   var _try = 0 ;
   var y1 = 0 ;
   var y2 = 0 ;
   var yY = 0 ;
   p1 = Calcola(p.Parametro(1));
   p2 = Calcola(p.Parametro(2));
   p3 = Calcola(p.Parametro(3));
   Piena = Calcola(p.Parametro(4));
   sps = 18 ;
   Tel = 60 ;
   a = "#A30;0;" + p1 + ";" + dl / 2 + ";" + p2 + ";" + dl + ";" + p3 ;
   ff = eng.wSagoma(a, 1);
   cc = eng.wColoreE(p.Colore1, 90);
   if (Piena == 0 || da < Tel * 2.5 || dl < Tel * 2.5) {
      a2 = "#A30;0;" + p1 + ";" + dl / 2 + ";" + p2 + ";" + dl + ";" + p3 + ";A30;" + dl + ";" + p3 + sps + ";" + dl / 2 + ";" + p2 + sps + ";0;" + p1 + sps ;
      if (da < 200) {
         cc = eng.wColoreE(p.Colore1);
      }
      
      ff2 = eng.wSagoma(a2, 1);
      eng.wEstrudi(0, ff2.nome, da, cc, cc);
   } else {
      x1 = ff.GetPuntoY(Tel);
      x2 = ff.GetPuntoY(dl - Tel);
      a2 = "#0;" + p1 + ";" + Tel + ";" + x1 + ";" + Tel + ";" + x1 + sps + ";0;" + p1 + sps ;
      ff2 = eng.wSagoma(a2, 1);
      eng.wEstrudi(0, ff2.nome, da, cc, cc);//  montante sx
      a2 = "#" + dl - Tel + ";" + x2 + ";" + dl + ";" + p3 + ";" + dl + ";" + p3 + sps + ";" + dl - Tel + ";" + x2 + sps ;
      ff2 = eng.wSagoma(a2, 1);
      eng.wEstrudi(0, ff2.nome, da, cc, cc);//  montante dx
      a2 = "#A30;" + Tel + ";" + x1 + ";" + dl / 2 + ";" + p2 + ";" + dl - Tel + ";" + x2 + ";A30;" + dl - Tel + ";" + x2 + sps + ";" + dl / 2 + ";" + p2 + sps + ";" + Tel + ";" + x1 + sps ;
      cc = eng.wColoreE(p.Colore1);
      ff2 = eng.wSagoma(a2, 1);
      
      eng.wEstrudi(0, ff2.nome, Tel, cc, cc);//  montante centrale
      eng.wrTrasforma(0, da - Tel, 0);
      eng.wEstrudi(0, ff2.nome, Tel, cc, cc);//  montante centrale
      eng.wrChiudi();
      q = Math.floor(da / 600);
      if (q) {
         h =(da - Tel)/(q + 1);
         for (k = 1; k <= q; k ++) {
            eng.wrTrasforma(0, h * k - Tel / 2, 0);
            eng.wEstrudi(0, ff2.nome, Tel, cc, cc);//  montante centrale
            eng.wrChiudi();
         }
      }
   //  Scommentare per disegnare il vetro
   // sps=5
   // a2="#A30;" & tel & ";" & x1 & ";" & dl/2 & ";" & p2 & ";" & dl-tel & ";" & x2 & ";A30;" & dl-tel & ";" & x2+sps & ";" & dl/2 & ";" & p2+sps & ";" & tel & ";" & x1+sps
   // set ff2=wSagoma(a2,1):
   // cc=wColoreE(194)
   // wrTrasforma 0,tel,5
   // wEstrudi 0,ff2.nome,da-tel*2,cc,cc  ' montante centrale
   // wrChiudi
   }
   if (p.MA > 0) {
      c1 = eng.wColoreE(3);
      lato = p.MA % 10 ;
      //  Posizionamento della maniglia e disegno della maniglia
      v = Split(p.Parametro(9), "/");
      v = Resize(v, 26);
      ll = Calcola(v[1]);
      aa = Calcola(v[2]);
      pp = Calcola(v[3]);
      orient = Calcola(v[5]);
      orient = 1 ;
      // if orient=2 then xx=ll:ll=aa:aa=xx
      dst = Calcola(v[9]);
      dsty = Calcola(v[10]);//  posizionamento in altezza per maniglie centrate
      if (dst == 0) {
         dst = 35 ;
      }
      
      if (dsty == 0) {
         dsty = dst ;
      }
      
      nome3ds = Percorso + "\\3DS\\MANIGLIE\\" + v[4];
      ang = 0 ;
      pa = hTerra ;
      if (dst < ll / 2) {
         dst = ll / 2 ;
      }
      if (lato == 1 || lato == 4 || lato == 7) {
         trx = dst ;
      }else if(lato == 2 || lato == 5 || lato == 8) {
         trx = dl / 2 ;
      }else if(lato == 3 || lato == 6 || lato == 9) {
         trx = dl - dst ;
      }
      if (lato == 1 || lato == 2 || lato == 3) {
         _try = dsty ;
      }else if(lato == 4 || lato == 5 || lato == 6) {
         _try = da / 2 - aa / 2 ;
      } else {
         _try = da - dsty - aa ;
      }
      if (pa < 700 && pa + da > 1400) {
         _try = da / 2 - aa / 2 ;
      }else if(pa < 700) {
         _try = da - dsty - aa / 2 ;
      } else {
         _try = dsty ;
      }
      y1 = ff.GetPuntoY(trx - ll / 2);
      y2 = ff.GetPuntoY(trx + ll / 2);
      yY = ff.GetPuntoY(trx);
      ang = atn((y1 - y2)/ ll)/ 0.01745329 ;
      eng.wrTrasforma(trx, _try, yY + pp / 2, ang, 0, 0);
      eng.wrX2(nome3ds, 1, c1, ll, aa, pp, 100, "");
      eng.wrChiudi();
   }
}
