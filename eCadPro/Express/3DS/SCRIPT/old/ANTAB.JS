﻿//INC antainclude


function main(p, dl, da, dp, hTerra) {
   var fl = 0 ;
   var lato = 0 ;
   var c1 = 0 ;
   var c2 = 0 ;
   var aa = 0 ;
   var _object = 0 ;
   var nome3ds = 0 ;
   var v = 0 ;
   var c3 = 0 ;
   var bb = 0 ;
   fl = Calcola(Ex(p.Parametro(7), "/", 1));
   lato =(p.ma % 10);
   c1 = eng.wColoreE(p.Colore1);
   c2 = p.Colore2 ;
   if (c2 == "") {
      c2 = c1 ;
   } else {
      c2 = eng.wColoreE(c2);
   }
   
   aa = p.Parametro(4);// il parametro 4 mi passa le diverse altezze
   aa = aa + "|" + p.Parametro(7);// questo parametro dalla posizione 10 in poi mi passa le informazioni dei 3d delle cerniere
   _object = Ex(p.Parametro(8), "/", 5);
   nome3ds = Percorso + "\\3DS\\doors\\" + _object ;
   v = Ex(p.Parametro(8), "/", 6);
   //  msgbox (p.parametro(7))
   if (lato == 0) {
      eng.wrPannelloExtended("ALP", dl, da, dp, 0, c1, c1, c1, c1, c1, c1, "2");
   } else {
      // Cerniere fl,da,dl,aa,dp,0  '  Parte Fissa (basetta)
      Anima(fl, dl, da, dp, 0, Ex(p.Parametro(7), "/", 18));
      // dl, da, dp = dimensions in width, height, depth of the object (door in this case)
      // fl = left or right (val 1 or 2)
      // aa = string paramter containing information about positioning and information about 3d files coming from a variant
      // Cerniere fl,da,dl,aa,dp,1  '  Parte Sull'anta
      aa = p.Parametro(5);
      Ripiani(fl, aa, dl, da, dp);
      if (_object == "") {
         //  no 3d object
         eng.wrPannelloExtended("ALP", dl, da, dp, 0, c1, c1, c1, c1, c1, c1, "1");
      } else {
         c3 = eng.wColore(7);
         bb = c1 + "," + c2 + "," + c3 ;
         eng.wrX2(nome3ds, 0, bb, dl, da, dp, 3, v, 1, 0, 0, 0);
      }
      if (Left(p.Parametro(5), 1)!= "#" && fl != 0) {
         maniglia(p, dl, da, dp, c1, c2, fl, lato, hTerra);
      }
      eng.wrChiudi();
   }
}
