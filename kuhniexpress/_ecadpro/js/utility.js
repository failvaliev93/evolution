"use strict";

if (String.prototype.ex == null) {
	String.prototype.ex = function (sep, id, mode) {
		if (id > 0) {
			if (!mode) {
				return this.split(sep)[id - 1] || "";
			} else {
				var v = this.split(sep).splice(id - 1, 9999);
				if (v) return v.join(sep);
			}
		}
		return "";
	};
}
if (String.prototype.startsWith == null) {
	String.prototype.startsWith = function (needle) {
		return this.indexOf(needle) === 0;
	};
}
if (String.prototype.replaceAll == null) {
	String.prototype.replaceAll = function (searchStr, replaceStr, sensitive) {
		var str = this;
		if (sensitive) {
			if (str.indexOf(searchStr) === -1) {
				return str;
			}
			return str.replace(searchStr, replaceStr).replaceAll(searchStr, replaceStr);
		} else {
			searchStr = searchStr.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
			return str.replace(new RegExp(searchStr, 'gi'), replaceStr);
		}
	};
}

if (Array.prototype.inArray == null) {
	Array.prototype.inArray = function (comparer) {
		for (var i = 0; i < this.length; i++) {
			if (comparer(this[i])) return true;
		}
		return false;
	};
}
if (Array.prototype.pushIfNotExist == null) {
	Array.prototype.pushIfNotExist = function (element, comparer) {
		if (!this.inArray(comparer)) {
			this.push(element);
		}
	};
}

if (Array.prototype.keySort == null) {
	Array.prototype.keySort = function (keys) {

		keys = keys || {};

		var obLen = function (obj) {
			var size = 0,
				key;
			for (key in obj) {
				if (obj.hasOwnProperty(key))
					size++;
			}
			return size;
		};

		// avoiding using Object.keys because I guess did it have IE8 issues?
		// else var obIx = function(obj, ix){ return Object.keys(obj)[ix]; } or
		// whatever
		var obIx = function (obj, ix) {
			var size = 0,
				key;
			for (key in obj) {
				if (obj.hasOwnProperty(key)) {
					if (size == ix)
						return key;
					size++;
				}
			}
			return false;
		};

		var keySort = function (a, b, d) {
			d = d !== null ? d : 1;
			if (typeof a === "string" && typeof b === "string") {
				a = a.toLowerCase(); // this breaks numbers
				b = b.toLowerCase();
			}
			if (a == b)
				return 0;
			return a > b ? 1 * d : -1 * d;
		};

		var KL = obLen(keys);

		if (!KL)
			return this.sort(keySort);

		for (var k in keys) {
			// asc unless desc or skip
			keys[k] =
				keys[k] == 'desc' || keys[k] == -1 ? -1 :
					(keys[k] == 'skip' || keys[k] === 0 ? 0 :
						1);
		}

		this.sort(function (a, b) {
			var sorted = 0,
				ix = 0;

			while (sorted === 0 && ix < KL) {
				var k = obIx(keys, ix);
				if (k) {
					var dir = keys[k];
					sorted = keySort(a[k], b[k], dir);
					ix++;
				}
			}
			return sorted;
		});
		return this;
	};
	/*
	 var obja = [
	 {USER:"bob",   SCORE:2000, TIME:32, AGE:16, COUNTRY:"US"},
	 {USER:"jane",  SCORE:4000, TIME:35, AGE:16, COUNTRY:"DE"},
	 {USER:"tim",   SCORE:1000, TIME:30, AGE:17, COUNTRY:"UK"},
	 {USER:"mary",  SCORE:1500, TIME:31, AGE:19, COUNTRY:"PL"},
	 {USER:"joe",   SCORE:2500, TIME:33, AGE:18, COUNTRY:"US"},
	 {USER:"sally", SCORE:2000, TIME:30, AGE:16, COUNTRY:"CA"},
	 {USER:"yuri",  SCORE:3000, TIME:34, AGE:19, COUNTRY:"RU"},
	 {USER:"anita", SCORE:2500, TIME:32, AGE:17, COUNTRY:"LV"},
	 {USER:"mark",  SCORE:2000, TIME:30, AGE:18, COUNTRY:"DE"},
	 {USER:"amy",   SCORE:1500, TIME:29, AGE:19, COUNTRY:"UK"}
	 ];
	 var sorto = {
	   SCORE:"desc",TIME:"asc", AGE:"asc"
	 };
	 obja.keySort(sorto);
	 */
}

if (Array.prototype.remove == null) {
	Array.prototype.remove = function () {
		var args = Array.apply(null, arguments);
		var indices = [];
		for (var i = 0; i < args.length; i++) {
			var arg = args[i];
			var index = this.indexOf(arg);
			while (index > -1) {
				indices.push(index);
				index = this.indexOf(arg, index + 1);
			}
		}
		indices.sort();
		for (var i = 0; i < indices.length; i++) {
			var index = indices[i] - i;
			this.splice(index, 1);
		}
	}
	/*
	var arr = ["cat", "dog", "bear", "cat", "bird", "dog", "dog"];
	arr.remove("cat", "dog");
	=> ["bear", "bird"]
	*/
}

Object.defineProperty(Object.prototype, "getProp", {
	value: function (prop) {
		var key, self = this;
		for (key in self) {
			if (key.toLowerCase() === prop.toLowerCase()) {
				return self[key];
			}
		}
	},
	enumerable: false
});
Object.defineProperty(Object.prototype, "setProp", {
	value: function (prop, val) {
		var key, self = this;
		var found = false;
		if (Object.keys(self).length > 0) {
			for (key in self) {
				if (key.toLowerCase() === prop.toLowerCase()) {
					//set existing property
					found = true;
					self[key] = val;
					break;
				}
			}
		}

		if (!found) {
			//if the property was not found, create it
			self[prop] = val;
		}

		return val;
	},
	enumerable: false
});

function toLowerCaseKey(obj) {
  var keys = Object.keys(obj);
  var newobj = {};
  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    newobj[key.toLowerCase()] = obj[key];
  }
  return newobj;
}
