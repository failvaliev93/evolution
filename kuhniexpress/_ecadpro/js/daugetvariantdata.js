"use strict";

var nCurrDir = null;
var nDesVar = null;
var nPercorso = null;
var objVar = null;
var varname = null;
var old3cadvalue = null;

function getParamWindowsHref(a) {
	a = a || window.location.search.substr(1).split("&").concat(window.location.hash.substr(1).split("&"));
	if (typeof a === "string")
		a = a.split("#").join("&").split("&");
	// se non ci sono valori, restituisce un oggetto vuoto
	if (!a) return {};
	var b = {};
	for (var i = 0; i < a.length; ++i) {
		// ottieni array con chiave/valore
		var p = a[i].split("=");
		// se non c'è valore, ignorare il parametro
		if (p.length !== 2) continue;
		// aggiunge la proprietà chiave all'oggetto restituito
		// con il valore decodificato, sostituendo `+` con ` `
		// per accettare URL codificati con `+` invece di `%20`
		b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
	}
	// restituisce l'oggetto creato
	return b;
}

function checkUndef(myvarname, myVar) {
	if (typeof myVar === "undefined") {
		alert(myvarname + " is NOT defined!");
		return false;
	} else {
		return true;
	}
}

function VarianteClass(vName, varStr) {
	try {
		var sep = "<---->";
		var sep2 = String.fromCharCode(18);
		this.cod = vName;
		var str = varStr.split(sep);
		// problema da verificare com tiz (se vuoto ritorna 0, quindi falso...)
		this.isvalid = true;
		if (str[0] === "0") {
			this.isvalid = false;
		}
    //New
		var codListForType = str[1].split(";");
		codListForType.length = 4;
		this.validoptions = ",,,";
		this.deprecatedoptions = ",,,";
		this.outdatedoptions = ",,,";
		if (codListForType[0] !== null) {
			this.validoptions = ",," + codListForType[0] + ",";
		}
		if (codListForType[1] !== null) {
			this.deprecatedoptions = ",," + codListForType[1] + ",";
		}
		if (codListForType[2] !== null) {
			this.outdatedoptions = ",," + codListForType[2] + ",";
		}
    //New
		this.des = str[2];
		this.classe = str[3];
		this.codalias = str[4];
		this.colphoto = 0;
		this.pathphoto = "";

		if (str[3].substring(0, 1) === "^") {
			// need to create all classe cases
			var p = str[3].split(";")[5];
			p = nCurrDir + "/" + nPercorso + p.replace("#", "/");
			this.colphoto = str[3].split(";")[4].split(",")[0];
			this.pathphoto = p;
		}
		// this.strrows=str[5];

		var rs = str[5].split(sep2);
		this.rowcount = rs.length;
		var i = 0;
		this.rowbycod = [];
		this.rowbyid = [];
		for (i = 0; i < rs.length; i++) {
			if (rs[i].split(",").length > 0) {
				var r = rs[i].split(",");
				this.rowbycod[r[0]] = new RowVariante();
				this.rowbycod[r[0]].cod = r[0];
				this.rowbycod[r[0]].des = r[1];
				if (r[this.colphoto - 1] !== "" && r[this.colphoto - 1] !== null && this.colphoto > 1) {
					this.rowbycod[r[0]].photopreview = this.pathphoto + r[this.colphoto - 1];
				}
				if (this.validoptions.indexOf("," + r[0] + ",") > 0) {
					this.rowbycod[r[0]].type = 1;
				}
				if (this.deprecatedoptions.indexOf("," + r[0] + ",") > 0) {
					this.rowbycod[r[0]].type = 2;
				}
				if (this.outdatedoptions.indexOf("," + r[0] + ",") > 0) {
					this.rowbycod[r[0]].type = 3;
				}
				var c = 0;
				this.rowbycod[r[0]].c = {};
				for (c = 0; c <= r.length; c++) {
					this.rowbycod[r[0]].c[c + 1] = r[c];
				}
				this.rowbyid[i] = new RowVariante();
				this.rowbyid[i] = this.rowbycod[r[0]];
			}
		}
	} catch (err) {
		alert(err.message);
	}
}
function RowVariante() {
	this.cod = "";
	this.des = "";
	this.photopreview = "";
	this.type = 0; // 0=invalid /1=valid / 2=deprecated / 3=outdated
	this.c = {};
  //New
	this.dataFilter = {};
	this.rgbpreview = "";
  //New
}

