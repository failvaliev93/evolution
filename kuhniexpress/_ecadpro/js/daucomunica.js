var APP = window.APP || (window.APP = {});

APP._isinternalflag = undefined;

APP.isInternal = function () {
	if (!APP._isinternalflag) {
		if (window.external) {
			APP._isinternalflag = (typeof window.external.comunica2 != 'undefined') ? 2 : 0;
		} else {
			APP._isinternalflag = 1;
		}
	}
	return APP._isinternalflag > 1;
};

APP.cmcallback = [];
APP.ncallback = -1;

APP.cm = function (s, callback) {
	// anche nei log, cos� si vede la comunicazione!
	if (!APP._isinternalflag) { APP.isInternal(); }
	if (APP._isinternalflag == 2) {
		v = s.split(':');
		if (callback) {
			// un po' naif.... per evitare sovrapposizioni essendo totalmente asincrono:
			// VB non gestisce thread, e le callback sono gestite con un timer. non si sa quando vengono eseguite:
			// la maggior parte non usa le callback e per quelle che lo usano visto che mi serve una funzione globale ne setto 5 a rotazione...
			APP.ncallback++; if (APP.ncallback > 11) APP.ncallback = 0;
			APP.cmcallback[APP.ncallback] = callback;
			if (APP.ncallback == 0) window.external.comunica2(s, function (res) { APP.cmcallback[0](res); });
			if (APP.ncallback == 1) window.external.comunica2(s, function (res) { APP.cmcallback[1](res); });
			if (APP.ncallback == 2) window.external.comunica2(s, function (res) { APP.cmcallback[2](res); });
			if (APP.ncallback == 3) window.external.comunica2(s, function (res) { APP.cmcallback[3](res); });
			if (APP.ncallback == 4) window.external.comunica2(s, function (res) { APP.cmcallback[4](res); });
			if (APP.ncallback == 5) window.external.comunica2(s, function (res) { APP.cmcallback[5](res); });
			if (APP.ncallback == 6) window.external.comunica2(s, function (res) { APP.cmcallback[6](res); });
			if (APP.ncallback == 7) window.external.comunica2(s, function (res) { APP.cmcallback[7](res); });
			if (APP.ncallback == 8) window.external.comunica2(s, function (res) { APP.cmcallback[8](res); });
			if (APP.ncallback == 9) window.external.comunica2(s, function (res) { APP.cmcallback[9](res); });
			if (APP.ncallback == 10) window.external.comunica2(s, function (res) { APP.cmcallback[10](res); });
		} else {
			window.external.comunica2(s, "");
		}
	} else {
		// missing code for now we must decide how to proceed.
		//alert('err');
		console.log(s);
		if (callback) callback(s);
	}
};
