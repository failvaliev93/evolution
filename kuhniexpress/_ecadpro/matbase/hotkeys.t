
[HotKeys] 
01:  F1       :  Varia Articolo
02:  F2       :  Varia Viste
03:  Ctrl +F1 :  Vista Fronte 1
04:  Ctrl +F2 :  Vista PRofilo 2
05:  Ctrl +F3 :  Vista Pianta 3
06:  Ctrl +F4 :  Vista Assonometrica 4
07:  Ctrl +F5 :  Vista Assonometrica 5
08:  Ctrl +M  :  Sposta 
08:  Ctrl +DX :  Sposta 
09:  Ctrl +T  :  Calcola Top
10:  Ctrl +S  :  Salva Ordine
12:  Shift+F3 :  Tracciamento 
13:  F4       :  Applica
11:  F5       :  Stampa Ordine
14:  Alt  +F1 :  HotKeys
15:  Alt  +F2 :  Unused 
16:  Alt  +F3 :  Testata
17:  Ctrl +F  :  Cerca Articolo
18:  F3       :  MENU ARTICOLI
19:  Ctrl +A  :  Crea Macro
20:  Ctrl +D  :  Dividi Macro
21:  Ctrl +Z  :  UNDO
21:  Alt+Back :  Unused
27:  F6       :  Editor Associazioni
22:  F7       :  Editor Varianti
26:  F8       :  Editor Rette
25:  F9       :  Dump Variabili
23:  F10      :  Editor Regola
23:  Ctrl+R   :               
24:  F11      :  Editor Grafica
24:  Ctrl+G   :                

28:  Ctrl +L  :  Unused
29:  Ctrl +J  :  Unused
30:  Ctrl +I  :  Crea miniatura
99:  Ctrl +H  :  Help ShortCuts
99:  Shift+F1 :                

