[Pipeline]
0-Top Quality/Performance
1-Top Quality Safe Mode
2-NVIDIA Optimized
3-ATI Optimized
4-INTEL Optimized
5-HARDWARE T&L / VIDEORAM 
6-HARDWARE T&L / SYSTEMRAM 
7-SOFTWARE T&L / VIDEORAM 
8-SOFTWARE T&L / SYSTEMRAM
9-PURE-HARDWARE T&L / VIDEORAM 

[Parameters]
0-L. Bias     ,0.02, 0.01,0.1
1-L. Bias2    ,0.02, 0.01,0.1
2-L. Color    ,1.00, 0.20,1.00
3-L. Color 2  ,1.00, 0.20,1.00
