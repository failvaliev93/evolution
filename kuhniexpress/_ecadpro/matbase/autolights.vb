option explicit

Sub main(mi_x,mi_y,mi_z,ma_x,ma_y,ma_z)
	' scene dimensions given as function parameters
	if mi_x < -20000 then mi_x = -20000
	if mi_z < -20000 then mi_z = -20000
	if ma_x > 20000 then ma_x = 20000
	if ma_z > 20000 then ma_z = 20000
	dim ris,gx,gz,intensity,temperature	
	dim xs		
	xs = leggiFileTestoU(getWorkDir & "\_ecadpro\matbase\tradAutoLights.txt")	
	if xs = "" then xs = "Delete User Lights and put auto lights?"	
	ris = msgbox (xs,vbYesNo)

	if ris = vbYes then 
		deng.CleanUserLights ' delete all the lights in the scene
		intensity = 30
		temperature = 5500 '5500 = white
		if ma_x-mi_x<3000 then 
		    gx=3000-(ma_x-mi_x)/2
		    mi_x=mi_x-gx
			ma_x=ma_x+gx
		end if 
		if ma_z-mi_z<3000 then 
		    gz=3000-(ma_z-mi_z)/2
		    mi_z=mi_z-gz
			ma_z=ma_z+gz
		end if 
		gx=(ma_x-mi_x)/3.5
		gz=(ma_z-mi_z)/3.5
		
		' Questo modello posiziona 5 Luci: quelle più vicine all'oggeto alte e le distanti basse
		deng.UserLight mi_x+gx, 3500, mi_z+gz, intensity, temperature , 0.2,0.1,0.05
		deng.UserLight ma_x-gx, 3500, mi_z+gz, intensity, temperature , 0.2,0.1,0.05
		deng.UserLight mi_x+gx,  500, ma_z-gz, intensity, temperature , 0.2,0.1,0.05
		deng.UserLight ma_x-gx,  500, ma_z-gz, intensity, temperature , 0.2,0.1,0.05
		deng.UserLight (mi_x+ma_x)/2, 1800, (ma_z+mi_z)/2, intensity*2, temperature , 0.2,0.1,0.05
		
		'Possibiliy to access at the boxes on the order
		' AMB.nbox    AMB.box(i)
		
	end if
End Sub