[MAIN]

#LET ID=0
#LETS DESC$(ID)=ARG VIEW
#LETS COMMAND$(ID)= #ARGVIEW  
#LETS MULTIPLEVIEW$(ID)= ARG

#LET ID=1
#LETS DESC$(ID)=WALL TOP VIEW
#LETS COMMAND$(ID)=#WALLTOPVIEW   
#LETS MULTIPLEVIEW$(ID)= TOPWALL

#LET ID=2
#LETS DESC$(ID)=WALL FRONT VIEW
#LETS COMMAND$(ID)=#WALLFRONTVIEW
#LETS MULTIPLEVIEW$(ID)= WALL

 
[ARGVIEW]

VLIBERA1 = ARG V4 /Z1 /F2.0 /LS-1 /BR /BI /QUOTE*1:10* /XBOXTARGA4
VLIBERA2 = ARG V5 /Z1 /F2.0 /LS-1 /BR /BI /QUOTE*1:10* /XBOXTARGA4


[WALLTOPVIEW]

#LETS layerBasi = 2:200:
#LETS layerPensili = 2:1700:::2800
#LETS layerPianta = $(layerPensili)*$(layerBasi)

VPIANTA = ARG V3 /AS /SL-1.5 /BI /QTPIANTA150;50;2 /QW$(layerpianta) /WQDIST300*150 /QUOTE*2:0*

[WALLFRONTVIEW]

//LAYERS DI QUOTATURA
//Вертикальная разметка изделий
#LETS qBasiVerticali = 0:::0:1  
//Горизонтально напольные
#LETS layerBasi = 2:200:  
//Горизонтально навесные
#LETS layerPensili = 2:1700::1  
//Горизонтально окно
#LETS foriPareteOriz = 1:: 
//Вертикально окно
#LETS foriPareteVert = 5:::1:0 

//AUTOREMOVE toglie i duplicati
//0 non toglierli
//1 non toglierli ma scrivi il nome del layer su una sola sezione
//3 toglili e accorpa eventualmente i nomi layer
#LETS togliDuplicat = AUTOREMOVE:3


#LETS quotewalls=/QW$(qBasiVerticali)*$(layerBasi)*$(layerPensili)*$(foriPareteOriz)*$(foriPareteVert)*$(togliDuplicat)

#IF $(IDBOXW)
	 VFRONTE{$(COUNT)+1} = ARG V0 $(quotewalls) /AS /Z1 /BI /SL-1.5 /XBOXTARGA4
#ENDIF

