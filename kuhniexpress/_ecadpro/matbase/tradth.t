C00002	Set catalogue:
C00003	Starting folder not valid: Verify path in: %1ecadpro.ini
C00004	No database has been set in:
C00005	Graphic engine loading error!
C00006	Cannot open database:
C00007	No catalogue can be selected
C00009	Tab:
C00010	Application error: %1 %2 - %3 Continue log?
C00011	Error: %1 - %2 %3: %4 Continue?
C00019	Cannot set Printer
C00020	Models PTO
C00021	Start Compile
C00022	Compile: (%2 of %3) %1
C00023	Rule Compilation
C00026	Bar Compilation!
C00028	Generate Bars
C00030	00-Standard
C00031	01-Lines Only
C00032	06- No Graphic
C00033	08-Line
C00034	09-Text
C00035	10-Text by Layout
C00036	11-Object
C00037	19-Frame
C00038	20-Rounded Surface
C00039	21-Solid id Rotation
C00040	22-Corner Strut
C00041	23-Interference
C00042	24-Subtract
C00043	25-Frame on WH plane
C00044	26-Frame on HD plane
C00045	27-Dimension
C00046	28-Light
C00047	40-Wall
C00048	41-Dragotech Object
C00049	98-Push
C00050	99-Pop
C00051	Object type
C00052	,(New Colour)
C00053	Base Colours
C00054	Door Management
C00056	Error:
C00057	Continue log
C00058	Total=
C00061	Rules
C00062	User not allowed to edit rules!
C00063	Enter HW protection to continue
C00068	Type
C00071	Variation
C00075	Shape:
C00076	1-Yes
C00077	0-No
C00078	Data
C00079	Confirm Print
C00080	Se&arch
C00082	Are you sure you want to clean all?
C00083	Question
C00084	Dxf importation
C00085	DXF Command
C00086	Imported DXF
C00087	Are you sure you want to delete DXF?
C00088	Are you sure you want to clean active context?
C00089	point is already part of a ceiling
C00090	Notice
C00091	point is already part of a floor
C00092	DESCRIPTION=Ceiling
C00093	DESCRIPTION=Floor
C00094	Cannot generate ceiling without walls
C00095	Cannot generate floor without walls
C00096	No adjacent wall
C00097	No walls can be selected
C00098	Chord distance...
C00099	Distance
C00100	Set angle
C00101	Fixed corner
C00102	Angle 1:
C00103	Set angle
C00104	Angle 2:
C00105	Move:
C00106	Set value
C00108	Set thickness
C00109	Length
C00110	Set length
C00111	Position:
C00112	Set coordinates
C00113	Set coordinates
C00114	Dimensions:
C00115	Set X and Y
C00116	Edit text %1
C00124	Press space bar to edit dimension
C00125	Drag to adjust the size or move ... use the space bar to edit from the keyboard
C00127	Added Floor %1
C00128	Added ceiling %1
C00129	End of edit
C00130	Automatic Saving
C00132	Delete %1 wall
C00133	%1 item deleted
C00134	Delete %1 area
C00135	Delete %1 light
C00136	Added wall %1
C00137	Added element %1
C00138	Added light %1
C00139	New Image
C00140	Invalid image file format
C00141	New Line
C00142	New rectangle
C00143	New magnetic point
C00144	New Light
C00145	Ceiling
C00146	New Ceiling
C00147	Floor
C00148	New Floor
C00149	New Wall
C00151	Clean Later
C00152	Saved Project
C00153	Uploaded Project
C00154	Layers:
C00155	%1 elements
C00157	Show rule
C00158	Surfaces…
C00161	Delete
C00163	Split
C00166	Detach at the beginning
C00167	Detach at the end
C00169	Join at the beginning
C00170	Join at the end
C00172	Straighten
C00173	Set chord distance
C00175	Cancel
C00179	Center
C00181	Select All
C00183	Clean
C00185	Delete DXF
C00187	Calculate floors
C00189	Calculate ceilings
C00190	DXF Layers
C00191	Select Area
C00192	Edit walls...
C00194	Add Point
C00195	Delete point
C00197	Delete all
C00198	Background
C00199	Grid
C00200	Rulers
C00201	Floors
C00202	Ceilings
C00203	Points
C00204	Walls
C00205	Doors and Windows
C00206	Footprint
C00207	Lights
C00208	Dimensions of Window and Door Frames
C00209	Free Dimensions
C00210	Dimensions of Walls
C00213	Delete Plan
C00214	Open an existing plan
C00215	Save Layout with name
C00216	Undo
C00217	Redo
C00218	History
C00219	Move up
C00220	Move down
C00221	Move to the Right
C00222	Move to the Left
C00223	Configuration
C00224	Reset Plan?
C00225	Name of file to be Saved
C00226	FileName
C00227	No history exists
C00228	Move up
C00229	Move down
C00230	Enter line
C00231	Delete line
C00232	Undo
C00233	Movement
C00234	Move Objects
C00235	Rotation
C00236	Plots
C00237	Remarks
C00238	Dimensions
C00239	Clip Planes
C00240	Materials
C00241	Self Align
C00242	Re-create drawing
C00243	Graphics Settings
C00244	Set current View
C00245	New
C00246	New Order
C00247	Open
C00248	Open existing order
C00249	Save
C00250	Save Order
C00251	Print
C00252	Print Order
C00253	Item
C00254	Edit Component
C00255	Replace
C00256	Replace selected component
C00257	Box
C00258	Search Box
C00259	Previous
C00260	Next
C00261	Frame All
C00262	Zoom in Window
C00263	Zoom +
C00264	Zoom -
C00265	Save JPG
C00266	Apply
C00267	Change Variations
C00268	Countertop
C00269	Joins the Countertops
C00270	Catalogues List
C00271	Program settings
C00272	Information…
C00274	EnvironmentColours
C00275	Confirm deletion of client destination?
C00276	enter a client code!!
C00277	Confirm Client deletion?
C00278	Assign an id between 1 and 999 to destination!
C00279	Attention! Order not saved
C00282	Confirm deletion of %1
C00283	Confirm entry of: %1
C00286	Close program?
C00287	Reset Order
C00288	Save Database
C00289	Confirm Deletion of: %1
C00290	Attention: Delete all clone nodes?
C00291	Miniature Creation :
C00292	Attention Order %1 Already Exists: Upload?
C00293	neutritems.typ='
C00294	Navigation:
C00295	Colour Image Generation Confirmation
C00296	Run Script
C00298	Save order : %1
C00305	Reading Models
C00309	Import XML:
C00311	Create Complete Drawing
C00312	Dimensions
C00314	Image Saved:
C00317	Create Database
C00318	A code must be assigned to the configuration
C00319	Configuration Name
C00321	Models Loading:
C00322	Model Selection
C00324	Configuration Loading:
C00325	Set apply values
C00326	View Name
C00327	Views
C00328	Description
C00329	Printer settings
C00330	Set Printer
C00331	Preview
C00332	Print Preview
C00333	Page
C00334	Next page
C00335	Previous page
C00337	Name assigned to Printout
C00338	Deleting file: %1
C00339	Cannot load script
C00341	Cannot add objects to script
C00342	Page
C00343	:Continue
C00344	Apply in progress
C00345	Update Countertop
C00346	Rule edits not allowed!
C00347	Confirm Deletion of selected orders?
C00348	Save folder XML files
C00352	Ascii
C00353	Show ASCII file
C00354	Variants
C00355	Move line up
C00356	Move line down
C00357	Save groups?
C00358	Group Name
C00359	Attention: file name cannot contain character control
C00360	group already exists
C00361	Do you really want to Delete %1
C00363	Inches
C00367	DXFLayerImport
C00369	Select a file!
C00370	Confirm deletion of:
C00371	Move from end
C00372	Move from beginning
C00373	Value (cm)
C00374	Value (Inches)
C00375	Value (mm)
C00376	Move next wall
C00377	Move previous wall
C00378	Value (degrees)
C00379	Enter HW protection to continue!
C00381	Factors:
C00382	%1 - %2 Error
C00385	Neutr.Item:
C00386	Model:
C00387	Item + Model:
C00388	Only for neutral codes
C00389	Confirm RH Side addition
C00390	Set variants to enabled
C00391	Set unsuitable variants
C00392	Set obsolete variations
C00393	proceed with creation of automatic rules
C00395	Item Code
C00396	confirm deletion of:
C00397	Attention! Do you wish to proceed with articles calculation
C00398	Calculate
C00399	Alias
C00401	Order
C00402	Force
C00403	Default
C00404	Code
C00405	Rh Code
C00406	Cannot delete main group!!
C00407	Attention: Do you really wish to copy model settings?
C00408	Copy model
C00409	Reason of NS Items
C00410	001-Base unit
C00412	C&alculate
C00413	Co&py and move
C00414	Group Code
C00415	Attention! Group '%1' already exists
C00416	Group entry error.
C00417	Attention! Cannot delete group '%1' as long as it contains items!
C00418	Classification
C00419	No Classification
C00424	Comments=
C00429	PrevLink=
C00430	NextLink=
C00433	MinimumDim.= 5, 5, 5
C00434	DefaultDim.= 0, 0, 0
C00435	BlockDim.=0,0,0
C00436	SpecialDim.=0,0,0
C00438	Link3=,
C00439	Spec=
C00440	NuovoBox=1
C00442	-,Management,,,Management Software
C00443	Code:
C00444	Proceed
C00445	Interrupt
C00447	there are no generation rules!
C00448	Compile Bars?
C00450	Start Generator
C00451	First
C00452	Last
C00453	Enter group code.
C00454	Code Generator
C00455	Generator already present
C00456	Remove generator [ %1 ]?
C00457	Edit generator?
C00458	Create new group?
C00459	Generator
C00463	tip=
C00465	Are you sure you want to change the generator?
C00466	Are you sure you want to change generator type
C00471	Process
C00472	Enter code
C00473	Save Group
C00474	Enter at least one generator!
C00476	delete group [%1]
C00477	Confirm processing of selected generators?
C00478	Error: %1 - %2
C00479	Cannot save new record.
C00481	%1 - %2 Error.
C00484	Functions
C00486	Macro Name
C00487	enter macro
C00488	Delete: %1 ?
C00490	Save: %1 ?
C00491	Compile
C00492	Compile rules
C00493	New group
C00494	Create a new Group
C00495	Rule file Name
C00496	file already exists
C00499	MinimumDim.= 100, 100, 100
C00500	Confirm Rule compilation?
C00501	Add group?
C00503	Rule Editor:
C00504	Rule Editor: ????
C00505	Rule has Changed: Save?
C00506	Comments
C00507	Photo
C00510	Prev.Link
C00511	NextLink
C00512	Width Table
C00513	Height Table
C00514	Depth Table
C00515	HeightShift Table
C00516	HeightDim Table
C00517	WidthShift Table
C00518	WidthDim. Table
C00520	Prev. LH Link
C00521	Next LH Link
C00523	New Box
C00524	Minimum
C00525	Maximum
C00526	Fixed Size
C00527	No Spec.
C00529	Link=
C00531	MinimumDim.=
C00532	MaximumDim.=
C00533	DefaultDim.=
C00534	BlockDim.=
C00535	SpecialDim.=
C00536	Link3=
C00540	[%1 - %2] error. Cannot save.
C00541	Rule Registration
C00542	images only in the current sub-folder!
C00543	Draw
C00544	Shape Name
C00545	Confirm deletion:
C00546	Layout
C00547	Front
C00548	Profile
C00549	Hood
C00550	Hood Front
C00551	Hood Profile
C00552	Modified shape! Save?
C00553	Attention!
C00554	Shape Already Exists! Overwrite?
C00555	Update Items
C00556	Update Neutral Items
C00557	Update Variations
C00558	Update Tables
C00559	Update Types and Colours
C00560	Create DB Words
C00562	Read Subfolders..
C00563	Folder:
C00564	File:
C00565	Convert:
C00566	Read Files…
C00567	Compact:
C00568	No executable file associated to:
C00569	Rules  //
C00570	Create BDR
C00571	Write Rules
C00572	Attention: File missing:
C00573	Rule Importation
C00574	Exportation Folder
C00576	Export:
C00577	not found:
C00578	Importation:
C00579	Colour:
C00580	Parameter
C00581	does not exist
C00582	Type:
C00583	Shapes:
C00584	generator not imported!!
C00586	Files List Reading
C00587	Dest. folder error
C00588	Compress:
C00589	Compress: Giornale.bin
C00590	Cannot save DAT files: ADO control missing!
C00591	Save DB:
C00592	Table Saving Error:
C00593	Search
C00597	Copy
C00598	Copy a Typology
C00599	List
C00600	Formula-based bill of materials
C00601	Price list
C00602	Formula-based Price List
C00603	Show Variation List
C00604	Typology Number (1-999)
C00605	Confirm typology delete?
C00606	Save typology?
C00607	Compile variants!
C00608	Enter a new column
C00609	Delete last column
C00610	Arrange increasing
C00611	Arrange decreasing
C00612	Access colours
C00613	Set an image
C00614	FrontPage
C00615	HTML Editor
C00617	Typical Description
C00618	Description language 1
C00619	Description language 2
C00620	Description language 3
C00621	Description language 4
C00622	Description language 5
C00623	Table Loading:
C00624	Load Variant lines again?
C00625	Save variations?
C00626	Confirm %1 deletion
C00627	Confirm compilation of all variations?
C00628	%1 group has changed! Save?
C00630	Error while saving!
C00631	Restore previous situation?
C00632	Line
C00633	Column Title
C00634	Set columns
C00635	No match found!
C00636	No other match exist!
C00638	Search
C00639	%1 - %2 %3 Error
C00640	StartingCorner
C00641	angI
C00642	EndCorner
C00643	angF
C00653	Colours:
C00654	Points:
C00655	Triangles:
C00658	OpenType
C00660	199,free,x,6,0,0,0,$L,$A,$P,181,181
C00661	999,Internal use,x,6,0,0,0,$W,$H,$D
C00662	MaxPrintPages
C00663	FormatDescriptionLineRule
C00664	DeleteVariantsHiddenByParameters
C00666	CheckConnectedBoxes
C00667	LinkHeight
C00668	TopRecalcForceBatch
C00673	BoxCode
C00674	RegroupsMacroBox
C00675	SpecialMacroPrice
C00676	GroupItemsCodesIni
C00678	ArrangePricesL1L3
C00679	SumFatherCodeWeights
C00681	RestoreSent
C00694	GeneralMacroCode
C00699	0,No
C00700	1,Yes
C00702	commands_
C00705	newValidity
C00711	Error: ADD
C00712	Line :
C00713	Error: RUN
C00714	Script Error:
C00715	Type does not match!
C00721	DauVBS
C00724	Load
C00725	Cannot include: %1 in %2
C00726	File not found:
C00728	Too Many Parameters for:
C00731	Cannot duplicate an internal Box!
C00732	Position occupied. Box shall not be created
C00739	Length
C00740	Init. Cut
C00741	Fin. Cut
C00742	Nr.
C00743	Countertop, manual. If you continue with recalculation all changes will be lost. Continue?
C00744	angl=
C00745	angf=
C00746	Loading file:
C00747	Cannot read: %1 (inconsistent format)
C00749	Saving File:
C00751	Empty  Line
C00752	Attention: this process adds instead of removing
C00753	Invalid Shape
C00754	Number of Areas=
C00756	Shapes do not join
C00757	0-Shapes do not join
C00759	shelf
C00760	Serial number=
C00762	OrderDate=
C00763	Client=
C00764	Bar=
C00765	Loading=
C00768	Client
C00769	Loading
C00770	Top transf. ERROR: BILL OF MATERIALS OF %1 VAR. %2 IS EMPTY
C00775	Lav=
C00776	Creation error:
C00777	Doors and windows: same type: POS =
C00778	View
C00779	CE control not managed
C00780	LI control not managed
C00781	BX control not managed
C00784	BoxType:
C00785	XLD writing error
C00786	Error: too many links
C00787	Attention! All links are used
C00788	Corresponding Code
C00789	Width
C00790	H.
C00791	Depth
C00792	Exception Error: %1
C00793	Verify exceptions
C00794	Box:
C00795	Item:
C00796	Position occupied
C00802	%1 Error (%2) in %3 procedure
C00803	items.model like '%'
C00804	items.model='
C00807	Invalid File.
C00819	APPLYFINISH()  finish no.
C00820	Error
C00821	log.txt
C00836	IDPad:
C00837	Ori:
C00838	Col:
C00858	Layer created...
C00859	Line processing :
C00864	Too Many Parameters!!
C00866	edit Table:
C00868	StProc
C00870	Copy Table
C00871	Table missing
C00881	Static part:
C00894	x +
C00895	y +
C00896	Variant|Option|Description
C00897	C|Variant|Option
C00898	Type|Side|PL|PA|PP|DL|DA|DP|AXY|AXZ|AYZ|C1|C2|F1|Ma|Shape|Object|Par1|Par2|Par3|Par4|Par5|Par6|Par7|Par8|Par9|Par10|Code|Variations|Lev.|Discounts|Nt1|Nt2|Nt3|Nt4|Nt5
C00899	Titles|Description
C00900	Code|Description
C00901	Countertop Type|Incidence type|Cut Type|Description|View|Transfer|Booleans|Edges|Mach
C00902	L|Variant|Type|Alias|Validity|Description|Column Code|Command|Values|Minimum|Maximum||C1|C2
C00903	Description|Option
C00904	|Code|Description|Formulas
C00905	V|S|O|Code|Option
C00906	En.|Variant|From Option|To Option
C00907	Item|Option
C00908	Code|Option|.
C00909	Code: %1      Model: %2     Category: %3
C00910	Confirm Company Backup: %1
C00911	Confirm Company Data Restore: %1
C00912	Database Copy in progress...
C00913	Database Restore in progress...
C00914	Enter an image class!
C00915	Error: file DauSupport.DLL not found!
C00919	Bar
C00920	Classifications
C00921	Key Expired
C00922	Thickness
C00923	Clean Before
C00924	Saving Previews
C00925	Saving Walls
C00926	Salving Files
C00929	Folder
C00930	Set up data correctly (values greater then zero)
C00931	Attention
C00932	Set up data correctly
C00934	Code
C00935	TipFM
C00936	Rule
C00938	Automatics Generetor
C00947	Verify if there are double primary keys.
C00948	New Record
C00952	Confirm deletion:
C00953	Save changes?
C00955	Enter an image class!
C00956	images
C00957	Colours
C00961	Error:
C00966	Catalogues: %1 missing! Cannot load the file!
C00967	Retry
C00968	Catalogue %1 missing! Impossible to continue!
C00969	Attention! No catalogue available
C00970	No Catalogue available offline: try to connect to internet!
C00971	Cannot enter folder:
C00972	Types
C00973	Attention: The '%1' Already exists! Delete?
C00974	Archetypes
C00975	Skin Library Version 0 (Green)
C00976	Enabled:
C00977	Delete all divisions?
C00978	Delete current division?
C00979	Set
C00980	Horizontal Lenth
C00981	Vertical Length
C00982	Create predefined room
C00983	Before Boolean Calulation
C00984	Read
C00985	Compact
C00986	Compile
C00987	Cannot calculate intersection
C00988	Size too big for the junction
C00989	Radius
C00990	Set radius
C00991	New element
C00992	Drawing
C00993	Catalogue change not allowed: change initial parameters!
C00994	Confirm change to catalogue: %1?
C00995	Restart program to apply changes
C00996	Rows,Columns,RowsDist,ColumnsDist
C00997	Restart program to continue
C00998	Connection not available!
C00999	Mod. 999
C01000	User V.
C01001	Select only one Object
C01002	item
C01003	Confirm new Article creation?
C01004	Activation done: please check email for activation code
C01005	Error in registration request
C01006	Invalid user name
C01007	Attention: Please confirm licence  conditions!
C01008	Activation Completed
C01009	Not Valid Activation Key!
C01010	User name or license missing
C01011	Catalogue Name
C01012	Restore file missing:
C01013	Attention: The catalogue
C01014	already exists! Delete before forcing restore!
C01015	Unzip BAK
C01016	Copy Catalogue Name
C01017	Confirm Deletion of: %1
C01018	Recognize
C01019	Copy DB Name
C01020	Restore DB: %1 from %2
C01021	File not found:
C01022	Archive Creation:
C01023	Filing:
C01024	Folders directory
C01025	Filing: (
C01026	Error XBOX:
C01027	) in procedures [Upload]
C01028	) in procedure GetFileList
C01029	CallBack:
C01030	Value X
C01031	Dongle not found:
C01032	Attention: to use walls you need a connection!
C01033	test
C01034	Layer
C01035	Not found! Rename one?
C01036	The shape is not inside the panel encumbrance, continue?
C01037	Circle
C01038	Level- To do
C01039	-Cannot save the file
C01040	The program found a new update. Do you want to install it? (Turn off antivirus!!)
C01041	Origin
C01042	File opening error!
C01043	Save?
C01044	Attention! File Save Error
C01045	Select Object
C01046	Select Vertex
C01047	Give Radius
C01048	Invalid Selction
C01049	Select Second Object
C01051	[F] Snap: Enabled
C01052	[F] Snap: Disabled
C01053	[R] Automatic Relations: Enabled
C01054	R] Automatic Relations: Disabled
C01055	[A] Fixed Angles: Enabled
C01056	[A] Fixed Angles: Disabled
C01057	[D] Background Sensibility (Dxf...): Enabled
C01058	[D] Background Sensibility (Dxf...): Disabled
C01059	[E] Snap with other objectsi: Enabled
C01060	[E] Snap with other objectsi: Disabled
C01061	Ghost
C01062	Enabled
C01063	File name
C01064	Last Change
C01065	Dimension (k)
C01066	Delete User/s ?
C01067	Operation completed correctly!
C01068	Error during user deletion!
C01069	Cannot credit a registered user !
C01070	Data saved correctly!
C01071	Data saving error!
C01072	The group
C01073	has been deleted
C01074	Group deleting error
C01075	Confirm Order deletion?
C01076	Operation completed!
C01077	Enter a destination folder!
C01078	Operation not executed!
C01079	Orders download completed!
C01080	Quotes download completed!
C01081	Error during Quote setting, n.:
C01082	Error during order setting, n.:
C01083	- version:
C01084	Delete Prototype?
C01085	Prototype '
C01086	 correctly deleted
C01087	Error during prototype deletion '
C01088	Prototype correctly sent!
C01089	Error during prototype sending!
C01090	Select orders destination folder
C01091	Select prototypes origin folder
C01092	Select data origin folder
C01093	Invalid Folder!
C01094	Error uploading file Giornale.bin.z:
C01095	Error uploading file antei.bin.z:
C01096	Catalogue update procedure completed
C01097	Export Completed
C01098	Wrong password!
C01099	Error saving data!
C01100	the catalogue
C01101	not in the folder
C01102	Folders on Servers will be shown
C01103	Delete Files?
C01104	Error during file deletion '
C01105	Files Deleted!
C01106	Update procedure completed
C01107	Delete Folder
C01108	Error during folder deletion '
C01109	Attention! Existing shape, overwrite?
C01110	Tahoma
C01111	New Document
C01112	General Properties
C01113	Value
C01114	Document Properties
C01115	Exit?
C01116	Changes have been done: Save?
C01117	File Opened
C01118	Is the Shape correct?
C01119	Arch Center
C01120	Arch first point
C01121	Not valid for selection
C01122	Junction
C01123	First Point:
C01124	Second Point
C01125	Stretch
C01126	not selectable
C01127	nonexistent
C01128	Replacement from starting point done
C01129	Perform again from beginning
C01130	Style Name
C01131	Already existing Style, overwrite?
C01132	Save changes?
C01133	Attention: file emf not saved
C01134	Attention! File name not correct
C01135	Are you sure you want to overwrite
C01136	Cannot automatically close the shape because the closing side intersects another one
C01137	Cannot insert last segment because it exits the panel
C01138	ai,af Before
C01139	ai,af After
C01140	am=
C01141	Segments insertion error, the
C01142	and the
C01143	they intersect
C01144	they match
C01145	Error during data input, last inserted and the
C01146	Be aware: This procedure cancels the catalogue local data to force data download! Continue?
C01147	read file
C01148	upload block
C01149	read file info
C01150	File Reading
C01151	download block
C01152	save file
C01153	) in procedures [Download]
C01154	Converting Data
C01155	To do GetFileList
C01156	Line|Lev|Code|Description|Qty|BasePrice|PriceIncrease1|PriceIncrease2|Dimensions|FSP|S/N|Discount|NetPrice|Variant
C01157	pann:
C01158	Selection:
C01159	Key not enabled!
C01160	Attention! The catalogue
C01161	already exists! Force restore?
C01162	2D Cad
C01163	there are no plotting rules!
C01164	Confirm JOB files creation?
C01165	Files created in folder:
C01166	Save Packaging…
C01167	Create Jobs…
C01168	Label:
C01169	Modified Data, save Changes?
C01170	Delete selected orders?
C01171	Error during order deletion, show log?
C01172	Ok!
C01173	Confirm selected orders download?
C01174	Download error! Show log?
C01175	Error saving data, show log?
C01176	Select data origin folder
C01177	-----> Error
C01178	Select Calalogue from List
C01179	Test scale 1:50
C01180	Width=
C01181	Height=
C01182	Scale 1:10
C01183	Scale 1:50
C01184	Scale 1:100
C01185	Download completed!
C01186	Download error!
C01187	Cannot delete order number
C01188	Done!
C01189	Confirm Delete Row
C01190	Handle Side
C01191	Shutter Side
C01192	Right
C01193	Left
C01194	Under
C01195	Above
C01196	Front
C01197	Back
C01198	Wrong password!
C01199	Order transfer procedure completed!
C01200	Order transfer procedure failed!
C01201	Backup:
C01202	Restore:
C01203	Recognize:
C01204	Confirm Formulas DB creation?
C01205	Delete Formulas DB?
C01206	) in procedure [Esequi] of model [mKomp]
C01207	Already existing View. Overwrite?
C01208	Test
C01209	Phase 1:
C01210	Formulas Verification
C01211	Levels inf.:
C01212	Tot.Rows:
C01213	exists, force Restore?
C01215	Locked order: OK to unlock ?
C01216	Operation ended
C01217	Operation completed
C01218	To do:
C01219	Package
C01220	Update: %1 files for %2 Kb
C01221	Recompute final script and linear element
C01222	Axonometry/Perspective
C01223	Create top views
C01224	Edit Top
C01225	Horizontal x Vertical Length
C01226	Supervisor login
C01227	Supervisor Management
C01228	>client code|<eMail|<Catalogue|Download|<Server n.|<Review|<Client N.|<Order status|<Blocked|<Date
C01229	<Catalogue|<Enabled|>Client code|>Price list code|>Price list coeff.|>Language|Agent|Shipment Mode|Reference Code
C01230	>User Id|<Catalogue|<Flag|<Date
C01231	<Year|<Month|<Points
C01232	<User Id|<eMail|<Catalogue|<Points
C01233	>Id|>Client Code|<Company Name|<eMail|Enabled
C01235	Copy in series
C01239	Detach at the beginning
C01241	Connect previous
C01245	Open left\right
C01246	Transfer procedure failed: order locked!
C01247	Add vertical division
C01248	Add horizontal division
C01249	Divide…
C01251	Delete division
C01252	Set…
C01253	Divide area
C01254	Set area
C01255	Detail
C01256	Total
C01257	User not enabled!
C01258	Already exist, force restore?
C01259	02-Only Solid
C01260	25-Placer
C01261	31-Edges
C01262	Placer Enabled:
C01263	Positions and Dimensions
C01264	Setting Name
C01265	Orders Filter
C01266	Inserted
C01267	Downloaded
C01268	Confirmed
C01269	In_Process
C01270	Export completed in Folder
C01271	Missing Data
C01272	Save Completed!
C01273	Catalogue
C01274	: Insert Code!
C01276	Monthly Fixed Amount
C01277	Delete View %1?
C01278	Impossible to add to processed View
C01279	Delete Clips from current Folder
C01280	Confirm Deletion of selected Reviews
C01281	Set selected Review to Main Order
C01282	Error during Order Deletion
C01283	Delete Review
C01284	Confirm File Deletion?
C01285	Discard Changes?
C01286	Move Y
C01287	Move Z
C01288	Delta
C01289	Wall Dist.
C01290	Wall H.
C01291	Insert all data
C01292	Insert a valid Name for Macro
C01293	Select Type for Macro
C01294	Delete Macro: %1
C01295	Delete this rule?
C01296	Missing Data
C01297	Order Number changed
C01298	Confirm Selection of Orders without Header
C01299	Confirm JobOrder Deletion
C01300	Confirm Load Filing
C01301	Loads Filing completed
C01302	File
C01303	Already present, overwrite?
C01304	Insert new Name for Archive
C01305	Invalid File!
C01306	Save Variations?
C01307	Bill of materials existing in
C01308	Error during Deletion
C01309	Confirm selected row deletion?
C01310	Already exist, delete data?
C01311	Modified JobOrder. Save?
C01312	Modified Load. Save?
C01313	ATTENTION! No selected Orders on Load
C01314	No Carrier
C01315	Invalid Code
C01316	Invalid Order
C01317	Order not confirmed.
C01318	Order already exist in JobOrder %1
C01319	Order already exist in Load %1
C01320	Order already exist in Directory
C01321	Press ESC to stop
C01322	Additional JobOrder Number
C01323	Additional Load Number
C01324	Attention! Update Orders?
C01325	Order
C01326	Agent
C01327	Zone
C01328	Delivery Date
C01329	Order Family
C01330	Courier
C01331	Truck
C01332	Truck Date
C01333	Orders Number
C01334	Package No.
C01335	Weight
C01336	Gross Weight
C01337	Volume
C01338	Amount
C01339	Confirm Customer Deletion?
C01340	Attention: select at least one Row
C01341	Attention: select rows from same Package
C01342	Attention: Article in different Department, will be forced to
C01343	Attention: one Article has a different Packaging, it will be forced to
C01344	Hardware Package
C01345	Recalculate Volume?
C01346	Loads
C01347	Dep. For Package n.
C01348	is changed, update?
C01349	3CAD Catalogue not found
C01350	on path
C01351	Dep.
C01352	Path
C01353	Order N.
C01354	Label No.
C01355	Unload
C01356	Load Processing
C01357	Renumber Labels
C01358	Price Increase Modified! Save?
C01359	Impossible to save a Price Increase without Code
C01360	Insert Date for Price List
C01361	Row No.
C01362	Price increase
C01363	Confirm Model Deletion
C01364	Modified Data, save Changes?
C01365	Save and Compile Variants?
C01366	Attention! Empty Model not Saveable
C01367	Reference
C01368	Ins.Date
C01369	Operator
C01370	) in Procedure 'varFiltrozionaordini'
C01371	Select a Print Module
C01372	Cannot load Order
C01373	Company Name
C01374	Date (from…to)
C01376	Status
C01377	Show quote
C01378	>Client Code|<eMail|>Client n.|>Check|>Amount|>Date
C01379	|>Code|Company Name|>Orders|>Quotes|Activation date
C01380	<|<File|<Upload
C01381	>Activation Date|>Last date use|>Orders Sent|>Required Quotes
C01382	>Date|<Subroutine|<Error
C01383	>Key n.|<Catalogue|<Enabled|<Client Code|<Client Code Reference|<Code List|<List Coeff.|>Company Name|>Address|>Zone Code|<Locality|<Province|>Tel.|>Fax|<Web|>Language
C01384	>User Id|<Enabled|<Catalogue|<Code List|<List Coeff.|<Reference Code|<Client Code|>Company Name|>Address|>Zone Code|<Locality|<Province|>Tel.|>Fax|<Web|>Language|<Moduli|>Company User|<Header Html|<Model Html|<Disable main office dispatch|<Expiry date
C01385	Order review n.
C01386	#99;|#0;Inserted|#1;downloaded|#5;Invalid|#11;Confirmed|#12;cancel_request|#14;Cancel_Process|#13;Cancellation|#21;in_progress
C01387	#99;|#0;Inserted|#1;Downloaded|#11;Confirmed|#12;Annulment_request|#13;Off|#21;In_Process
C01388	Write Engine Logs?
C01389	Render not installed!
C01390	Restart program to enable changes
C01391	Confirm User Deletion?
C01392	Err:
C01393	Attention! Mabry Barcod Control not installed.
C01394	Insert file name:
C01395	File name missing
C01396	Already existing File. Overwrite?
C01397	Delete: %1 ?
C01398	Quote
C01399	Composition
C01400	Error loading Order!
C01406	Wait!
C01407	Downloading Updates
C01408	file not valid
C01409	) in procedure 'EstraiPreview' in Class Module 'Conn'
C01410	Save Review?
C01411	Not existing File, create?
C01412	Select Price List
C01413	Set Mode
C01414	Empty Print Module
C01415	Module not found
C01416	;Initial Catalog=
C01417	Restoring catalogue
C01418	in progress…
C01419	completed!
C01420	14-Dragger
C01421	Overwrite base color?
C01422	Load Walls
C01423	Save:
C01424	Saving data error!
C01425	Can't set to current gallery: it belongs to
C01426	copied to gallery
C01427	Load composition?
C01428	Delete composition?
C01429	Download File
C01430	New folder name:
C01431	New folder
C01432	Delete folder?
C01433	The composition already exists, overwrite?
C01434	Activation trends during the period
C01435	Trends in demand estimates
C01436	Sent orders performance during period
C01437	Activations_
C01438	Quotes_
C01439	Orders_
C01440	The database is not appropriate!
C01441	Add structure macro
C01442	Add vain macro
C01443	Vertical division
C01444	Horizontal division
C01445	Divider type
C01446	Thickness divider list (*)
C01447	Horizontal spacing (*)
C01448	Vertical spacing (*)
C01449	Invalid access to the site
C01450	Unable to access the remote site
C01451	Converting Data old format!
C01452	Lift walls
C01453	Drop walls
C01454	Enable lift/drop walls
C01455	Move working plan
C01456	Enable/disable collisions
C01457	confirm: press enter
C01458	abort operation: press esc
C01459	Note management
C01460	Enable/disable dimensions
C01461	Dimensions management
C01462	Puntual dimensions managment
C01463	Enable/disable snap on wall
C01464	Enable/disable snap on box
C01465	Free positioning mode
C01466	Select a work plan
C01467	Set new point
C01468	Set new line
C01469	Set point in offset
C01470	Set perpendicular line
C01471	Enable/disable plotting
C01472	Free mode
C01473	Saved:
C01474	Missing module M07!
C01475	Autocad files not found!
C01476	Autocad files not initalized!
C01477	Error: %1 - %2
C01478	Attention! new program update available
C01479	Select save file path
C01480	select folder for download
C01481	Do you confirm the release of updates
C01482	Dett|Typology|Lato
C01483	V|ID|Kb|Name
C01484	Variant|Alias|Description
C01485	Code|C|Description
C01486	AB|Code|Description
C01487	0-Default|1-Arch|2-Oblique Arch|4-Vertical division block|5-Horizontal division block|9-grid Division
C01488	Build Ogg...
C01489	Build DRG1...
C01490	Compile...
C01491	Confirm import
C01492	confirm save
C01493	Attention:manually modified Tops, calculate?
C01494	aggregate
C01495	Confirm filing?
C01496	>lenght|>width|<Article|<Description
C01497	Truck modified. Save?
C01498	Order already existing in truck %1
C01500	Truck number to add
C01501	Select|from value|to value
C01502	Package|>Qty
C01503	Nr.|Company name 1|Company name 2|address|zip code|City|Prv
C01504	Model code already exist in records, it will be overwritten! Continue?
C01505	Specify start model!
C01506	Specify finish model!
C01507	Insert a description for the new model
C01508	Operation successfully completed!
C01509	Error during elaboration:
C01510	Pack|Laod|Description|Ord|Label
C01511	Code||Description|Code2
C01512	Error during data importation
C01513	Dimension Y not valid
C01514	Do you confirm the elimination of the object?
C01515	Insert valid dimension!
C01516	Be aware! All ID are occupied.
C01517	Import DXF
C01518	Set point
C01519	Set  measurement
C01520	Export DWG
C01521	Sub-environment
C01522	Send eMail
C01523	Send Pdf
C01524	Send fax
C01525	Text
C01526	Line
C01527	Current box
C01528	Orders multiprint
C01532	Set printer
C01533	|<Catalogue|>Order Number|<Code|<Client|<Reference|>Entry date|>Delivery Date|Amount
C01534	Work orders storage
C01535	Loads storage
C01536	Truck storage
C01537	Fl|Work order|Order|Client|Ref.|Amount
C01538	Fl|Load|Order|Client|Ref.|Amount
C01539	Fl|Truck|Order|Client|Ref.|Amount
C01540	Work orders processing
C01541	Loads processing
C01542	Truck processing
C01545	Preview
C01546	Import
C01548	Create work orders
C01549	Create loads
C01550	Create truck
C01551	Refresh
C01552	Join
C01553	Attach load
C01554	Attach truck
C01555	Attach work order
C01556	New line
C01558	Clear loads column
C01559	Recalculate unloads
C01560	Reorder
C01561	Reset reorder
C01562	Move up
C01563	Move down
C01564	Move before
C01565	Move after
C01566	|<Model|>Order N.|>Unload|<Carrier|<Code|<Customer|<Reference|>Delivery Date|O|R|B|>Net Weight|>Gross Weight|>Amount|>Packages|<Address|>ZIP|<City|<State|>Cod. Dest.
C01567	|>Package|<Code|<Description|<Rep|>Volume|>Dim 1|>Dim 2|>Dim 3|>Row|<CodPak|<DesPack|>Identifications|>Identification|>Height|>Witdh|>Depth.
C01568	Table counter management
C01569	Job processing
C01570	|Variant|Option|Hide
C01571	|Drift Variant|Drift Options|Derivative Variant|Derivative Option
C01572	|Name|Value
C01573	Duplicate model
C01574	<|>Id|<Catalogue|>Order|<Operation|>Date|<Print|>Priority|<Dispatched|<Parameters|<Output
C01575	#1;Normal|#2;eMail|#3;Pdf|#4;Fax
C01576	EcadPro print service
C01577	Re-Compile Bars
C01578	Items MasterData
C01579	|Child Code|...|Des.|Quantity|Validity|FormulaL|FormulaA|FormulaP|Note 1|Note 2|Note 3|Note 4|Note 5|Note 6|Variants
C01580	|^Var|^...|^Opz|^...|^Child Var|^...|^Child Opt |^…
C01581	|Price List Code|...|Des.|Price|Virtual|Date
C01582	|Price List Code|...|Des.|Variation|...|Option|...|%|Price|Base|Date|CodeIncr
C01583	|Price List Code|...|Des.|W|H|D|%|Price|Date|CodeIncr
C01584	Yes
C01585	** not defined **
C01586	Insert data (dd/mm/yyyy):
C01587	Borders editor
C01588	Client master data
C01589	|Price List|...|Start Date|End Date|Product Category|...|Disc.1|Disc.2|Disc.3|Disc.4|Agent|...|Commission|Model|…
C01590	|Price List|...|Des.|Variant|...|Option|...|%|Price|Price Rises Type|Date|Price Rises Code
C01591	|Price List|...|Des.|W|H|D|%|Price|Price Rises Type|Date|Price Rises Code
C01592	#0;Standard|#1;Base Price|#2;Fixed
C01593	Field name|Value
C01594	Packages
C01595	Set number sign (#) to give values to all categories
C01596	Code|Des.|Packages List|
C01597	All files|*.*
C01598	Shape copied to the clipboard
C01599	Shape
C01600	Set new thickness
C01601	Unsaved data!
C01602	Enabled|Description|flCatalogo
C01603	Select the type of export!
C01604	Users points
C01605	Offline keys
C01606	Plane Ass.
C01607	Order saved:
C01608	Cancellation request
C01609	All|Order|Quotation|Confirmed
C01610	All
C01611	Attach existing file
C01612	Image Scanner
C01613	Delete attachment
C01614	Set Measurement
C01615	Motion
C01616	Record
C01617	Full screen
C01618	Error: Box %1 collision. Change position!
C01619	|>Code|<Sender|>Server N.|>Review|>Shop N.|>Status|Delete|>Blocked|<Date|<Type|<Info|   
C01620	>Order|<Catalogue||>USer Id|>Server n.|>Review|>Client N.|>Status|>Blocked|<Date
C01621	Canceled
C01622	In production
C01626	Black and White
C01627	Cartoon
C01628	Squid
C01629	Pastel BW
C01630	Paint
C01634	Plane Ass.
C01635	Ass. Lh 30
C01636	Ass. Rh 30
C01637	Prosp. Lx 45
C01638	Prosp. Rx 45
C01639	Recent configuration
C01640	|Child Code|...|Des.|Quantity|Validity|Phase|FormulaL|FormulaA|FormulaP|Note 1|Note 2|Note 3|Note 4|Note 5|Note 6|Variations|Units
C01641	Offset plane
C01642	Offset line
C01643	Lh,Rh
C01644	Wall offset
C01645	Wall
C01646	Short,Tall
C01647	Point
C01648	Incorrect values
C01649	Dimension...
C01650	Create ceiling
C01651	Dump sections
C01652	Swap 1-2
C01653	Delete group
C01654	drCalculate
C01655	New item
C01656	Delete item
C01657	Paste items
C01658	Exit
C01659	|<Cod|<Company Name|<eMail
C01660	Head office
C01661	Order Type
C01663	Move
C01664	Position
C01665	From above
C01666	Variant|Description|Locked|Value
C01668	[D] Background Sensitivity (DXF ...): Disabled
C01669	[I] Automatic Insertion: Enabled
C01670	[I] Automatic Insertion: Disabled
C01675	[O] Orthogonal: Enabled
C01676	[O]  Orthogonal: Disabled
C01677	>User Id|<Enabled|<Catalogue|<CodeList|<ListCoeff|<Ref.Code|<ClientCode||>CompanyName|>Add.|>ZoneCode|<Loc|<Prov|>Tel.|>Fax|<Web|>Lang.|<Modules|>CompanyUser|<HeaderHtml|<ModelHtml|<Disable main office dispatch|<Expiry date|<eMailNotif|Group Id|<Other Dest
C01678	Measure
C01679	Set offset
C01680	Layers…
C01681	[L]Auto Layers: Enabled
C01682	[L]Auto Layers: Disabled
C01683	Loading walls
C01684	Set Dxf Source
C01685	Reverse side
C01686	> COD.CLIENTE|<COMPANY NAME|<E-MAIL
C01687	Auto alignment box
C01688	Rotate +- 90°
C01689	Loading Shaders
C01690	Raise View
C01691	Lower View
C01692	Enlarge FOV
C01693	Narrow FOV
C01694	Replace Camera
C01695	Play
C01696	Options
C01697	Trim
C01698	Select Cut Points
C01699	Select father of Placer
C01700	Bring orthogonal camera to the wall
C01701	Select …
C01702	Segment
C01703	Image
C01704	Surface
C01705	Hole
C01706	Size
C01707	Light
C01708	Dimension
C01709	Object
C01710	move origin
C01712	Pos.Mode
C01713	Var.Obb.
C01714	Link
C01715	Placer
C01716	Specials
C01717	Divider
C01718	Flags
C01719	Managing Typologies
C01720	Management table lists
C01721	<CATALOGUE|>MASTER LIST|>PRODUCTS LIST|>COEFF
C01722	Snap View Point of sub-elements
C01723	Hide all snap point
C01724	Add new arc dimension
C01725	Add a line between two faces
C01726	Hide a visible line
C01727	Edit lines
C01728	Set room origin point
C01729	Brings to the fore part of the photo
C01730	Move scene origin
C01731	Calculating quote…
C01732	Number of elements in each direction:
C01733	Select the plane for tracking …
C01734	Select two objects to be connected …
C01735	Select a plane where to place the object …
C01736	Select a wall where to place the object …
C01737	Warning: Confirm Re-Calc AKA?
C01738	DXF
C01739	04-Origin
C01740	05-Dimensions
C01741	15-Tracer cylinder
C01742	Select folder!
C01743	Select file!
C01744	Chord
C01745	Set height…
C01746	Cannot tilt an area that has at least three landmarks
C01747	Select a point to set dimensions
C01748	Height
C01749	Surface height
C01750	Profiles
C01751	Add profiles
C01752	Delete profile
C01753	Set height…
C01754	Angle
C01755	Angle in Degrees
C01756	Enter search text:
C01757	Arrange Items
C01758	Vis. | Mod. | Layer Name
C01759	Name:
C01760	-no element-
C01761	Show properties
C01762	Width:
C01763	Height from Ground:
C01764	Thickness:
C01765	Height:
C01766	Horizontal mirror
C01767	Vertical mirror
C01768	Scale
C01769	Sets scale factor
C01770	Amount:
C01771	Mirror
C01772	Confirm
C01773	Generic
C01774	Generic
C01775	Add  generic
C01776	Dump
C01777	Data copied on Clipboard
C01778	07-Mirror
C01779	Clear materials assignment
C01780	Replace material inside the box
C01781	Replace material on the scene
C01782	Want to reset materials replacement?
C01783	Create Drawing Page
C01784	Warning! Detected program open instance can not update!
C01785	To complete upgrade you must run as Administrator!
C01786	| Id Materials | New Materials
C01787	Size (Kb)
C01788	Summits
C01789	sides
C01790	Do you want to delete option:
C01791	You can not delete catalog variants!
C01792	Enter a file and a description!
C01793	Attention existing file! Overwrite?
C01794	32-Dressing Area
C01795	33-Drilling template
C01796	11 -No Cylinder Angle
C01797	12 -Polygon Cylinder
C01798	13 -Cylinder 2 pts
C01799	14 -Cylinder wall floor
C01800	15 -Cylinder Goniometer
C01801	16-Complements Horizontal
C01802	17-Complements vertical
C01803	Dressing Area
C01804	Property
C01805	34-Panel Boole
C01806	Edit Notes
C01807	New shape
C01808	New ring
C01809	New shaped hole
C01810	Shape (neg)
C01811	Circle (neg)
C01812	New circular hole
C01813	new dimension
C01814	Boolean
C01815	Execute boolean
C01816	insert provider
C01817	Converting Data :
C01818	Can not open database!
C01819	Can not find catalogue 3
C01820	Delete
C01821	paste
C01822	Cut
C01823	Sel|Code|Description|Adjust|Exceptions|Flag|Bill|Dimensions
C01824	Connect  other classification
C01825	Unplug  other classification
C01826	Import Text File
C01827	Obsolete variations
C01828	Manag.Code|Order|x1|x2|x3|x4|x5|Extra
C01829	Bill of materials
C01830	Bill of materials Package
C01831	Recalculate Top:
C01832	Macro Graphics:
C01833	Job:
C01834	Formula-based Price List
C01835	Formula-based Bill of materials
C01836	Extra data
C01837	Ceiling successfully created
C01838	Create
C01839	First select a floor
C01840	Obsolete
C01841	Import
C01842	Reached maximum length of notes!
C01843	 Strech Mode
C01844	Failed to load xml data from file!
C01845	Go to the catalog% 1?
C01846	Restart the program to complete procedure!!
C01847	Reduce gallery
C01848	Expand gallery
C01849	Insert comment line
C01850	Cancel?
C01851	Elements:
C01852	Finish:
C01853	Procedure:
C01854	Depth
C01855	All files
C01856	Error on saving
C01857	Area
C01858	Type:
C01859	Finish:
C01860	Strut
C01861	Finish
C01862	Linked:
C01863	Yes
C01864	No
C01865	Set All
C01866	Machine program record
C01867	Production phase management
C01868	Phase/department default for phase macro
C01869	Remove junction
C01870	Attachments
C01871	Change junction
C01872	Attachments:
C01873	Synchronize gallery? Operation may take several minutes...
C01874	Be aware! dll recording operation extented operation suspended!!
C01875	Impossible to find file on _ECADPRO\DauBaseSQLN.DLL
C01876	Impossible to find file %1\DLL\DauBaseSQLN.dll
C01877	Delete selected items
C01878	Be aware! Do you want to delete the current bar?
C01879	Be aware! Update error of variants for l article:
C01880	Be aware! Operation delete by user!
C01881	Insert bill code!
C01882	Be aware! Save data error in ArtDepositi table!
C01883	Be aware, impossible to save data. Error on ArtDepositi table cancelling!
C01884	Unvalid data!
C01885	Test bill of material
C01886	ID|Reason|DescrReason|DataMov|Warehouse|Department|Phase|Cli/Sup|Item|L|A|P|Um|Qty|Um2|QtyUm2|Price|Variant|Notes|Quality|DocType|DocNumber|Line|Rc|UserTmp
C01887	Deposit|Location|...|Valid|Department|Stock|Notes
C01888	Production cycle management
C01889	Add line
C01890	Remove line
C01891	Move up
C01892	Move down
C01893	Cancel selected line?
C01894	Save bill?
C01895	|Code|Description|Rel.|Decl.|Prog. Mng.|Preferential|
C01896	Confirm l eliminate phase %1 ?
C01897	Be aware! Phase %1 is linked to other survey phases, cancel linked phases?
C01898	Be aware! Do you want ot save current phase changes?
C01899	|Diml|DimA|Cod.Prg.|Script|...|Notes
C01900	Attention! Cancel current program?
C01901	Impossible to add script! First set dimensions and program code!
C01902	Attention! Save current program modifications?
C01903	Be aware! Press Yes to add the variants on the items of the current bar, No to add the articles of all bars, Cancel to void l operation?
C01904	String class not valid:
C01905	Compute|Draw
C01906	<Option|<Description|<Model|...|<Article|...|...|<Variant Values management
C01907	Management variants
C01908	Making of a new catalogue
C01909	Apply not available on catalogue item %1 !
C01910	Overwrite existing order n. %1?. Select No to enable revision management and keep a copy.
C01911	Spares management
C01912	>Line|<Line type|<Item|<Description|>Price|>Qty|<Variants
C01913	Replacement order number created
C01914	Confirm replacement order!
C01915	Comp. Line
C01916	Comp. Code
C01917	Comp. Qty
C01918	Comp. Variants
C01919	First height:
C01920	Second height:
C01921	Right width
C01922	Left width
C01923	Second Alt.
C01924	First Alt.
C01925	Right W.
C01926	Left W.
C01927	Three side diagonal
C01928	Statistical graphics
C01929	DATE|ACTIVATION
C01930	DATE|QUOTES
C01931	DATE|ORDER
C01932	Impossible to create the object Excel.application. Operation Voided!
C01933	Not executed!
C01934	Select table to update!
C01935	Send to Server
C01936	BLOCK
C01937	No selected file!
C01938	Database update in progress...
C01939	Expiry date minimum 1 day!
C01940	Expiry date max 01/01/2030
C01941	Unvalid expery date!
C01942	ID
C01943	eMail
C01944	CompanyName
C01945	Date
C01946	n. ACCESSES
C01947	User elimination cancelled: user has been activated at least one time.
C01948	File types
C01949	42-Ruler Point
C01950	43-Multicatalogue Placer
C01951	44-Box Marker
C01952	Program Info
C01953	Manual
C01954	Open maintenance data
C01955	Close maintenance data
C01956	View multicatalogue placer compartments
C01957	Perform zoom on scene detail
C01958	Edit current clip plane
C01959	Change phantom objects visualisation mode
C01960	Hide menu/show/object ball selected
C01961	35-extrude with processing color
C01962	45-Line with shape cutter
C01963	On-line catalogue
C01964	Local catalogue
C01965	Utility to enabled users table data maintenance
C01966	If specified the modified initial value will be executed on all records
C01967	|ID|COM.NAME|EMAIL CLI|LANGUAGE|EXP DATE|WARNING DATE|COM EMAIL|AGE EMAIL|WARNING STRENGTH
C01968	<User Id|<Client Cod.|<eMail|<Catalogue
C01969	>Id|<Cli Cod.|<Com.Nam|<eMail|<Info|>PointsC
C01970	Exportation executed in table StatisticheSv of eCadMaster database!
C01971	Sent Options|Db Update|Sycro
C01972	DB comparison
C01973	B. Head
C01974	B. Tail
C01975	L.Raw
C01976	A
C01977	P
C01978	Check file auto...
C01979	Btl file not present for order
C01980	Cancel whole order
C01981	Make Macro
C01982	Rotate whole order
C01983	Show dimensions
C01984	Duplicate current price
C01985	Make complete picture of order
C01986	Start Machine
C01987	Tools
C01988	Upload previous
C01989	Upload next
C01990	Assistance
C01991	Transparent colors
C01992	Move down
C01993	Remove selected processing
C01994	Repeat selected processing
C01995	Re-upload process macro
C01996	Re-upload process
C01997	Save process template
C01998	Enable and disable processing
C01999	Select a process
C02000	Default description
C02001	[Group.]Code:
C02002	Description:
C02003	X-Y Dimentions
C02004	Confirm order complete rotation for work order %1 ?
C02005	Eliminate all processings belonging to order %1 ?
C02006	ATTENTION: Impossible to replace the beam as long as rotation or invertion are present!
C02007	Set all to zero!
C02008	Insert beam length
C02009	Order %1 code %2 already present, overwrite?
C02010	Upload data?
C02011	Available precesses
C02012	Inserted processings|Progressive
C02013	Eliminate process %1 order %2 ?
C02014	Data changed! Save?
C02015	Confirm macro eliminition %1
C02016	Insert a bar!
C02017	New processing inserted!
C02018	The macro name cannot be over 10 digits
C02019	Attention: Macro[%1] exist! Replace?
C02020	Create jobs for order %1 - code %2
C02021	Generate prices based on order lists?
C02022	Loads to process (separated by -)
C02023	Create jobs for order: %1 - %2
C02024	Error creates programs
C02025	Lengths calculation order %1 - code %2
C02026	Loads number (separated by -)
C02027	Pieces length computation procedure
C02028	Calculate pieces length of all orders?
C02029	Calculate pieces lengths order %1 - code %2
C02030	Calculate pieces lengths load %1 - order %2 - code %3
C02031	Upload the newly create piece?
C02032	Import file %1 in progress...
C02034	Import file ttr completed!
C02035	Exportation path
C02036	The reduction may not be more then bar length!
C02037	Description
C02038	Cancel color %1 - %2?
C02039	Begin work order
C02040	Move list up
C02041	Move list down
C02042	Reload moved lists
C02043	Cancel list
C02044	Save list
C02045	Ok?|Order|Code|Ins. Date|Item|Order Qty|Residual Order|Leng.|Wid.|Launch Len.|Material|Des.|Code|Rotation|TemplLav|DiffLav
C02046	Register number|Dimensions|Stock|Recovery|Engaged|Ordered|Use|hide|Description|Exclude from Optimization
C02047	Processed|List|Qty|Bar|Registry number|Legth|Heading|L_Residual
C02048	Processed|Order|Code|Description|Processing Qty|Position|Length|C.Head|C.Tail
C02049	Do not save bar without pieces!
C02050	List elimination in progress...
C02051	Work order already optimized, continue? (The lists will be resetted)
C02052	Optimization in progress...
C02053	Empty lists?
C02054	This kind of optimization works only for uploading!
C02055	Optimization completed!
C02056	- Processing file check in progress...
C02057	Processing file check gave negative result!
C02058	No work orders uploaded!
C02059	Create work order ASCII: %1 ?
C02061	Data uploading in progress...
C02062	Uploading completed
C02063	Data work order %1 missing!
C02064	Do you wish to eliminate work order %1?
C02065	No bar selected!
C02066	Prices already completed in the lists!
C02067	Impossible to insert price with wrong rotation!
C02068	Chosen dimention insufficient space to insert beam!
C02069	Bar dimention too small! Impossible to accept!
C02070	Heading dimention excessive! Impossible to accept!
C02071	Save list %1 to update data on file lst!
C02072	Optimization upload error!
C02073	Bar %1 not available, verify warehouse records!
C02074	Optimization not complete for bar %1 -  %2
C02075	Stop automatic work order!
C02076	List export in progress...
C02077	Duplicate beam
C02078	Cancel beam
C02079	Eliminate beam: %1 registry number %2?
C02080	reload data...
C02081	Rule detail
C02082	Return
C02083	Template
C02084	Insert[Group.]Code:
C02085	Save template
C02086	Be aware: want to replace defaults for %1
C02087	Cancel template %1
C02088	Authentication
C02089	Password:
C02090	Apply Macro
C02091	Bar forward
C02092	Bar back
C02093	Insert line
C02094	Warehouse management
C02095	Bar dimention not correctly saved!
C02096	Save changes?
C02097	Select a file inside catalogue %1!
C02098	Eliminate bar %1 ?
C02099	Recover|registry number|Codtype|Assigned|Length|Quantity|Location|DataGeneration|List make recovery|List use recovery
C02100	Impossible to eliminate dimension because used in a list!
C02101	Repeat processing
C02102	Eliminate all warehouse bars
C02103	Parameters
C02104	Impoert export data path
C02105	ATTENTION:this procedure eliminates from the database all PROCESSINGS before date
C02106	PROCEED?
C02107	ATTENTION: this procedure eliminates from the database all inserted PROCESSINGS!
C02108	Process concluded!
C02109	ATTENTION:this procedure eliminates from the database all WORK ORDERS before date
C02110	ATTENTION: this procedure eliminates from the database all inserted WORK ORDERS!
C02111	Conferm complete elimination of recoveries?
C02112	Select an export path!
C02113	Data export in progress...
C02114	Data export completed!
C02115	Select data import path
C02116	Data import in progress...
C02117	Data import completed, restart the program to enable the changes!
C02118	Data import completed!
C02119	Code|Description|Type|Height|Depth
C02120	Order|Code|Description|CodMaterial|Length|Width|Thickness|Quantity|List|Date Creation|Load|Camion|Des5|Des26|Client
C02121	Order|Code|Description|CodMaterial|Length|Height|Depth|Quantity|List|Date Creation|Load
C02122	Order|Date
C02123	Data Research
C02124	Items availability analyzes
C02125	Detailed production analyzes
C02126	Production analyzes
C02127	Available period analyzes
C02128	Storage reason
C02129	Effects management
C02130	Documents
C02131	Exercise
C02132	Inventory management
C02133	Warehouse movements
C02134	payments
C02135	Items search
C02136	Advance search
C02137	Documents search
C02138	Set values for item variants
C02139	Document type
C02140	Document type to extract
C02141	View movements
C02142	Warehouse movements
C02143	Documents generation
C02144	Varying Bill
C02145	Grid uploaded in
C02146	seconds
C02147	Inventory:
C02148	of
C02149	Availabilty analyzes
C02150	Warehouse situation
C02151	The counter forced at number
C02152	as last used number
C02153	Document n.
C02154	View Document
C02155	Show doc. lines
C02156	Line|Item Cod.|Description|Dimensions|UM|Qty1|Qty2|Qty Eff.|Qty Res.|Qty Prel. Phase|Qty Prel. Phase|Qty Prel. Phase|Qty Prel. Phase|Qty Prel. Phase|Quality|Qty 1°|Qty 2°|Qty 3°|Process
C02157	Error!! Rule
C02158	not found in
C02159	path verification
C02160	Attention!! Impossible to proceed!! Reason of document missing!!
C02161	Proceed to make documents?
C02162	Do you want to terminat?
C02163	Impossible to cancel! Item linked to documents!
C02164	Impossible to cancel! Item linked to documents!
C02165	Save changes before exiting?
C02166	Items. Confirm cancellation?
C02167	Export completed
C02168	Impossible to find reference on ProdPianifica id
C02169	Retry!!
C02170	Attention!! Impossible to proceed!! Customer/Supplier or  reason of document missing!!
C02171	Attention! Confirm the changes to the cylces and generate the missing codes?
C02172	Attention! Item code missing!! Verify data!!
C02173	id Item data update error
C02174	and user
C02175	Proceed to make document?
C02176	Production planning
C02177	Attention! The document you are trying to open doesn't exist!
C02179	Verify paths!!
C02180	Proceed to make the c/work documents?
C02181	Attention! There have made
C02182	C/Work documents
C02183	Attention! No C/W document made!
C02184	Attention!! Impossible to proceed!! Customer/Supplier or  reason of document missing!!
C02185	Attention! No documents will be generated, select the line on which to generate the document!
C02186	No elaborations to be uploaded, manually set the order!!
C02187	No elaboration found!!
C02188	Attention! Clear all lines?
C02189	Attention!! Select all lines?
C02190	Attebtion!! Impossible to proceed!!
C02191	Customer/Supplier or document reason for missing semi-processed!!
C02192	Excel is required for this option
C02193	Attebntion! No document is set for production! Impossible to recover print modules!
C02194	Proceed with counter update for the selected shop?
C02195	Attention! Numberator already exist per code
C02196	Update anyway? Errors may occur in the documents numbering!!
C02197	Attention! Meter update error!!
C02198	Attention! Meter insertion error !!
C02199	Document type Save error!!
C02200	Impossible to determine the item requested state:
C02201	Attention item inventory date missing:
C02202	Meter Set  Error:
C02203	File path not found!
C02204	Do you want to save the production data with the changes on the table?
C02205	Desire to cancel movement
C02206	Warehouse movements
C02207	during the elimination movement!
C02208	Error on movement elimination
C02209	Stock
C02210	Deposit
C02211	Ordered
C02212	Committed
C02213	Processed
C02214	Data format error!
C02215	Save current settings?
C02216	Attention! Document type varies, want to save data?
C02217	Attention! Show valid code before saving!!
C02218	Attention! Document type not saved!! Close form anyway? 
C02219	Attention! Sure you want to cancel document
C02220	Attention! Impossible to cancel the type of document. There are n.
C02221	documents inserted with this type!!
C02222	Attention! You have selected the same type of document in the filter as in the generation! Impossible to proceed!!
C02223	Select a document type!
C02224	No document made!!
C02225	Attention! More documents are being tried to be made
C02226	of how many are not available free numbers
C02227	No document selected, operation aborted!!
C02228	Do you want to proceed with the document generation?
C02229	Error on the table elimination
C02230	Error on the table filling
C02231	Impossible to proceed!!
C02232	Attention! Not all lines have been saved coorectly for the document
C02233	Deadlines
C02234	Attention! VAT headframe not inserted!! Verify data!!
C02235	Erroe on the date!!
C02236	Attention! An inferior document number is being inserted in respect to the last available one!!
C02237	Cancel selected document?
C02238	Document erase error!
C02239	No documents found with the set criteria!
C02240	Attention! The document has state
C02241	Impossible to set a state at a lower level!!
C02242	To insert a line depress the button INSERT LINE on th toolbar!
C02243	Attention! Select document type to make before proceeding!
C02244	Attention! Document letterhead rule not found! Verify path!
C02245	Attention, missing data! Impossible to save!!
C02246	Set warehouse, impossible to continue to save
C02247	Blocked document or in production, impossible to save changes!!
C02248	Attention! Line n.
C02249	WILL NOT be saved because already in production!!
C02250	Attention! Document blocked or arranged for production! Impossible to cancel!!
C02251	Attention, document not cancelled!
C02252	Document Management!
C02253	Document cancelation error!
C02254	Cancel document?
C02255	Remove line number
C02256	Line cancellation error
C02257	Attention! No movements to display!
C02258	Attention! Document changed, want to proceed?
C02259	Attention! Set the document type before proceeding!
C02260	Feaute not active for document type!
C02261	Attention! No bill to change for the selected document line!! 
C02262	Attention! Line in production! Impossible to change!!
C02263	Attention! Save document to optimize!!
C02264	No optimizer specify!!
C02265	The file making is void because not working!
C02266	File making executed correctly.
C02267	No effect present on archive.
C02268	Error, the client
C02269	doesn't have ABICAB code!
C02270	Error  while writing 
C02271	Imcomplete or wrong data.
C02272	Invoice Ns
C02273	Effect Ns
C02274	Attention! VAT headframe cancellation error on the temporary tables!
C02275	Attention! VAT headframe computation error!
C02276	Attention! VAT headframe save error!
C02277	Attention! Error while saving VAT error!!
C02278	Error while saving the deadlines
C02279	Error while saving the document movements
C02280	Error while saving the document movements
C02281	Attention! Sp Doc_CalcolaTotali missing!!
C02282	Error on the data preparation for production!!
C02283	Data changed. Save?
C02284	Attention! Payment can not be cancelled because being used by documents!
C02285	Attention! Do you want to cancel the present payment?
C02286	Attention! Payment hasn't been cancelled successfully! Verify data!
C02287	Attention! Want to cancel the selected line?
C02288	Impossible to save effect!
C02289	No bank receipt in the search criteria!
C02290	Attention! Line changed! Save?
C02291	Carry over document lines?
C02292	Attention! Confirm the changes to the bill of material?
C02293	Non existing item
C02294	Rule editor
C02295	EXCEL
C02296	Export Excel
C02297	SCRIPT
C02298	Process script
C02299	Custom processess
C02300	Not defined research mode
C02301	Attention! Inventory already processed impossible to change item selection!!
C02302	Attention! Filter already set for current inventary. Overwrite and load new items?
C02303	Attention, do you really want to cancel the selected inventory?
C02304	Attention inventory cancellation error!
C02305	No inventory selected!
C02306	Attention a line has been changed!
C02307	Wish to save?
C02308	Inventory altered?
C02309	Wish to save inventory?
C02310	Error while sqaving inventory database!
C02311	Attention! Impossible to process an INVALID inventory!!
C02312	Attention, wish to process current inventory?
C02313	Attention! Specify the reasons for rectifying before proceeding!!
C02314	Attention! Specify the reason of inventory before proceeding!!
C02315	Attention! Impossible to cancel the inventory movements previously inserted. The new process has beenb interupted!!
C02316	Process completed with %1 of %2 movementsmgenerated correctly!
C02317	Exportation completed in
C02318	36-Area tileable
C02319	Tiles
C02320	change replacement
C02321	#0;  |#1;3.Head office|#2;Agent and group|#3;Head office and group
C02322	|>Code|<Recipient|>Server N.|>Review|>Shop N.|>Status|Delete|>Blocked|<Date|<Type|<Info|       
C02323	Open order server n. %1?
C02324	46-Virtual Wall
C02325	Spare parts
C02326	Required field
C02327	Enable-disable magic links
C02328	Catalog will be enabled within the next 24 hours!
C02329	Sent to headquarters
C02330	Wall thickness
C02331	production launch
C02332	Not valid document
C02333	Document alreadt present on launch %1.
C02334	Subject
C02335	Document making in progress…
C02336	Attention! Commodity group parameter mandatory!! Interupted operation!!
C02337	Error while making document !
C02338	Created document nr.
C02339	Attention! Missing stored procedure %1!! Update database!!
C02340	package
C02341	Sinlge Point tracking
C02342	Set beginning…
C02343	Set end…
C02344	None
C02345	Lateral Add-on Macro
C02346	Reset
C02347	Cancel limit
C02348	Corner
C02349	Alligned linear
C02350	Linear Hor./vert.
C02351	Guide line
C02352	Giude circle
C02353	Construction
C02354	Cant rectangular
C02355	Ellypse
C02356	Rectangular
C02357	Processes…
C02358	Polygon
C02359	Generate
C02360	Reset angle
C02361	Angle:
C02362	Turn
C02363	Scale X
C02364	Reset
C02365	Open…
C02366	Radii:
C02367	Cut shape
C02368	Reference:
C02369	 mouse click + key ctrl to select the whole shape from a point on the polygon\n- mouse click+key alt to move or select a shape reference point\n
C02370	Information
C02371	Arch
C02372	Type point
C02373	Edge:
C02374	Distance:
C02375	Join diagonal
C02376	Join
C02377	Curve
C02378	Offset:
C02379	Clone
C02380	Diff. X:
C02381	Diff. Y:
C02382	Horizontallyl
C02383	Vertically
C02384	Reference in the center
C02385	Set first point
C02386	Positive
C02387	Negative
C02388	Exit modification
C02389	Clean background dxf
C02390	Clean all
C02391	General
C02392	Shape
C02393	Import Dxf
C02394	Background
C02395	Background image
C02396	eSag on file
C02397	eSag on clipboard
C02398	Hex
C02399	Export in Dxf…
C02400	Generate in Emf…
C02401	Export Dxf in eSag…
C02402	Print…
C02403	Repeat
C02404	Configure
C02405	Giude axis
C02406	Snap
C02407	Invert Y axis
C02408	Ok
C02409	Side length/ Radii
C02410	n°
C02411	Customed
C02412	dodecagon (12)
C02413	Octagon (8)
C02414	Hesagon (6)
C02415	Pentagon (5)
C02416	Square (4)
C02417	Triangle (3)
C02418	Number sides
C02419	Dimensions
C02420	Ellipse approximated to a 36 sided polygon
C02421	Radii 2:
C02422	Circle radii
C02423	Shape type
C02424	First point
C02425	Cancel shape
C02426	Mirror
C02427	Origine in the center
C02428	Save new process
C02429	Reference dimention
C02430	Y scale
C02431	Parallel distance?
C02432	In valid dimentions
C02433	In valid dimention
C02434	In valid dimention
C02435	Set at least 3 sides
C02436	Cancel select element?
C02437	Cancelation
C02438	Select an element first
C02439	Select shape to cut
C02440	Information
C02441	Invalid radii
C02442	Impossible to join this point
C02443	Cancel entire shape?
C02444	Cancel the selected point?
C02445	Edges and processes info will be lost, conitnue?
C02446	More positive shapes are present, continue only using the first?
C02447	Layer already exist
C02448	Maintain shapes?
C02449	No data to save on clipboard
C02450	Text:
C02451	Input
C02452	Turn of…
C02453	%2 module not found!%1Proceed with installation?
C02454	Installing %1 module failed!
C02455	Double quote not allowed in the email field!
C02456	Consecutive dots not allowed in the email field!
C02457	Invalid characters found in the email field!
C02458	Invalid characters (  %1  ) in the email field!
C02459	Character @ not found in the email field!
C02460	Too many @ found in the email field!
C02461	Unicode characters not allowed in the email field!
C02462	Domain extension not found in the email field!
C02463	Activation has already been executed, send another request?
C02464	Domain name not valid in the email field!
C02465	Invalid or not registered user!
C02466	Viewpoint|Resolution Width|Quality|Active|Info|Of
C02467	"1 - Low Quality|2 - Medium Quality|3 - High Quality"
C02468	[User Current]
C02469	No prospective
C02470	Camera
C02471	1 - Low Quality
C02472	Attention, all saved cameras do not use the prospective
C02473	Send in progress,  wait …
C02474	Input email adress!
C02475	Error added file!
C02476	Invalid password or not corresponding data!
C02477	User already registered!
C02478	Performed registration!
C02479	Open website for points accreditation?
C02480	Performed operation!
C02481	Error on data sending!
C02482	Void send operation?
C02483	Send
C02484	Residuo credit insufficient! Open Renderfarm website to purchase new points!
C02485	Remove the selected views?
C02486	height from ground:
C02487	Session max time reached, reinsert credentials!
C02488	Upload error!
C02489	Catalogue compression error, procedure canceled?
C02490	Decompress Zip
C02491	Wall propriety
C02492	height from ground:
C02493	Macro
C02494	Want to save
C02495	Elements search
C02496	Results
C02497	Bill of packeging
C02498	Dimensions script
C02499	Top macro
C02500	User management
C02501	ID|Feature|Deny access|Read Only Access
C02502	Catalog elaborations
C02503	Integrated cut optimizer
C02504	Apply this version
C02505	Compare
C02506	Change traceability
C02507	Select second version
C02508	Are you sure?\n\nYou are to overwrite the current version selected\n\nClick Yes to confirm No to cancel.
C02509	Version recovery error
C02510	Version Check
C02511	Canceled
C02512	Changed
C02513	Apply the selected version
C02514	Key
C02515	Copy
C02516	Bar code
C02517	Attentioon! The group is assocciated to users, cancel anyway?
C02518	Attention! Management access not active, do you want to activate it?
C02519	Class
C02520	Management Variant
C02521	Show in language
C02522	Mandatory
C02523	On Item
C02524	Variant Management
C02525	Filter
C02526	Start
C02527	Contains
C02528	End
C02529	Show order
C02530	Not
C02531	Vis./Hide
C02532	trucks
C02533	Delivery round
C02534	Attention, do you want to save?
C02535	Activate this catalogue version?
C02536	Translate
C02537	Translate
C02538	Blocked variant
C02539	No update available, the version of %1 is updated.
C02540	Unable to save not view perspective.
C02541	Version
C02542	Wait
C02543	V.Standard
C02544	PhotoMatch
C02545	V.Changes
C02546	Resource type not supported
C02547	Type|Name|Description|Catalogue|Order
C02548	Ref.|Type|Description|Status|Points|Catalogue|Created|Elaborated
C02549	Operation carried out with success
C02550	Impossible to perform %1
C02551	Confirm new password by digiting it again
C02552	Password and user invalid
C02553	Associate|Description|Active|Expiry|Temporary|Mail
C02554	Not connected
C02555	Password mandatory
C02556	Email mandatory
C02557	Invalid email
C02558	Successful registration, check email
C02559	Login
C02560	Set a description
C02561	Insufficient points
C02562	Verify company name, Status and VAT
C02563	State initials
C02564	Viewpoint|Quality|Active|Info|Of
C02565	Points: %1
C02566	Render
C02567	Accessories/Technical drawing
C02568	Language index
C02569	Automatic translation
C02570	Set catalogue language
C02571	0-No force
C02572	1-Force package
C02573	2-Force on Macro comp.
C02574	Code|...|Description|Otions|...|Description
C02575	Code|...|Description
C02576	<Module|>Copies<Printer|Parameters
C02577	Attention! Multiple print rule not found! Verify paths!
C02578	Cancel current element?
C02579	Help
C02580	Estimated points: %1npoints available: %2\nPoints remaining: %3
C02581	January
C02582	February
C02583	March
C02584	April
C02585	May
C02586	June
C02587	July
C02588	August
C02589	September
C02590	October
C02591	November
C02592	December
C02593	De-activated or expired license!
C02594	the President of the Republic
C02595	Convert Unicode DB
C02596	Quote ago./vert.
C02597	Set Text
C02598	Move shape
C02599	Move point
C02600	None
C02601	Solid
C02602	Lines
C02603	Board
C02604	Title:
C02605	- mouse click + ctrl key to select the entire shape of the polygon from a point
C02606	- mouse click + alt key to move or select landmark silhouette
C02607	Fillet with prev.
C02608	Current order is without any light. Continue?
C02609	Please activate at least one view!
C02610	Request Password
C02611	This your password for Renderfarm: @1
C02612	Check the E. Mail
C02613	E. Mail is not valid
C02614	Forgotten?
C02615	Script copied to the clipboard
C02616	Create Grouping
C02617	Break Grouping
C02618	Orders to download
C02619	Fields|Filter activations
C02620	User ID
C02621	Client Code
C02622	email
C02623	Date of maturity
C02624	Modules
C02625	Types Activation
C02626	enables
C02627	ignores
C02628	Disable
C02629	Download file
C02630	Install the module %1 and restart eCadPro
C02631	Enable-Avatar disables shots
C02632	Code|Var. Gest.|Var. Graphics
C02633	Basis
C02634	Base lines
C02635	Render Real Time
C02636	Render Real Time With Lines
C02637	white
C02638	VARIATIONS|TYPES|MACRO|SHAPES|COLOURS|EDGES
C02639	ID|Description|Value
C02640	CodiceAlias
C02641	Lancia Processing
C02642	Select graphical!
C02643	Gallery / RenderFarm
C02644	Internet connection is not available!
C02645	Create new light
C02646	Lights effects on
C02647	Compute self lightings
C02648	Move light up-down
C02649	Increase light intensity
C02650	Decrease light intensity
C02651	Delete light
C02652	Increase light temperatures
C02653	Decrease light temperatures
C02654	Editor legend
C02655	Diagonal 1000x1000 (mm)
C02656	Diagonal 39 "" 37 x 39 "" 37 (Inch)
C02657	Diagonal 100x100 (cm)
C02658	Enter a valid email!
C02659	Insert the key for charging
C02660	Website Render farm not found!
C02661	Dump DB
C02662	Address
C02663	ZipCode
C02664	City
C02665	State
C02666	Tel
C02667	fax
C02668	Accessories
C02669	Select at least one order recipient!
C02670	Proc. cancellation
C02671	NOT_VALID
C02672	Crea punto di vista per panorama
C02673	shop administration
C02674	IGT Coefficients Management
C02675	Client Code |Company |Start Date |Price |B2B Coeff |B2C Min |Max B2C
C02676	Start date
C02677	End date
C02678	option variant
C02679	Percentuale sconto
C02680	Discount Points
C02681	Variant code
C02682	Option code
C02683	IGT Discounts Management
C02684	Items list
C02685	N.Box|Code| Des.|Dimensions|Color|Notes|Buy|Special|Direct shipping
C02686	Select users to list on the left and set the following values to be applied
C02687	<Variant code|...|<Variant value|...
C02688	Forcing variants on price list
C02689	Deposits management
C02690	Phases / Department
C02691	   |Fase|...|Descr. Fase|Reparto|...|Reparti Alt.|...|Note
C02692	info
C02693	Station not enabled to check activations expiration!
C02694	Checking credentials failed: check internet connection!
C02695	eCadPro expiring activation service control
C02696	eCadPro order cancellation service
C02697	Checking credentials ...
C02698	Viewing activations expiring licenses
C02699	Orders to download Viewing 
C02700	Deleting orders...
C02701	Downloading orders...
C02702	Customer selection
C02703	Modules selection
C02704	IGNORA|NO|SI|DESCRIZIONE
C02705	CODCLIENTE|RAGSOC|RAGSOC2|IND|CAP|lOC|PROV|EMAIL|TEL|FAX|LISTINO|MAGGLIS|CODRIF
C02706	Selection
C02707	eCadLog - Help info
C02708	Order in production, can not be undone!
C02709	Request cancellation order already sent!
C02710	Order already canceled!
C02711	Cancellation processing in progress, request rejected!
C02712	Confirms cancellation order?
C02713	Remove cancellation order?
C02714	Found an update for the module% 1. Update?
C02715	Install
C02716	Background installation
C02717	%1 updates available
C02718	Ongoing updates search
C02719	Distance from previous:
C02720	Angle (degrees clockwise) from the previous:
C02721	Found temporary saving for variant 1%. Recover data?
C02722	Family
C02723	custCode|Family|FamilyDes|StartDate|B2bCoeff
C02724	Coeff. family
C02725	New snap point
C02726	Worklines edit
C02727	New orthogonal line
C02728	Make offset snap
C02729	increases number
C02730	decreased number
C02731	Code|...|Description|B2C Current|New B2C coeff
C02732	Minimum allowed:% 1
C02733	Maximum allowed:% 1
C02734	Dots editor
C02735	construction elements
C02736	Warning! Unable to establish connection with the server. Check Internet connection!
C02737	Warning! Detected Internet connection speed problems, the update may take a long time. Continue?
C02738	Create installation package
C02739	Update ready for installation!
C02740	Warning! You will work with a not updated catalog until the operation!
C02741	Single Scene JS Export
C02742	Recreate numbers
C02743	Renumber: Unique identifier box
C02744	Renumber: Item number
C02745	Renumber: Dimensions
C02746	unlock license
C02747	#Y;Yes|#N;No|#H;Owner
C02748	Roles
C02749	Actions|Flag|Id Message
C02750	Shop Id
C02751	Error saving on Cloud. Try again!
C02752	The local order number will be replaced by cloud number.
C02753	ERP Nr|Cloud Nr|Order Type|Rev| ... |Notes|DesigneId |Designer|Status Cloud|Substatus|ReqDD|ConfDD|Pdf|Path|Cli|ShopId|Id UM|Date UM|Actions
C02754	Id|Cust.Code|Email|Company|NrServer|Rev|State|Lock|Date|Notes
C02755	Actions
C02756	Notification
C02757	Azzera Conf 3D
C02758	Aggiungi Conf 3D
C02759	3D Conf
C02760	Recreate all auto views 
C02761	Delete all auto views 
C02762	local
C02763	internet
C02764	Change Base Color?
C02765	Setup Video
C02766	Setup Risoluzione
C02767	Risoluzione
C02768	Capture
C02769	Clear Clip
C02770	VARIANTI APERTE
C02771	VARIANTI FISSE
C02772	ATTENZIONE: IMPOSSIBILE UNIRE LO STESSO CARICO!
C02773	L|A|Rot|Rif
C02774	L|A|Qt|Rif
C02775	Codice|L|A|Rot|Des
C02776	|Ordine|Cliente|Rif.
C02777	ID|VAL|TEXT
C02778	transazione
C02779	Numero
C02780	Codice Iva
C02781	Chiavi
C02782	Pubblico
C02783	Messages
C02784	Your version is not the latest. Overwrite the order in the cloud ?
C02785	Confirm Ottimization
C02786	Attenzione! Ordine non rimosso dal camion! Riprovare!
C02787	Attenzione! Camion non trovato! Creare prima il nuovo camion!
C02788	Attenzione! Camion non cancellato, ordini non rimossi dal camion!
C02789	Cancellare il camion %1 ?
C02790	Attenzione! Camion n. %1 non cancellato!
C02791	Attenzione! Camion n. %1 non creato!
C02792	Attenzione! Ordine n. %1 non aggiunto al camion %2!
C02793	Attenzione! Vuoi aggiornare la data del camion?
C02794	Attenzione! Data del camion non aggiornata correttamente!
C02795	Attenzione! Vuoi aggiornare la descrizione del camion?
C02796	Attenzione! Errore aggiornamento descrizione camion!
C02797	Nessun Camion Selezionato!
C02798	Attenzione! Allengando un carico su un altro carico, gli ordini verranno spostati dal camion di origine a quello di destinazione!
C02799	Manage roles
C02800	Manage action roles
C02801	Manage email notification
C02802	Your local version have been modified,Do you want to open your local version (Yes) or the cloud version (No) ?
C02803	Ricrea Vista
C02804	File downloaded in %1. Do you want to open it?
C02805	Add current order
C02806	Warning, set ID message!
C02807	compressing database
C02808	Inconsistent backup file name!
C02809	send catalog in zip format
C02810	Processing catalog %1
C02811	Resource download 
C02812	File %1 not found at %2!
C02813	Operation aborted!
C02814	Database upload error
C02815	Schedule update
C02816	Upload mandatory database
C02817	Upload Catalog Failed
C02818	Attention, database %1 backup goes back two days ago!
C02819	Publication update failed!
C02820	Do not post setup!
C02821	Delete selected files?
C02822	Fill in both fields!
C02823	Folder %1 already exists in %2, overwrite?
C02824	Delete schedule?
C02825	Clear Active Scheduler
C02826	Error creating backup!
C02827	The database %1 is not present in the sql specified instance !
C02828	You can schedule only one upload at a time!
C02829	Check the expiration date! %1 User: %2 - Product: %3 - Deadline: 4%
C02830	Date scheduling minimum %1 %2!
C02831	Error while creating archive
C02832	Schedule not set!
C02833	Schedule set on %1 at %2
C02834	Delete file
C02835	Public update
C02836	Public update as mandatory
C02837	Set execution deadline mandatory update!
C02838	Mandatory update will be installed!
C02839	Mandatory update detected!
C02840	Dead line
C02841	Continue with installation ?
C02842	Switch off antivirus software and run with administration rights!
C02843	Nome nuova vista
C02844	Clona vista
C02845	0;All|1;Only Enabled|2;Only Disabled|3;Hide Ex-Enabled (green)|4;New to Enable
C02846	Render inviato
C02847	[USER]
C02848	memoria
C02849	Script (Extra Assign commands)
C02850	Descrizione Padre:
C02851	Offline mode is allowed for 15 days. An internet connection is required to continue.
C02852	Problem connecting to the server (error %1). %2 the program will work in offline mode!
C02853	Another workstation with the same credentials (email, hard disk serial, activation code) has logged in to the server.
C02854	Multiple access to the service using the same login credentials is not allowed!
C02855	Register using a different email, continue?
Control code not verified.
C02856	Control code not verified.
C02857	If your operating system has been reinstalled, please contact your service provider to unlock your license.
C02858	Select a valid dressing area
C02859	black
C02860	connetti
C02861	Nessuna Stanza selezionata, ordine contrassegnato come Unassigned (Non Assegnato)
C02862	Selezionare Stanza %1 per l'ordine corrente?
C02863	Vuoi davvero cancellare permanentemente l'ordine %1 ?
C02864	Cancellare %1 %2 ?
C02865	Bloccare / Sbloccare %1 %2 e tutti i suoi componenti?
C02866	Selezione non valida! La Stanza è Bloccata da
C02867	Selezionare una stanza valida
C02868	File Bloccato da:
C02869	Aggiungere una nuova Opzione per questa Stanza?
C02870	Errore! L'oggetto selezionato è già Bloccato da:
C02871	Bloccare ordine selezionato?
C02872	Sbloccare ordine selezionato?
C02873	Impossibile cancellare l'ordine 
C02874	Impossibile spostare l'ordine 
C02875	Inserisci un nuovo percorso
C02876	Il nuovo percorso specificato non esiste! Creare prima la Stanza
C02877	Selezione Stanza
C02878	Selezionare una Stanza
C02879	Gestione Contratti
C02880	Opzioni Disponibili Stanza Corrente
C02881	Ordini Non Assegnati
C02882	Contratto
C02883	Piano
C02884	Stanza
C02885	Inserisci un nome:
C02886	Clona nel percorso:
C02887	Quante volte si desidera clonare l elemento
C02888	Input non valido!
C02889	Impossibile rinominare %1 ! Il nodo selezionato o i suoi figli sono bloccati da altri utenti
C02890	Apri finestra Gestione Contratti
C02891	Scegli versione corrente dell ordine
C02892	Rinomina
C02893	Nuovo Contratto
C02894	Cancella Contratto
C02895	Blocca / Sblocca Contratto
C02896	Nuovo Piano
C02897	Cancella Piano
C02898	Blocca / Sblocca Piano
C02899	Clona Piano
C02900	Nuova Stanza
C02901	Cancella Stanza
C02902	Blocca / Sblocca Stanza
C02903	Clona Stanza
C02904	>Ordini|>Preventivo
C02905	Nome non valido
C02906	Invia Contratto
C02907	Invia Piano
C02908	Invia Stanza
C02909	Tutti gli ordini ATTIVI presenti in %1 verrano inviati alla sede. Continuare?
C02910	Spazi vuoti non consentiti nel campo
H00000A	Auto Bump
H00001A	X
H-00001A	Layer
H00002A	...
H00007A	Reflection
H00008A	BW Tex
H00009A	Bump
H00010A	Angle
H00011A	Texture dim.
H00012A	Texture
H00013A	Metallic
H00014A	Transparency
H00015A	Description
H00016A	+
H00016B	Add compartment
H00017A	-
H00017B	Remove last room
H00018A	X,Y Scale
H00019A	Rotate
H00020A	Yscale
H00021A	Xscale
H00022A	Set Background
H00023A	Walls|&Doors and Windows|&Floors|&Objects|Lights|Background
H00024A	Test
H00025A	Save
H00026B	Edit Rule
H00030A	Delete
H00033A	eMail
H00034A	Telephone
H00035A	Locality
H00036A	Address
H00037A	Company Name
H00038A	Client
H00039A	Reference
H00040A	Order n.
H00044A	Delivery &Address (if different)
H00045A	Client Address
H00046A	ID
H00047A	MODEL
H00049A	Script
H00050A	Extensive Colour
H00051A	Base Colour
H00052A	Depth
H00053A	H.
H00054A	Width
H00055A	Description
H00056A	Code
H00057A	Recalculate prices
H00058A	-General Discounts
H00061A	Net total
H00063A	Discounted Total
H00065A	Volume
H00067A	Gross Weight
H00069A	Weigth
H00071A	(of which surcharge)
H00073A	Taxable total
H00074A	Heading|Company
H00075A	Delete Errors
H00076A	Global Procedures|Catalogue Procedures|Error Control
H00077A	Catalogue Info|&Programme Manual|Business Information
H00078A	Catalogue Info|&Programme Manual
H00079A	Environment|Heading|Configurator|Quote|Attachments|Procedures|Info|Controls
H00082A	Move
H00083A	0
H00085A	Clone
H00086A	Duplicate
H00088A	Perspective
H00089A	Axonometric projection 45'
H00090A	Axonometric projection -45'
H00091A	Front
H00092A	Layout
H00093A	Total Layout
H00094A	Views
H00095A	Save miniature image
H00096A	gmFunctions
H00097A	Company
H00099A	Ok
H00100A	Recalculate price
H00102A	Item Code
H00104A	Remarks
H00105A	Qty
H00106A	Base Price
H00107A	Surch 1
H00108A	Surch 2
H00109A	Discounts
H00110A	Total
H00111A	Dimensions
H00112A	Special Flag
H00113A	Item|Price|Bill of materials
H00115C	Text1
H00119A	Configuration:
H00120A	Models
H00121A	Catalogues
H00122A	New
H00125A	Countertop
H00125B	Create Views of Top and Special Items
H00126A	Total View
H00126B	Total View
H00127A	Dimensions
H00127B	Create Views with Automatic Dimensions
H00128A	Flags
H00131A	Printout Archive
H00135A	Force Apply to All
H00136A	All Models
H00137A	Only Sel.Mode
H00138A	aaa
H00139A	Configuration
H00140A	TO Model
H00141A	From Model
H00142A	Apply|&Verify
H00143A	Lock
H00147A	Redraw
H00148A	0
H00152A	Item|Associations|Detail
H00154A	Edit &Item
H00156A	Associate to Item
H00157A	Variations &Management
H00158A	Edit &Rule
H00159A	Edit &Graphics
H00160A	Functions
H00166A	Order Date
H00167A	References
H00169A	Add
H00170A	Set Path
H00172A	Search
H00173A	Display Lines
H00173B	Display Lines
H00174A	Orders|&Search|&XML
H00175A	Cancel
H00177A	Search:
H00178A	Continue (F5)
H00179A	Next Line (F8)
H00181A	Menu
H00182A	item
H00184A	LS
H00189A	3DS
H00190A	Position
H00191A	Dim.
H00192A	Angles
H00193A	Shape
H00194A	W
H00195A	H
H00196A	D
H00197A	AXY
H00198A	AXZ
H00199A	AYZ
H00201A	Please wait.....
H00203A	Background
H00204A	Dimensions of Walls
H00205A	Other Dimensions
H00208A	Snap
H00209A	View rulers
H00210A	Display Grid
H00211A	Display Magnetic Points
H00212A	Display:
H00213A	mm
H00214A	Thickness
H00215A	Enable/Disable
H00216A	Grid:
H00223A	Scale:
H00224A	Rotate:
H00230A	Request
H00234A	Fixed corner
H00235A	Edit length
H00239A	Move object from end
H00240A	Move object from beginning
H00241A	Edit Type:
H00242A	Value (mm):
H00244A	Close
H00245A	label3
H00246A	Modules
H00249A	User
H00250A	Key
H00252A	Loading...
H00256A	E8
H00258A	Axes
H00259A	Communication
H00260A	Grid
H00262A	Logo
H00263A	E7
H00264A	E6
H00265A	E5
H00266A	E4
H00267A	E3
H00268A	E2
H00269A	E1
H00270A	Background layout
H00271A	Soft Shadows
H00272A	Shadows
H00273A	Reflections
H00274A	2D texts
H00275A	Texts
H00276A	Lights
H00277A	Materials
H00278A	Silhouette
H00279A	Lines
H00280A	Default
H00281A	Perspective Correction
H00285A	Minimum Encumbrance
H00287A	Specular
H00288A	Diffused
H00290A	Chromatic Corrections
H00291A	Settings
H00295A	Include All
H00296A	Add to the Right
H00297A	Variations Enabled
H00299A	Include
H00301A	By complete Item
H00302A	By neutral Item
H00303A	By Model
H00304A	All
H00306A	Derived Options
H00307A	Derived Variation Code
H00308A	Options to be Derived
H00309A	Variation Code to be Derived
H00311A	Variation Lim.|&Neutral Item|Model|Complete Item
H00312A	Associations
H00314A	Price lists
H00315A	Associations|Variations
H00316A	Model Management
H00317A	Variation Management
H00319A	Rules Editor
H00320A	Rule Graphic Editor
H00322A	Classification Management
H00324A	Create Automatic rules
H00325A	Reason of Non Standard Items
H00326A	Operations
H00327C	100x100
H00328C	$(.percorso)\HTML\
H00329A	Proceed
H00330A	Create by model
H00338A	Forcing
H00340A	Destination
H00341A	From Group
H00342A	TO Group
H00343A	From Item
H00344A	TO Item
H00348A	Group
H00349C	Combo3
H00354A	Item
H00355A	Generator
H00356A	Move        W                      H                       D         of Corner
H00357A	Height
H00358A	Depth
H00359A	Length
H00362A	Process
H00363A	Open
H00364A	LH RH
H00365A	Plinth
H00366A	Backsplash
H00367A	Frame
H00379A	Technical
H00380A	If Condition
H00382A	Handle
H00383A	Colour2
H00384A	Facade
H00385A	Colour1
H00389A	3D
H00391A	Comments
H00392A	Variations
H00393A	It.Code
H00394A	Language
H00395A	X9
H00396A	X8
H00397A	X7
H00398A	X6
H00399A	X5
H00400A	X4
H00401A	X3
H00402A	X2
H00403A	X1
H00404A	Other
H00415A	Serial number
H00416A	Remark 6
H00417A	Remark 5
H00418A	Remark 4
H00419A	Remark 3
H00420A	Remark 2
H00421A	Remark 1
H00423A	Quantity
H00428A	Base unit|Special|Macros|Formula|Bill of Materials
H00429A	Code / Formula
H00432A	Restore
H00435A	Shapes:
H00436A	Delete Default
H00438A	Ctrl+H =  Line Help; Ctrl+M = Edit Macro
H00439A	Parameters
H00440A	Depth
H00441A	H.
H00445A	Local
H00446A	Create Language Archive
H00447A	Settings|Backup|Languages
H00448A	Create Documentation
H00450A	Height Delta betw. Height and Height Shift
H00451A	Linking lines for Rules
H00452A	Furniture Height Lines
H00453A	Catalogues|Furnishing solutions|X|Find|Materials
H00454A	Drawing layouts
H00455A	Top Types
H00456A	Open Types
H00457A	Types list
H00458A	Down
H00459A	To
H00460A	Enter
H00462A	Ascii
H00463A	Variations List
H00464A	Items and Associations
H00465A	Edit
H00477A	Export ASCII
H00478A	Import from ASCII
H00479A	Save Set Values only
H00481A	Reset Sel.
H00482A	Sel All
H00483A	Special
H00484A	Apply
H00486A	Formula-based Price List
H00487A	Formula-based bill of materials
H00488A	Countertop Macro
H00489A	Macro
H00491A	Types
H00492A	Colours
H00494A	Export DBR
H00495A	Import BDR
H00499C	rfSpeciali. FRX
H00500A	Regenerate All
H00501A	Prepare for Internet
H00502A	Create OGG files
H00503A	Compact System Files
H00504A	Copy HTML
H00505A	Update Database files
H00506A	Saving Folder
H00507A	Catalogue SubFolders
H00508A	Heights|Type Groups|Derivation|Import/Export|Internet
H00515A	Rotation
H00516A	Di&mensions
H00517A	Orientation
H00518A	0
H00519A	Drawing
H00520A	X10
H00523A	Width
H00524A	YZ
H00525A	XZ
H00526A	XY
H00528A	Finish
H00541A	Corrections
H00549A	Type Description
H00551A	Detail
H00552A	Show in language
H00553A	Mandatory
H00554A	On Item
H00556A	Alias
H00576B	X,Y Scale
H00577B	Rotate
H00578B	Yscale
H00579B	Xscale
H00581A	Walls|&Doors and Windows|&Floors|&Objects|Lights
H00591A	Mail
H00597A	Reference/P.o.
H00609A	...
H00609B	Base Colour
H00617A	Label5
H00635A	Catalogue Info|&Programme Manual|Business Information|Tec.Information
H00637A	Roomplan|Order Header|Configurator|Quote|Attachments|Tools|Info|Controls
H00638A	bBoxR
H00639A	bBox
H00656A	zz20
H00671A	Item|Price|Bill of materials|Spare parts
H00692A	Run
H00732A	Orders|&Search|&XML
H00738A	zz
H00741A	zVoce
H00758A	|
H00765C	0
H00775C	0.0
H00777C	10
H00779A	X:
H00780A	Y:
H00808A	Key
H00811C	9999
H00823A	E5
H00847A	Environment
H00850A	Effects
H00856A	Exclude
H00888A	Cancel Bill of Material
H00912A	Exceptions
H00914A	Code Formula
H00916A	Code Generator
H00921A	Renumber
H00923A	Groups|Generate|Bill of Materials|Results
H00952A	X0
H01005A	Settings|Import/Export|Languages|Internet
H01011A	Plan-based types
H01013A	Countertop types
H01056C	Text2
H01057C	rfSetup.frx
H01059A	Load
H01066A	Heights|Type Groups|Derivation|Varianti nascoste|Varianti articolo
H01076A	0
H01116A	Class
H01117A	C.
H01119A	Rule
H01120A	Dim
H01120B	Sort by dimension
H01121A	folder
H01123A	Matrix
H01124A	Angle
H01125A	Number
H01126A	Radius
H01127A	Rotation:
H01128A	Vertical:
H01129A	Horizontal:
H01130A	Distance
H01134A	Test DB Conn
H01135A	Rules
H01136A	Delete Language Fields
H01137A	UDL
H01138A	Create DRG1 Files
H01140A	Settings|Backup|Languages|Internet|Drago
H01141A	L8
H01142A	L7
H01143A	L6
H01144A	L5
H01145A	L4
H01146A	L3
H01147A	L2
H01148A	L1
H01149A	Base unit
H01150A	C.
H01150B	Translate
H01154A	folder
H01183A	C.
H01183B	Translate
H01205C	c:\rr. drg1
H01282A	T
H01282B	Translate
H01283A	Create Drawing Page
H01284A	Recalculate Quote 
H01285A	Profile
H01286A	3:4
H01287A	2:3
H01288A	1:2
H01289A	4:3
H01290A	3:2
H01291A	2:1
H01292A	1:1
H01293A	Undo Del
H01294A	To Front
H01295A	To Back
H01297A	Vert Lin.
H01298A	3D|2D
H01299A	Horiz Lin.
H01300A	Text
H01301A	Print|Archive|Manual
H01304A	H:
H01305A	W:
H01308A	Font:
H01309A	Name:
H01310A	AttributeAfter:
H01311A	Attribute:
H01312A	Format:
H01313A	Date:
H01314A	Shape
H01314B	Shape Management
H01316A	Stop Adding
H01317A	Save DB
H01317B	Open order and re-save in DB
H01318A	Next (F8)
H01319A	Proceed (F5)
H01320A	Undo
H01321A	Print Cond.
H01322A	I Accept Conditions
H01323A	Reject
H01324A	Request Activation Code
H01325A	Activate
H01326A	Close
H01327A	Activation
H01328A	PC Serial Number
H01329A	eMail address*
H01341A	Horizontal
H01343A	Vertical
H01351A	Centre
H01352A	zoom
H01354A	Redo
H01356A	Boolean
H01357A	Command1
H01360A	Command4
H01362A	Command2
H01392A	Angle
H01401A	Connection
H01402A	Delete Database
H01403A	Create Database
H01404A	Copy Database
H01405A	Backup
H01406A	Restore
H01407A	Restore
H01409A	Copy from other Server
H01411A	Copy from remote Server
H01412A	DataBase
H01413A	PassW
H01414A	Login
H01415A	Server Name
H01416A	Backup Folder (Client)
H01417A	Backup Folder (Server)
H01418A	Catalogues List
H01419A	Only compiled files (Exluded .ERG,.JPG,.3DS,.BMP,.TGA except HTML folder)
H01421A	Catalogue Backup
H01422A	Restore Catalogue
H01423A	Copy Catalogue
H01425A	Delete Catalogue
H01426A	File Backup
H01428A	Backup Catalogues|Database|Errors|Automatic Restore|Paths
H01430A	Various
H01431A	Export Aritcles
H01432A	Articles Import
H01433A	Compile Rules
H01433B	Add saved Photos to Quote
H01434A	image
H01434B	Add current Image
H01435A	Quote Info
H01436A	Download
H01437A	Delete
H01437B	Delete selected Orders
H01438A	Open
H01439A	Find
H01440A	Destination Folder
H01441A	Send request
H01443A	Customer Code
H01444A	Email Saved
H01446A	Delete
H01447A	Send to Server
H01448A	Qualified Users
H01449A	New User
H01450A	Groups
H01452A	Prototype to delete (name with no extensions)
H01455A	Update
H01456A	Origin Folder
H01457A	Send Update
H01458A	Send Database Update
H01459A	Send HTML files
H01460A	Send complete catalog
H01461A	Select All Files
H01462A	Find
H01464A	Sending options
H01465A	Total Users
H01466A	Sent Orders
H01467A	Done Quotes
H01468A	Frame3
H01469A	View folder on Server
H01470A	Accept Guest
H01472A	Confirm Password
H01473A	New Password
H01474A	Email
H01475A	Web
H01476A	Catalogue Info
H01479A	Files on Server
H01480A	Local Files
H01481A	Local Folders
H01482A	Files Management
H01483A	Receive Orders|Activations|On-line Catalogue|Statistics|Handbook
H01484A	Confirm
H01486A	Shape Name
H01487A	Preview
H01488A	Shapes directory
H01489A	Straights Editor
H01490A	Update Parameters
H01491A	General|Documents|Selection|Level|Views
H01492A	LEVELS
H01493A	Shape|Img
H01496A	Print
H01497A	File
H01499A	Copy
H01500A	Cut
H01501A	Paste
H01502A	Select All
H01505A	Scroll
H01506A	Redraw
H01507A	Frame All
H01509A	Zoom +
H01510A	Zoom -
H01511A	Previous Scale
H01512A	Video
H01513A	Line
H01514A	Arc Tangent
H01515A	Poliline
H01516A	Flush line + Distance
H01518A	Rectangle
H01519A	Rounded Rectangle
H01520A	End + Centre
H01521A	For 3 points
H01522A	Line/Arc Tangent
H01523A	Arc
H01524A	Circle + Radius
H01525A	For 2 points
H01527A	Circle
H01528A	Ellipse
H01529A	Flush line
H01532A	Diameter
H01535A	Dimension
H01537A	Geometry
H01540A	Extend
H01542A	Offset
H01543A	Stretch
H01544A	Connect
H01545A	Round
H01546A	Explode
H01547A	Distance 2 pt
H01548A	Create Border
H01550A	Set dimensions style
H01551A	Set text style
H01552A	Set grid
H01553A	Set layer
H01555A	Grid Mode
H01556A	Orthogonal Mode
H01557A	Snap Mode
H01559A	Dump
H01564A	Search in current Row
H01565A	Search in first part
H01566A	Only &entire words
H01567A	Start from beginning
H01568A	Down
H01570A	Direction
H01571A	Uppercase/Lowercase
H01572A	Replace
H01573A	Replace All
H01574A	Replace with:
H01583A	Measure B:
H01584A	Measure A:
H01587A	New Style
H01588A	Save Style
H01589A	Delete Stile
H01590A	Style
H01591A	decimal n.
H01592A	Height Text
H01593A	Arrow dim:
H01594A	Marked
H01595A	Underline
H01596A	Italic
H01597A	Bold
H01602A	Font
H01604A	H. Text
H01605A	Text width
H01607C	Rule ????
H01616A	Division Type:
H01616B	0
H01635A	Prototype
H01637C	Dis ????
H01641A	Type
H01648A	All
H01648B	Force folder deletion and restore files
H01650A	Send
H01652A	Label1
H01653A	Overwrite
H01654C	*
H01655A	Compile Languages
H01656A	Export Rules DBR
H01657A	RGB
H01658A	Sc
H01659A	Auto
H01660A	Confirmed
H01661A	Set customer
H01662A	Group
H01663A	Divide
H01664A	Create Macro
H01665A	Create JOB
H01666A	Create Files Labels 
H01667A	Change Item
H01669A	Calculate Tops
H01670A	Edit Top
H01672A	Monthly Summary
H01674A	To date
H01675A	From Date
H01676A	Frame1
H01677A	Frame5
H01679A	Load
H01683A	Line Width
H01684A	Report img
H01690A	To Back
H01692A	Add Text
H01693A	Add &Line
H01695A	Scale 1:10
H01696A	Scale 1:50
H01697A	Scale 1:100
H01704A	Enable
H01705A	Views Management
H01708A	Variables Dump
H01709A	Download Order
H01710A	View
H01711A	Delete Order
H01712A	Load Order|View on-line
H01716A	1
H01730A	Self-Ini
H01731A	Self
H01734A	Password
H01735C	0
H01766A	test
H01771C	320
H01772C	3
H01778A	Size:
H01778B	0
H01779A	Vertical Areas :
H01779B	0
H01780A	Horizontal Areas :
H01780B	0
H01782A	Second Dimension
H01783A	First Measure
H01786A	Division…
H01786B	0
H01787A	Absolute distance:
H01787B	0
H01788A	Relative distance:
H01788B	0
H01789A	Division dim:
H01789B	0
H01794A	0.00 x 0.00
H01794B	0
H01795A	Percentage
H01795B	0
H01797A	Percentage:
H01797B	0
H01798A	Distance:
H01798B	0
H01830A	Do not Send
H01831A	Order n.
H01838A	Variation
H01839A	Item
H01841A	Option
H01845A	All
H01845B	Add all Generators
H01846A	Context
H01847A	Job
H01851A	Maistri
H01852A	Create
H01853A	Delete
H01865A	Force model change
H01866A	Script 2D
H01868A	Image
H01869A	Remark 2D
H01870A	Cartoon
H01871A	Remark
H01872A	Size
H01873A	Object
H01875A	Data
H01900A	GL
H01902A	Jobs
H01903A	Synchronize Catalogue
H01904A	Model selection
H01905A	Rule
H01906A	User data setting
H01907A	Change model variation
H01909A	Orders uploading
H01910A	Change Item Management
H01911A	Supervisor Management
H01912A	Search rule
H01914A	Catalogue
H01915A	Server n.
H01916A	Status
H01917A	User index
H01918A	User detail
H01920A	Data folder
H01922A	Points amount
H01923A	Photo
H01923B	Add saved Photos to Quote
H01924A	Modify dimension
H01925A	Copy in series
H01927A	Order transfer
H01928A	Configurations
H01929A	Set area
H01930A	Surfaces
H01932A	Code|Description|Qty|Dimensions|Varianti|Remark1|Remark2|Remark3|Remark4|Remark5|Remark6|Special|SN
H01933A	Type|Item|Description|Value|Qty|Base unit|GenCod|GenVar
H01934A	Enable
H01935A	Line|Lev|Code|Description|Qty|BasePrice|PriceIncrease1|PriceIncrease2|Dimensions|FSP|S/N|Discount|NetPrice|Variant
H01937A	Model and catalogue selection
H01938A	Variants management
H01939A	Editor Special Corner
H01944A	Colour
H01945A	Intensity
H01946A	Range mm.
H01948A	Access
H01949A	Modify Light
H01951A	Filter
H01953A	Activate Review
H01954A	Print Preview
H01955A	Effects|Layers|Advanced
H01956A	Software SetUp
H01958A	Description|Types Dyrectory
H01959A	Special Settings
H01960A	0
H01961A	Base|Extra
H01962A	Fax
H01963A	Rule Macro
H01964A	Sub Ambient
H01965A	Culling
H01966A	Pipeilne
H01969A	Frame6
H01970A	Activation Log
H01971A	Users to Activate
H01972A	Activated Users
H01974A	Frame8
H01975A	Enabled
H01976A	Customer Code Ref.
H01978A	Price List Coefficient
H01979A	Price List Code
H01983A	District
H01985A	Zip Code
H01989A	User ID
H01991A	Frame7
H01993A	Refresh
H01996A	Users|Dealers|Lic Expiry
H01997A	Local Catalogue
H01998A	On-Line Catalogue
H02006A	Update|Upload Files
H02010A	Score by Operator
H02011A	User list (txt)
H02012A	User list (xml)
H02013A	Calculate
H02015A	Score|Invoicing
H02017A	Reset
H02018A	User Guide|Errors
H02023A	Clean
H02024A	Scale 1:20
H02026A	Cancel
H02027A	Recalculate
H02028A	Images|Capture
H02034A	Load Order|Wiew on-line|Web Orders
H02037A	User info
H02038A	Retailer Logo
H02039A	City
H02046A	Company Name*
H02048A	0
H02053A	[E] Snap with other Objects
H02054A	[D] Background Sensitivity (Dxf...)
H02055A	[A] Fixed Angles
H02056A	[R] Automatic Relations
H02057A	[F] Snap
H02059C	UploadOrdine.frx
H02060A	Same
H02064B	Model directory by
H02065A	Selected Items
H02066A	All Boxes
H02068A	Re-create Index
H02069A	Models (sep ,)
H02071A	Macro Code
H02072C	Filter
H02074A	...
H02074C	rfSetup. FRX
H02076A	Archive
H02079A	To Load
H02080A	From Load
H02083A	Folder
H02084A	Load Filing|Restore
H02085A	Special
H02087A	T
H02090A	Re-Compile Bars
H02091A	Cut Dimensions for Specials
H02091B	Maximum Cut Dimension
H02092A	Standard dimensions Range
H02093A	Errore Code
H02094A	Alternative Code
H02099A	Description
H02100A	Bar Codes|Articles
H02101A	Delete Line
H02102A	Add Line
H02103A	Mix
H02104A	Management
H02105A	Formula-based
H02106A	New Line
H02109A	Verify
H02110A	Delete Bill of materials
H02111A	Bill of materials Code
H02112A	Bill of material contains hardware
H02113A	Hardware Package
H02114A	Packages in Bill of materials
H02115A	Macro Column
H02116A	...
H02116B	Bar code research
H02117A	...
H02117B	Packaging research
H02118A	...
H02118B	Variants research
H02119A	...
H02119B	fm department research
H02120A	...
H02120B	Commodity category research
H02122A	...
H02122B	Neutral item research
H02124A	H. Cod.
H02125A	Bill of materials Code
H02126A	Variants
H02127A	Supplier
H02128A	Supplier Item
H02129A	Price Lm/Sm
H02130A	Vat Code
H02131A	Bar Code
H02132A	Cost
H02133A	Other Data
H02134A	Departement
H02136A	Special Dep.
H02137A	Codes - Model
H02138A	Commercial Cod.
H02139A	neutral Code
H02142A	Length
H02145A	Modifiable L
H02146A	Weigth
H02147A	Modifiable H.
H02148A	Gross Weight
H02149A	Modifiable Depth
H02150A	Volume
H02152A	Category
H02153A	Package
H02154A	MasterData|BillOfMaterials|PriceList|PriceRises|Extra
H02155A	Copy Data from 3CAD
H02156A	Create System DB
H02159A	Database Name
H02165A	Description
H02171A	Order Selection
H02174A	Frame2
H02177A	Orders
H02178A	Carrier
H02179A	Delivery Date
H02180A	Search Filters
H02182A	Packages
H02190A	Zone
H02191A	Area
H02192A	Shipment
H02193A	Bank Information
H02194A	Additional Data
H02195A	Exchange n.
H02197A	CAB
H02198A	ABI
H02199A	Checking Account
H02200A	Square
H02201A	Bank
H02202A	Sc4
H02203A	Sc3
H02204A	Sc2
H02205A	Sc1
H02206A	Current Price List
H02207A	Commission
H02208A	Inspector
H02210A	Company Name 1
H02211A	Payment
H02212A	Invoicing Customer
H02214A	Tax Code
H02215A	VAT Number
H02216A	Mobile
H02219A	State
H02222A	Street
H02223A	Company Name 2
H02224A	Agent
H02225A	MasterData|Destination|PriceLists
H02226A	Package No.
H02229A	Create Package
H02230A	Split Package
H02231A	Delete Package
H02232A	Features
H02233A	Package
H02235A	Dim. W.
H02236A	Dim. H.
H02237A	Dim. D.
H02239A	Selection
H02242A	Order
H02248A	Table to copy from
H02249A	Create and Restore Database
H02250A	Restore Catalogue
H02255A	Screen Mode
H02256A	Print Mode
H02258A	Processes Directory
H02259A	Filters
H02260A	Loads Selection
H02268A	R1
H02269A	R2
H02271A	H1
H02272A	RealH
H02273A	- Air
H02274A	Hnom
H02275A	Oven Dimensions
H02281A	h
H02282A	h2
H02284A	Fridge Dimensions
H02285A	Fill with Pieces from same Composition
H02286A	Fill with Pieces from same Macro
H02287A	Fill with Pieces of the same Box
H02288A	Alternative Packaging
H02289A	Mounted
H02290A	No Package
H02291A	Special separate
H02292A	Forcing Package
H02293A	Packages Balancing
H02299A	Package Code
H02301A	Other Pieces to insert
H02302A	Group Labels
H02303A	Packages Flag
H02304A	Arrangement: . Progressive / + Increasing / - Decreasing / C Code / L dim l / A dim A / P Dim P / B Box / 2 Match -- Es. +A-P
H02305A	Arrangement
H02311A	Max.
H02313A	Min.
H02316A	Weight
H02321A	Create Dwg File
H02323A	Drawings|Text
H02325A	Start Compilation (Phase 2)
H02327A	Start Compilation
H02328A	Generate Programs
H02329A	Delete previous Processes
H02330A	Create Labels
H02331A	Load Number
H02333A	Delete List
H02334A	Load
H02336A	Options
H02338A	Select Orders
H02339A	Process|Drawing|Jobs|Job.Job|CFG.Drawing
H02342A	Variants|Cut
H02344A	Product Category
H02346A	Add Derivation
H02347A	Set Derivation
H02349A	Variant Options
H02351A	Variants Derivation
H02352A	Enabled Mod.
H02353A	Enables
H02356A	Change Headers
H02357A	Save Headers
H02358A	Generic Default
H02359A	Prefixes
H02360A	Default Variants Model
H02362A	MasterData|Derive
H02363A	Standard Box
H02367A	Cut again for Hole
H02372A	Recalculate Packages
H02373A	Confirm Order after Print
H02374A	Load Order
H02375A	Print Modules
H02377A	Orders Filter
H02378A	Double Row
H02379A	Recalculate
H02380A	Price List Selection
H02380C	pers.frx
H02381C	afmain.frx
H02382A	Commercial Specification
H02383A	No Walls
H02384A	Explode
H02385A	Persp.
H02386A	Render
H02387A	Save 3DS
H02388A	Env. Light
H02389A	Quality
H02390A	Gamma
H02391A	Effects|Layers|Advanced|Render
H02393A	Module 2
H02394A	Module 3
H02395A	Rendering
H02396A	Module 5
H02397A	Module 6
H02398A	Module 7
H02399A	Module 8
H02400A	Module 9
H02401A	Module 10
H02402A	Set Modules Default
H02403A	Additional modules activation:
H02404A	Frame9
H02405A	Delete User
H02406A	Update Data
H02407A	Set only selected
H02408A	Process Commercial Specification
H02409A	Edit
H02418A	End
H02419A	Body/Articles
H02420A	Introduction
H02423A	Load Orders
H02424A	Preview
H02425A	Order Type
H02429A	Sender
H02431A	Incoming|Sent
H02434A	Generic Increases
H02435A	Variants-Option
H02436A	Date
H02438A	Cuts
H02439A	E
H02439B	Edit File
H02441A	Associate File
H02458C	15
H02459A	View dispatched prints
H02461A	Seconds
H02462A	Set Timer
H02463A	Calculate Prices
H02464A	Update Costs
H02465A	Save Base Material
H02466A	Save xml order
H02467A	Save xml tops
H02468A	Save 3DS
H02469A	SkyBox
H02470A	Stats
H02471A	Volo View
H02472A	Box Culling
H02472B	Hides Boxes when not oriented toward the camera
H02473A	Face Culling
H02473B	Draw faces with positive normal
H02474A	Drag Lite
H02474B	Enabled dragging objects simplified mode: big environment
H02475A	HDR
H02476A	Effects|Layers|Advanced|Render
H02477A	Graphic
H02478B	Block divider
H02480A	Copy in gallery
H02480B	Open order and save on db
H02481A	Expand html
H02482A	Orders|Find|Xml|Web Orders|Gallery
H02484A	Local order|Web Orders|Gallery|Expo|Web Orders
H02486A	Create folder
H02487A	Delete folder
H02488A	Composition name
H02489A	Quotations
H02491A	Activations
H02496A	LINE GRAPHIC
H02497A	HISTOGRAM
H02498A	DIAGONAL
H02500A	Save DWG
H02502A	Download update
H02503A	Release update
H02504A	Delete update
H02506A	Public folder
H02507A	Private folder
H02508A	Update|Upload Files|Update program
H02509A	Check1
H02512A	Import JOB
H02513A	Export JOB
H02514A	No
H02514B	Only default
H02515A	No
H02515B	Force on Article
H02516A	No
H02524C	0.00
H02525A	Compile
H02526A	Import data
H02527A	Counters|Dimensions|Multiple codes
H02528A	View dimension
H02529A	Layer 20
H02530A	Layer 19
H02531A	Layer 18
H02532A	Layer 17
H02533A	Layer 16
H02534A	Layer 15
H02535A	Layer 14
H02536A	Layer 13
H02537A	Layer 12
H02538A	Layer 11
H02539A	Layer 10
H02540A	Layer 9
H02541A	Layer 8
H02542A	Layer 7
H02543A	Layer 6
H02544A	Layer 5
H02545A	Layer 4
H02546A	Layer 3
H02547A	Layer 2
H02548A	Layer 1
H02549A	Layer 0
H02550A	0
H02550B	Add
H02551A	0
H02551B	Delete
H02552A	OK
H02552B	Delete
H02553A	View layer
H02555A	DimH
H02556A	DimW
H02557A	Pos. X
H02558A	Pos. Y
H02559A	Recent configurations
H02560A	Catalogue|Number|Position|Date|Deliv. Date|Reference|Load|Price|Client|User
H02561A	Line|Code|Description|Des2|Prices|Price1|Price2|L|H|W
H02562A	Multi-pass effect
H02563A	Chk|Code|Description
H02564A	Packages|Associated packages
H02567A	Export
H02568A	Export DWG
H02570A	Processings
H02571A	Synchro catalogue
H02572A	Small Size
H02573A	Radiosity + Soft Shadows
H02574A	Radiosity + Hard Shadows
H02575A	Soft Shadows
H02576A	Hard Shadows
H02577A	Base Quality
H02578A	Test (Small Size)
H02579A	Quality:
H02580A	Only disabled
H02581A	Only enabled
H02582A	Show all
H02583A	eMail Notification activation(,)
H02584A	Enable additional catalogs (;)
H02585A	Version
H02586A	Export users list
H02587A	Group
H02589A	Create Avi
H02590C	400
H02591C	640
H02595A	File path
H02596A	Speed
H02597A	File name
H02598A	Y
H02599A	Img X dim.
H02600A	Synchronize gallery
H02603A	Diagonal 1000x1000
H02604A	Degrees
H02605A	Value
H02606A	a1|a2|a3|a4|a5|a6|a7
H02607A	Create Bump
H02607B	No = no forcing; C = blocked D = default only
H02650A	No CD
H02650B	No=No forcing;C=Blocked;D=Only default
H02651A	Dimension L
H02652A	Dimension R
H02653A	Point 1
H02654A	Point 2
H02655A	Point 3
H02656A	Height from ground
H02657A	Swap
H02658A	Swap 1-2
H02659A	Swap 2-3
H02660A	Swap 1-3
H02661A	Above
H02662A	Under
H02663A	Left
H02664A	Right
H02665A	List of variants not to show in the rules
H02666A	[O] Orthogonal
H02667A	Auto Layers 
H02668A	 Layer Manag.
H02669A	[L] Auto Layers
H02670A	Create Bump
H02671A	Export logic
H02672A	Table drift|Table derived
H02673A	|Description|Side|Types|Height|Alignment
H02674A	Var.Warehouse
H02675A	Unity
H02676A	Unity 2
H02677A	Conv. factor
H02678A	Warehouse Opt.
H02679A	Number of elements per direction:
H02680A	&Management Rates Classification
H02681A	&Recalculate AKA codes
H02682A	Edit AKA codes
H02683A	&Miniatures|List
H02684A	Process Rates
H02685A	Create Images
H02686A	F6: Print Bind.
H02687A	Zoom Area
H02688A	Selected
H02689A	create dwg
H02690A	Edit type
H02691A	Absolute Object Position
H02692A	Move manually
H02693A	Absolute Size Subject
H02694A	[I] Automatic insertion
H02695A	Point A
H02696A	Point B
H02697A	Point C
H02698A	Dimension Reference line
H02699A	Dimension Reference Point [C]
H02700A	-Nothing-
H02701A	Property
H02702A	Current element
H02703A	Internal
H02704A	Exterior Side
H02705A	replication series
H02706A	Job View
H02707A	Prices
H02708A	Whole words only
H02709A	Use search criteria
H02710A	Go back to end
H02711A	Select All
H02712A	Jump to
H02713A	Line number
H02714A	Total Lines
H02715A	Total Lines:
H02716A	Replace
H02717A	Replace All
H02718A	Variant | Add 3D
H02719A	Remove Option
H02720A	Adding External Color Variants
H02721A	Save base
H02722A	Add option
H02723A	SCALE
H02724A	Select Layers ...
H02725A	Choose Path
H02726A	Creating a New Catalog
H02727A	& Building Database and equalises
H02728A	Groups Editor
H02729A	Code Generator
H02730A	Managing Tables
H02731A	Managing Types
H02732A	Code | Description
H02733A	Utilities and maintenance Catalogue DB
H02734A	Repetitions (1-20)
H02735A	Contrary Reference
H02736A	Mode Strech
H02737A	Information
H02738A	Z
H02739A	Table Materials
H02740A	Start
H02741A	End
H02742A	Multi Variant Testing System
H02743A	Receive notification to sender
H02744A	Receipt notification to addressee
H02745A	Incoming orders
H02746A	Run SQL File
H02747A	Phases
H02748A	Production phase management
H02749A	Info|Cost|Script|Programs|Compartment Script
H02750A	Machine cycle programs management
H02751A	Path file machine 1
H02752A	Path file machine 2
H02753A	Data collection
H02754A	Phase/Phase macro default compartment
H02755A	Declaration
H02756A	Linked phases:
H02757A	Cost 1
H02758A	Cost2
H02759A	Annual overall cost
H02760A	Variant management
H02761A	Item variant update
H02762A	C. B.o.M.
H02763A	C. Acc. B.o.M.
H02764A	DB
H02765A	Dep. FM
H02766A	Progr.
H02767A	Var. Incr.
H02768A	Pol.Prod
H02769A	Lead Time
H02770A	Inv. Date
H02771A	Week cover/Minimum Stock
H02772A	Reorganize days
H02773A	Maximum stock
H02774A	Average consumption
H02775A	Formula Phase 1 (absolute)
H02776A	Formula Phase 2 (plant)
H02777A	Formula Phase 3 (process)
H02778A	Cost 3
H02779A	Use formulas L,A,P,Q,<var..> to declare the use per phase
H02780A	Info|Cost|Script Compartment|Optimizer
H02781A	Esternal
H02782A	Pref. Supl.
H02783A	Cost type 1
H02784A	Cost type 2
H02785A	Cost type 3
H02786A	Dimensions|Script
H02787A	Scritp for dimension
H02788A	Record|Bill of material|Price list|Increases|Extra|Stock|Movements
H02789A	Processing|Results
H02790A	Variant code
H02791A	Managerial variant code
H02792A	Formula
H02793B	Bar record
H02794B	Increases record per product category
H02795B	Packaging record
H02796B	Itemized List
H02797A	Order Ref.
H02798A	Amount
H02799A	Three side diagonal
H02800A	Side WAll 1
H02801A	Side Wall 2
H02802A	Optimizer Type
H02803A	Unloaded Doc. Type
H02804A	Subject
H02805A	Unload Stock
H02806A	Loaded Doc. Type
H02807A	Load Stock
H02808A	Optimize: phase that expect luanch of a optimizer
H02810A	Product Cat.
H02811A	Export access log
H02812A	Logins list (xml)
H02813A	Logins list (txt)
H02814A	Logins list (csv)
H02815A	Count
H02816A	Load file (csv;txt)
H02817A	Items
H02818A	Records
H02819A	Reset notice
H02820A	Force notice
H02821A	View Data
H02822A	email alerts order sent (H.O.)
H02823A	Activation Cod.
H02824A	Open Data maintenance
H02825A	Hide selection
H02826A	Show Selection
H02827A	Automatic peening
H02828A	Remove automatic peening
H02829A	Add-ons |Data maintenance|Licenses log|More
H02830A	Ratings|Exp.Users|Exp.Orders|Exp.Logins
H02831A	Users list (sql)
H02832A	Send only last changes
H02833A	Download catalogue online
H02834A	Menu|Design|Work order|Stock|Parameters
H02835A	Shelf
H02836A	Special
H02837A	H bar
H02838A	Order
H02839A	Order
H02840A	ProcTot. Time
H02841A	ProcSing. Time
H02842A	Done
H02843A	X Rotation
H02844A	Y Rotation
H02845A	B.Forw.
H02846A	B.Rear
H02847A	Graphic|Management
H02848A	Selected data|Luanch work order
H02849A	Pieces to manufacture list
H02850A	Remaining length
H02851A	Current bar
H02852A	Total Proc. Time
H02853A	Single Proc. Time
H02854A	List Proc. Time
H02855A	Both
H02856A	Only bars
H02857A	Only parts
H02858A	Only Top
H02859A	Only shelves
H02860A	List summary
H02861A	Parts list summary
H02862A	Production
H02863A	Preventive
H02864A	Simulation
H02865A	Explosion
H02866A	Code
H02867A	Save Dafault
H02868A	Template
H02869A	Password:
H02870A	F3=Processings
H02871A	Material
H02872A	Bar|Parameters
H02873A	Offset 1
H02874A	Offset 2
H02875A	Offset 3
H02876A	Offset 4
H02877A	Offset 5
H02878A	Offset 6
H02879A	Offset 7
H02880A	Offset 8
H02881A	Parameters setup|Database management
H02882A	Delete processings or previous work orders on date
H02883A	Delete processings
H02884A	Delete work orders
H02885A	Delete Bar
H02886A	Delete Stock
H02887A	Delete recovery
H02888A	Order (TTR)
H02889A	Work order
H02890A	Stock
H02891A	Stock Tops 16
H02892A	Tools
H02893A	Default\Rule template
H02894A	Import
H02895A	Price list Incr.
H02896A	Export DXF
H02897A	Prod. group
H02898A	Fix Var.
H02899A	Father Code
H02900A	Stock management fix unit of measurement
H02901A	Unit of measurement on documents
H02902A	Flag showing which unit of measurement has been used as planning default
H02903A	Cost applied on bill of material
H02904A	Processing Code
H02905A	Rep. Mon.
H02906A	Category
H02907A	Minimum stock
H02908A	Weeks of cover
H02909A	Re-order minimum supply
H02910A	Suppliers:
H02911A	Advance payments:
H02912A	Reason
H02913A	Cli/Sup
H02914A	Upload movement
H02915A	Predefined
H02916A	Company
H02917A	Capacity
H02918A	Unity
H02919A	IBAN
H02920A	&Visualization|&Generated documents
H02921A	Stock
H02922A	Ordered
H02923A	Reserved
H02924A	Availability
H02925A	To order
H02926A	Only under-stock
H02927A	Cycle
H02928A	Linked documents list:
H02929A	Only lines not entirely extracted
H02930A	Expiry year
H02931A	Expiry month
H02932A	Filter status
H02933A	Expiry filter
H02934A	Effects management
H02935A	Effect
H02936A	Creat. date
H02937A	Ref. Document
H02938A	Expiry
H02939A	Pay. Type
H02940A	ABI / CAB
H02941A	Denomination
H02942A	Receipts Management
H02943A	From Effect
H02944A	Reprint already printed
H02945A	Print selected receipts?
H02946A	&Heading|&Lines|&Foot
H02947A	Document Type
H02948A	Document nr
H02949A	Doc. status
H02950A	Subject data
H02951A	Subject
H02952A	Stock 1
H02953A	Stock 2
H02954A	Calculate delivery date
H02955A	Item cod.
H02956A	Special size
H02957A	U. M. 2
H02958A	VAT
H02959A	Unit price
H02960A	Incr.
H02961A	Open
H02962A	Qty
H02963A	Qty 2
H02964A	Qty Eff.
H02965A	Tot. Price
H02966A	Tax. Discount
H02967A	Tax. Incr.
H02968A	&Variants|&Production
H02969A	Phase
H02970A	In production
H02971A	VAT headframe:
H02972A	Totals:
H02973A	Tax
H02974A	Net Amount
H02975A	Gross Amount
H02976A	Pieces
H02977A	Period
H02978A	Set Documents Counters
H02979A	Set Counters
H02980A	Valid
H02981A	Adjustment in Date
H02982A	Mov. Adj. +
H02983A	Mov. Adj. -
H02984A	Inventory in Date
H02985A	Inventory Mov.
H02986A	Select Items
H02987A	Recalculate Stock
H02988A	Effective date
H02989A	Discount %
H02990A	Exclusion period
H02991A	Month 1 Excl.
H02992A	Days after Excl.
H02993A	Month 2 Escl.
H02994A	Month 3 Escl.
H02995A	Month 4 Escl.
H02996A	Payment procedure
H02997A	Find Items
H02998A	&Search|&Doc. Lines Withdrawn|
H02999A	Find Document
H03000A	Show Document
H03001A	Show withdrawal per phase
H03002A	Withdraw
H03003A	Doc. to withdraw
H03004A	Subject Type
H03005A	Create Accounting Movement
H03006A	Numberator
H03007A	Phase advancement
H03008A	Document that does survey
H03009A	Document for production
H03010A	Move stock
H03011A	List
H03012A	Register
H03013A	Temporary
H03014A	Reason 1
H03015A	Reason 2
H03016A	Reason 3
H03017A	Reason 4
H03018A	Reason Mov.
H03019A	Type Movement
H03020A	Document
H03021A	Analyze Movement
H03022A	Hystory Print
H03023A	Date Mov.
H03024A	Product Cat.
H03025A	Cli./Sup.
H03026A	Item Code
H03027A	View Hystory
H03028A	Consult Deposits
H03029A	Item Dest.
H03030A	Item Info
H03031A	Processed
H03032A	Last Mov.
H03033A	Unit Measurement 1
H03034A	Unit Measurement 2
H03035A	Quantity UM 1
H03036A	Quantity UM 2
H03037A	Cost/Price
H03038A	Size 3
H03039A	Movement notes
H03040A	Type Doc.|Year|Number|Line
H03041A	Filter|Process|Result
H03042A	Type Doc.
H03043A	Date Document
H03044A	1° Number Available
H03045A	Lat Number Used
H03046A	1° Free Number Used
H03047A	FORCE NUMBERING
H03048A	FORCE COUNTER
H03049A	UNIFICATION
H03050A	PAYMENT DISCOUNT
H03051A	CLIENT/SUPPLIER
H03052A	DELAY PAYMENT
H03053A	Increase
H03054A	Explode extended bill of material
H03055A	Download\Open
H03056A	Automatic Box Realign
H03057A	* required field
H03058A	Wall thickness
H03059A	Walls
H03060A	Recognition Dxf
H03061A	First distance:
H03062A	Second distance:
H03063A	Void
H03064A	Segments:
H03065A	Credit detail
H03066A	Local Mac
H03067A	Mail Key
H03068A	Record User
H03069A	Management
H03070A	Residual Credit
H03071A	Output format
H03072A	Sending in progress, wait....
H03073A	Remove view
H03074A	Open Renderfarm
H03075A	eMail notification receipt of order
H03076A	Reactivate session
H03077A	eMail notification receipt of order
H03078A	New to enable
H03079A	Assistance info
H03080A	Del. day
H03081A	Delivery day
H03082A	Show delivery day value from 1 to 7, 1=Monday
H03083A	Periodicity
H03084A	Days
H03085A	Weeks
H03086A	Period number
H03087A	Priority
H03088A	Last del. date
H03089A	Itinerary that may be unified
H03090A	Laoding day
H03091A	System Users
H03092A	ECAD Users
H03093A	Users | Groups
H03094A	Disabled
H03095A	Auto recognition
H03096A	Price on Macro
H03097A	Item Verify
H03098A	Save Item
H03099A	Check software updates
H03100A	Send to Public Gallery
H03101A	Send to Render Farm
H03102A	Export DXF
H03103A	Export PovRay
H03104A	Export Collada
H03105A	Setup Spares
H03106A	Display Spares
H03107A	!
H03107B	Update Key
H03108A	Enable Layers
H03109A	Hide-Wall Camera
H03110A	Dynamic Quote
H03111A	I
H03112A	Lights On/Off
H03113A	Add Photo
H03114A	Cancel Photo
H03115A	
H03115B	Reload
H03116A	Compare Folder
H03117A	Test Socket
H03118A	Next
H03119A	Save DDF SQL
H03120A	Reload
H03121A	Company Name
H03122A	Users Management|Send Options|References|Date Account|XX
H03123A	Date Account
H03124A	Key Words
H03125A	Movement list
H03126A	Resources
H03127A	State initials
H03128A	VAT
H03129A	Points estimation
H03130A	Filter
H03131A	Only valid ones
H03132A	Only temporary
H03133A	Only none temporary
H03134A	Order:
H03135A	Catalogue:
H03136A	All Sub-environments
H03137A	Walls
H03138A	Script management on Connect
H03139A	Integrated Edge
H03140A	Convex Shape
H03141A	End Straight
H03142A	I must Standardize
H03143A	Fix Variants
H03144A	Open Variants
H03145A	View
H03146A	Order Elaboration Management
H03147A	Catalogue Thumbnails Creation
H03148A	Catalogue Thumbnails Creation
H03149A	Compute Gallery
H03150A	Select Layer…
H03151A	Help
H03152A	Type:
H03153A	Month:
H03154A	Year:
H03155A	Reset
H03156A	Custom Processing
H03157A	Export Logic
H03158A	Export SketchUp
H03159A	Export Tops XML
H03160A	Snapshots Dimensions
H03161A	Remake Dimensions
H03162A	Save Clip on DWG
H03163A	Operator points detail
H03164A	Display hidden columns
H03165A	Objects|Layers|Setup|DXF
H03166A	Create LocDB
H03167A	Forgot?
H03168A	Generate Script
H03169A	Recipient
H03170A	Master|Additional Modules|Log Qualifications|Maintenance Data|Other
H03171A	Country
H03172A	Shop
H03173A	Pc
H03174A	Date Due. activation
H03175A	Export to file
H03176A	Check Image Quality
H03177A	Rebuild Rules and Variations
H03178A	Check Items 3D
H03179A	Fill Extra Internet
H03180A	Fill Languages ​​and HTML
H03181A	Export to Folder Saving
H03182A	Sets flags on selection
H03183A	Exclude [school bag]\File
H03184A	Types Excluding Exploded
H03185A	Dump Box
H03186A	Dump Rule
H03187A	ClipBoard
H03188A	Save macro
H03189A	Side
H03190A	Import Data From Catalog
H03191A	Type Element
H03192A	Source
H03193A	Translation Texts
H03194A	Processing eCadPro
H03195A	img
H03196A	Add User Option
H03197A	Mat Base
H03198A	Title
H03199A	Upload points
H03200A	Points
H03201A	Share
H03202A	Public
H03203A	Public object 3D
H03204A	Points insufficient
H03205A	DXF
H03206A	Coefficient | Margin
H03207A	Current coefficient
H03208A	New coefficient
H03209A	Quote margin
H03210A	Valid from
H03211A	to
H03212A	Applied to
H03213A	Code option
H03214A	Type of discount
H03215A	From pricelist
H03216A	To pricelist
H03217A	From htm model
H03218A	To htm model
H03219A	Price increase
H03220A	Add Ordine
H03221A	Share Points
H03222A	New Password (change)
H03223A	Repeat
H03224A	Change PW
H03225A	Like Search
H03226A	More..
H03227A	From Client code
H03228A	To Client code
H03229A	Local Order
H03230A	Cloud Number
H03231A	Revision
H03232A	Create new version
H03233A	Actions
H03234A	None
H03235A	Design completed
H03236A	Request revision
H03237A	Order completed
H03238A	Update status
H03239A	Notification
H03240A	Forgot Passw
H03241A	Create item
H03242A	Variants list
H03243A	3242
H03244A	Delivery week
H03245A	Lot
H03246A	Other pieces (no control)
H03247A	alternatives
H03248A	Total plans
H03249A	yield %
H03250A	Optimize
H03251A	Cloud connection not available !
H03252A	Retry Cloud Access
H03253A	Display orders local copy list
H03254A	Apri Monitor VR
H03255A	Export VR
H03256A	Crea Backup database
H03257A	memoria
H03258A	Allow Change Position
H03259A	Allow Change Angles
H03260A	Allow Change Dimensions
H03261A	Script (Extra Assign commands)
H03262A	Arg|Layers|Entities|Macro
H03263A	Orders sent to HQ
H03264A	Orders sent to Anyone
H03265A	Descrizione Padre:
H03266A	Create multi-EMF
H03267A	New Option
H03268A	Imposta Attivo
H03269A	Lock/Unlock Ordine
H03270A	Cambia Stanza
H03271A	Seleziona
H03272A	Seleziona
H03273A	Ordini|Preventivo
H03274A	Invio in Sede