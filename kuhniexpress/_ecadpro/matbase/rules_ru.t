[GRAMMAR]
LET        , ������������ �������� ����������: LET <nomevar>=<valore>[,<nome2>=valore2,..] \ ������ #LET a=1240+30 (����������� a �������� 1270) \ ������������ �������� #LETS a={1240+30}
LETS       , ������������ ��������-�������� ����������: LETS <nomevar>=Valore [,<nome2>=valore2,..] \ ��������, LETS ALFA=PROVA,BETA=1240+30 (����������� alfa �������� PROVA � BETA �������� 1240+30 (� �� ���������)
LETT       , ������������ ��������-�������� ����������: LETT <nomevar>=Valore  \ �� ��������� � LETS ����������� ���������� ����� ��������� ���� ',' ������� �� �������� ����������
LETV       , ������������ �������� ����������: LETV <nomevar>=<codicevariante> , ����������� ��� ���� ��������, ���� �� ����������.
REG        , �������� �������... REG <codicearticolo>,<variantigestionali>,<regola>,[<variante1>=<valore1>,...] \������������ � �������������� ���������� ��������� �������
M          , ����� ����� ������� �������  M <nomemacro> \ ������������ ������ � ���������� ������� ������������ � �����-������ � ��������� \ � ����������� ����� ������� ��������������, ���� ������� ��� ������� � ������ ������!
PUSH       , ��������� � stack ��������� ���������� PUSH <var1>,...[<varn>] \ ���������� � �������� POP � POPA ��� �������������� ���������� \ ������ ����������� � ���������, ������� ������������� ���������� ������� PUSH � POP
POP        , ��������������� ����������, ����������� ��������� PUSH
POPALL     , ��������������� � ������� ���������� stack ������ ��� ������� push.
GOTO       , ������� � ��������, ���������  ; ������ ������ \ GOTO <etichetta>
JUMP       , ������� � ��������, ���������  ; ������ ������ \ JUMP <etichetta> (����� GOTO) 
VB         , ��������� ������ ������ ������ VB
ENDVB      , ��������� ����� ������ VB
MSG        , ��������� ��������� � ��������� ������� \ MSG <testo>
DUMP       , ��������� DUMP ���������� � ������� output \ NB. � ����������� ����� ������������ DUMPFILE
DUMPFILE   , ��������� DUMP ���������� � ��������� ����� DUMP <nomefile>
SELECT     , ������ ����� ������ �����: SELECT <dato> \ ��� <DATO> ����� ���� ���������� ��� ����������� ������� \ ����.:  SELECT $(alfa:3)*5 
CASE       , ���������� � SELECT, CASE <valore1>:..<valoren>  \ ������ �������� ��� ��������� �������� �� : � �������� �������� �������� \ ������������ CASES {<valore1>}...
CASES      , ���������� � SELECT, CASES <valore1>:..<valoren>  \ ������ �������� ��� ��������� �������� �� : � �������� ��������-�������� �������� \ ��������� �������� � ��������� �����
DEFAULT    , ������� �� ��������� ����� ������ ���� CASE ������ SELECT
ENDSELECT  , �������� ���������� SELECT
IF         , ������� SE IF <condizione> \ ��������� ���������������� ���������� �� ENDIF ���� �������� �����, ���� �������� ����� ������� � ���������� ELSEIF ��� ELSE\ ����������� ����� � ����� ������ IF <condizione> #LET <varvero>=<valore> #LET <varfalso>=<valore>
IFDEF      , ������� ���� ���������, �.�. ����� ���� ���������� ������� <> null\ IFDEF <condizione>
IFNDEF     , ������� ���� �� ���������, �.�. ����� ���� ���������� ������� = null\ IFNDEF <condizione>
ELSEIF     , �������������� �������� �����������, ���� ���������� ���� IF ��� ������������� \ ELSEIF <condizione>
ELSEIFDEF  , �������������� �������� SE ������������ �����������, ���� ���������� ���� IF ��� ������������� \ ELSEIFDEF <condizione>
ELSE       , �������������� �������� ���� ���������� IF � ELSEIF ���� �������������� \ ELSE
ENDIF      , �������� ���������� IF
FOR        , ���������� ���������� ��� ��������� �������� \ FOR <valore1>;...;<valoren>,<condizione>,[<variabile>]  \ ��������� �� ENDFOR ��� ���� ��������, ���������� ���������� (I) ��� ���������� 
FORFILE    , ���������� ���������� ��� ������ ������ ����� \ FORFILE  <nomefile>,<condizione>,<variabile> \ ��������� �� ENDFOR ��� ���� ��������, ���������� ���������� (I) ��� ����������
EXITFOR    , ���������� ����������� ���������� FOR, FORFILE
ENDFOR     , ��������� ���������� FOR
DO         , ������ ���������� ����� DO ... LOOP   \ DO [<condizioni di uscita>]
LOOP       , ��������� ���������� ����� DO ... LOOP    \ LOOP [<condizioni di uscita>]  
BREAK      , ����������� ���������� DO.. LOOP    \ BREAK [<condizioni di uscita>] , ���� ������� ������ �� �������� �� ������ ������� 
EXIT       , ����������� ���������� DO.. LOOP    \ EXIT  [<condizioni di uscita>] , ���� ������� ������ �� �������� �� ������ �������
LOADVAR    , ��������� ������� �� ���������� \ #LOADVAR <variabile>,<variante>,<opzione>
INP        , ���������� ���������, ����� ������ �������� \ INP <nomevariabile>,<valoredefault>,<domanda da fare>
INPUT      , ���������� ���������, ����� ������ �������� \ INPUT <nomevariabile>,<valoredefault>,<domanda da fare> \ ��������� INP
DEFSQL     , ����������� ���������� ���������� ������� (�� ������������ ��� ������ LITE!) \ DEFSQL <nomevar>=<comandoSQL>
SQL        , ����������� ���������� ���������� ������� ��� ��������� ������� SQL \ �� ������������ ��� ������ LITE! \ SQL <nomevar>=<comandoSQL> ��� SQL <comandoSQL>
SQLFILE    , ��������� ������ ������, ������������ � ����� SQL \ �� ������������ ��� ������ LITE! \ SQLFILE <nomefile>
FORDBS     , ��������� ���� ��� ���� ��������� ����� � ������� ������� SQL \ FORDBS <nomevar>=<comandosql> \ ����� �������� ������ � ����� ����� ������������ ��������� ������ ��������� \ �� ������������ ��� ������ LITE!
ENDDBS     , ��������� ���������� FORDBS 
FORTAB     , ��������� ���� ��� ���� ��������� ����� ��������� ������� \ FORTAB <nomevar>=<nometabella> \ ����� �������� ������ � ����� ����� ������������ ��������� ������ ��������� \ �� ������������ ��� ������ LITE!
ENDTAB     , ��������� ���������� FORTAB
ORDERVARS  , ��������� ��������� � ��������� �� �� ���������� pos. (forma COD=...*POS=...*var=...*...)
ORDERVAR   , ��������� ��������� � ����������� ������� ���������� � ��������������� ���������
SETVARS    , ����������� ���������� ���������� ��� cadmensole
SETVAR     , ����������� ���������� ������ ��� cadmensole
SUBVARS    , ����������� ���������� ��� sub aggiunta: \ SUBVARS <nomesub> [<nomevar>] 
XML        , ��������� ���� XML � ���������� ������������� ����������: $(.xml.nodo.nodo...)
XMLROOT    , ����������� ������ ������������ ����� XML \ ������������� ����������: $(.xml.nodo.nodo...)




[ENDGRAMMAR]

[FUNCTIONS]
JOB         , !JOB <nomefile>;...  ���������� ������������� ��������� �� ������� ����� ������ ��� ������, ������������� � ���� ����������
DIM         , !DIM <nomedim> <elencodimensioni>  \ ��� ������������� � 3CAD Classic. ����������� ������ ��������. \ ����������
MODELLO     , !MODELLO <nomevar>  \ ����������� <nomevar> ��� ������� ������
NEUTRO      , !NEUTRO <nomevar> \ ����������� <nomevar> ��� ������������ �������� �������
GETCOMMENTO* , !GETCOMMENTO<n> <nomevar> \ ����������� NomeVar ������ <n> ����������� ������� \ ���� ������ <n> ����������� ���� �����������
CODICE      , !CODICE <nomevar> \ ����������� <nomevar> ��� �������� �������
GENCOD      , !GENCOD <variabile>,<forzacrea>,<coderrore>,<generatore>,<parametri> \ ����������� <varibile> ���,  ��������������� ����������/���������� � ���������� �������� � <genera>\���� �� ������� ��� �� ����� ������������ ���������� <coderrore>\ ��������� ���������� |
SETCODICE   , !SETCODICE <codice> \ ����������� �������� ����� ����� ���, ������������  <codice> � ���������� ���������� ���� � ��������
FLAGSPEC    , !FALGSPEC  <BYTE>  \ ����������� 4-�� ���� �� ��������� �� �������� ���������� � <BYTE>
SETREG      , !SETREG <dati>    \ ����������� ���������� _REGOLA ������, ���������� ��� \
                                 �������� ������� � ������ id �����. ������������� ����������� �������.
BARRA       , !BARRA <nomevariabile>,<codicebarra>,<l>,<a>,<p>	 \ ����������� nomevariabile �������� ����, ������������ � \
              <codicebarra> � ������� <l>,<a>,<p>
DIMBARRA    , !DIMBARRA <nomevariabile>,<codicebarra>,<dim>,<chedim>   \
              ����������� nomevariabile ���������� �������� � ������� <chedim>: \
              L  -> ������ L >= <dim> \
              L- -> ������ L <= <dim> \
              A  -> ������ A >= <dim> \
              A- -> ������ A <=	<dim> 		  
LOOK        , !LOOK <nomevar>,<misura>,<elencomisure>,<fm> \ ������� ������� ����������� ���������� ���������� ������ �������� ����� � ������� ������� \
              <nomevar>-  -> ������ �������� ����� \ 
			  <nomevar>+  -> ������ �������� ������ \ 
			  <nomevar>_TOT- ->��������� �������� ����� \ 
			  <nomevar>_TOT+ -> ��������� �������� ������ \ 
			  <nomevar>_TOT -> ��������� ��������
PLACERPAR   , !PLACERPAR <variabile>,<nomeparametro> \ ����������� ���������� �������� ��������� <nomeparametro> (1-10) ��������-��������
PLACERBOX   , !PLACERBOX <variabile>,<nomevarbox>    \ ����������� ���������� �������� ������� ���������� (LOC_BX<n>) ����� ��������-��������.
PLACERVAR   , !PLACERVAR <variabile>,<variabilebox>  \ ����������� ���������� �������� ������� ���������� (LOC_BX<n>) ������� �����-�������  \  ��������� ; � �������� �����������, ����� ������������ �� ������ FOR
TXT         , !TXT <nomevar> <testo> \ ����������� <nomevar> ���������� <testo> \ ��� ������� �����, ����� �������������� ��������������� ����� ����� ������ \ � ������� �������� �� �����-���� �����!
COMMENTO    , !COMMENTO<n> <descrizione> \ ����������� ����������� <n> ����� 
LOADPLACER  , !LOADPLACER \n ���������� �������������� �������� ��������� ��������������
LASER       , !LASER <variabile>,<p1x>,<p1y>,<p1z>,<p2x>,<p2y>,<p2z> \ ������ ����� �� P1 �� P2 � ������ � <variabile> ���������� ���������� ������� �������� \
               NB. �� ����� ������� ���� �� BOX. 
LASERGRID   , !LASERGRID <variabile>,<p1x>,<p1y>,<p1z>,<p2z>,<ripx>,<distx>,<ripy>,<disty> \ ������ ����� ����� �� P1 �� P2 (� ��������� XY) \ ������� � P1 � n ������������ �� ���������� dist \
			   � ������ � <variabile> ���������� ���������� ������� �������� \
               NB. �� ����� ������� ���� �� BOX � ����� ������ ������� ��������. 
BOXVICINO   , !BOXVICINO <nomevariabile>,<elencolati>  \ !! ���������� !! 
VARIANTE    , !VARIANTE <variabile>,<variantegest>,<solocodice> \ ������ � ���������� �������� ���������� ��������������� �������� ������� ��������
MTINIT      , !MTINIT  \ �������������� ������� ��� �������
MTTRASLA    , !MTTRASLA <X>,<Y>,<Z>,<AY>,<AY>,<AZ>  \ ���������� � ������� ������� ��� �������
STAMPASPECIALI , !STAMPASPECIALI <FLAGS> \ ��������� ������� ������ n ���������� SPECIALI, �������� 16 ���� \ ����. !StampaSpeciali 000100000 ��������� 4-�� ����
PM          , !PM <verso> \ ����������� ����������� ����� (1 ��, 3 ��)
ADDLINK*    , !ADDLINK<n> <descrizione>,<lato>,<pl>,<pa>,<pp>,<angolo> \ ����������� �������������� ������ ��� ������ ���� <n>. \ �������� ����������  � ����������� ���� ������ \
               ������� e un resto di 3CAD classic � ����� ����� ������ ��������: \
			   101 - �� , 198 ���������� , 201 �� � �.�. � �����, ����� ���������� ����� ���������� 
ADDMAGIC*   ,  !ADDMAGIC<n> <p1x>,<p1z>,<p2x>,<p2z>,<alt>   \ ������� "Magic Link" ��� ������ ���� <N>, ������� ��������� ��������� �������� ����� �� ����� \���� �� <p1> �� <p2> \ � ������������ �������
MAGICBREAK  ,  !MAGICBREAK [0-1] \ ���������� ���� ���������� ����"magic Link" ���� �������� ������ ��������������� ��������������
IMGSCALA    ,  !IMGSCALA <NOMEVAR>,<FILEVAR> \ ������ � nomevar ��������� ����� ������� � ������� �����������  <filevar>
AZZERA      ,  !AZZERA  \ �������� ��� ������, ����������� � ADDLINK
SCEGLIADDLINK, !SCEGLIADDLINK  \ ���������� ������ ������ ��� ������ �������� �������
TIPOBOX     ,  !TIPOBOX <n> , ����������� ������ ���� ����� �� <n>. �������� ��� addlink.
HMIN        ,  !HMIN <var>,<spostal>,<spostaa> \ ����������� (� ���������): ������ � ���������� S1 ���������� �� ���� � ��������� ����� � ���� \ ��������� ������� � ���������� <var>_ang
HMAX        ,  !HMIN <var>,<spostal>,<spostaa> \ ����������� (� ���������): ������ � ���������� S1 ���������� �� ������� � ��������� ����� � ���� \ ��������� ������� � ���������� <var>_ang
LMIN        ,  !HMIN <var>,<spostal>,<spostaa> \ ����������� (� ���������): ������ � ���������� S1 ���������� � ����� ������� � ��������� ����� � ���� \ ��������� ������� � ���������� <var>_ang
LMAX        ,  !HMIN <var>,<spostal>,<spostaa> \ ����������� (� ���������): ������ � ���������� S1 ���������� � ������ ������� � ��������� ����� � ���� \ ��������� ������� � ���������� <var>_ang
SAGOMA      ,  !SAGOMA <var> <elenco punti separati da ,> \ ������� ������ � ���������� �  � ���������� VAR ����������� ��� ���
SAGALIGN    ,  !SAGALIGN  <newsag> <VARPOS> <inverti> oldsag \ ������� ������, ��������������� �� ��� x: � ������������� � 0,0 � ������ � varpos px,py � ����
SAGMIRROR   ,  !SAGMIRROR <newsag> oldsag <tipomirror: X-Y-XY> \ ������� ������, ������� �������� �������� ������ �� ��� X, Y ��� �� �����
SAGOFFSET   ,  !SAGOFFSET <newsag> oldsag valoreoffset  \ ����������� ������� ������� ������������� �������� offset
SAGOMAINT   ,  !SAGOMAINT <var>,<spostal>,<spostaa>,<l>,<a>  \ ����������� (� ���������): ������� � VAR ������, ��������������� �������� �������� ���������� ������������ � ��������
SAGSPOSTA    , !SAGSPOSTA <newsag>,oldsag,x,y,angolo \ ���������� ����� oldsag � newsag, ��������� ��� ����� �, � � ���� ����.
SAGCREA     ,  !SAGCREA <nomevariabile> <prefisso> \ ������������� ��� ����������� ���������!
SAGOFFY     ,  !SAGCREA <newsag> oldsag valore \ �����, �������� ������ � ������� Y (�������� �������� �������
SAGPERCX    ,  !SAGPERCX <variabile>,posx,sagoma \���������� ������� �������, ����� �� �������� ����� ����� � (������ ������� �������� ����.)
SAGPERCY    ,  !SAGPERCX <variabile>,posx,sagoma \ ���������� ������� �������, ����� �� �������� ����� ����� y (������ ������� �������� ����.)
SAGDUMP     ,  !SAGDUMP <variabile>,sagoma \������ � ���������� ������ ����� �������
SAGUNION    ,  !SAGUNION <variabile>,sagoma1 sagoma2 \���������� ������� 1 � 2 
SAGDIFF     ,  !SAGDIFF  <variabile>,sagoma1 sagoma2 \ �������� sagoma2 �� sagoma1
SAGCLIPX    ,  !SAGCLIPX <variabile>,sagoma,x1,x2  \�������� ������ � x �� x1 �� x2
SAGCLIPY    ,  !SAGCLIPY <variabile>,sagoma,y1,y2  \ �������� ������ � y �� y1 �� y2
SAGCLIPY    ,  !SAGCLIPY <variabile>,sagoma,y1,y2  \ �������� ������ � y �� y1 �� y2
SAGCLIPL    ,  !SAGCLIPL <variabile>,sagoma,x1,[x2] \������������� ������ ����� ����� �������
SAGCLIPR    ,  !SAGCLIPR <variabile>,sagoma,x1,[x2] \������������� ������ ������ ����� �������
SAGCLIPU    ,  !SAGCLIPU <variabile>,sagoma,y1,[y2] \������������� ������ ������� ����� �������
SAGCLIPD    ,  !SAGCLIPD <variabile>,sagoma,y1,[y2] \������������� ������ ������ ����� �������
SAGCLIPT    ,  !SAGCLIPD <variabile>,sagoma,dist \���������� ������������� ������ ������� � offset dist
SCEGLIVARIANTE,!SCEGLIVARIANTE <nomeVariante>  \ ���������� ����� �������� (����������� � �������) ��� ����������
YSAGOMA     ,  !YSAGOMA <var>,<puntox>,<sagoma> \ ������ � ���������� var ����� Y, ��������������� ������������ ����� X �������
YSAGOMAMIN  ,  !YSAGOMAMIN <var>,<puntox>,<sagoma> \ ������ � ���������� var ����� Y, ���������������  ����������� ����� X �������
YSAGOMAMAX  ,  !YSAGOMAMAX <var>,<puntox>,<sagoma> \ ������ � ���������� var ����� Y, ��������������� ������������ ����� X �������
XSAGOMAMIN  ,  !XSAGOMAMIN <var>,<puntoY>,<sagoma> \ ������ � ���������� var ����� X, ���������������  ����������� ����� Y ������� 
XSAGOMAMAX  ,  !XSAGOMAMAX <var>,<puntoY>,<sagoma> \ ������ � ���������� var ����� X, ��������������� ������������ ����� Y �������
LAMIERA     ,  !LAMIERA<n> <var>,spessore,sagoma/el.punti \ �������� ��� ������� sagoma � �� ��������� ������, ������������� ����������  <spessore>
QUOTA       ,  !QUOTA<layer> (x1;y1;z1),(x2;y2 z2),<testo>,<offset>,,<scala> \ �������� �� ������
QUOTAJ       , !QUOTAJ<layer> (x1;y1;z1),(x2;y2;z2),<testo>,<offset> \ ����������� ��������
QUOTAP       , !QUOTAP<layer> (x1;y1;z1),(x2;y2;z2),<testo>,<offset> \ �������� �����
QUOTAT       , !QUOTAT<layer> (x1;y1;z1),(x2;y2;z2),<testo>,<offset> \ �������� ��������
QUOTAC       , !QUOTAC<layer> (x1;y1;z1),(x2;y2;z2),<testo>,<offset>,,<scala> \ �������� ������ ������ � ������
QUOTAL       , !QUOTAL<layer> (x1;y1;z1),(x2;y2;z2),<testo>,<offset>,,<scala> \ �������� ������ ������ �����
QUOTAR       , !QUOTAR<layer> (x1;y1;z1),(x2;y2;z2),<testo>,<offset>,,<scala> \ �������� ������ ������ ������
QUOTAANGOLO  , !QUOTAANGOLO<layer> (x1;y1;z1),(x2;y2;z2),(x3;y3;z3) <testo>,<offset> \ �������� ����
QUOTARIF     , !QUOTARIF<layer> (x1;y1;z1),(x2;y2;z2),<testo>,<offset> \ ������������ ��������
VB           , !VB <parametri> \ ��������� �� Ambiente.VBS ������� ControllaRegola(���������)
ALTEZZE      , !ALTEZZE  <idtabella>  \ ����������� ������� ����� <idtabella>
INVERTILATO  , !INVERTILATO \ �������� ������� ����� �� ����� �������: �������� ��� �������������� �����
AZZERAINVERTILATO, !AZZERAINVERTILATO \ ��������������� ������� ����� �������
SAGTRACCIA   , 
ADDPUNTO*    , !ADDPUNTO<livello> x,y,z \ ��������� ����� ��������
ADDLINEA     , !ADDLINEA x1,y1,z1,x2,y2,z2 \ ��������� ����� ��������
AZZERAPUNTI  , !AZZERAPUNTI <n> \ �������� ����� ��������; <n> ���������, �������� �� ����� ����� �� ���������
OFFSET       , !OFFSET
DAMMIPUNTO   , !DAMMIPUNTO <VX>,<VY>,<VZ>,x,y,z \ ������ � ���������� VX,VY,VZ ���������� �������� � ������������ ����� X,Y,Z \ ��������, ��� �������� ��������  � ������������� \����������!!
PARETE       , !PARETE <parametri>  
PARETEOPPOSTA, !PARETEOPPOSTA <parametri>
MATHAZZERA   , !MATHAZZERA \ �������� ������� ������� ������ 
MTCOLLEGA    , !MTCOLLEGA x1,z1,x2,z2,angolo \ ������� ������� ������, ����������� ����� X1,Y1 � X2,Y2 � ����� <angolo>
INTERSECA    , !INTERSECA <X1>,<Y1>,RETTA1,RETTA2 \ ��������� � x1,y1 ����� ����������� ������ ����� retta1 � retta2
DISTANZA     , !DISTANZA <VAR>,x1,z1,x2,z2   \ ������ � VAR ���������� ����� 2 �������
RETTA        , !RETTA <id>,<tipo>,<distanza>,<suboxc> \ ������� ������ ����� 
PUNTO        , !PUNTO <varx>,<vary>,x,y   \ � ������� MTCOLLEGA ���������� ����� x,y � ���������� <varx> � <vary>
PUNTOL       , !PUNTOL <varx>,<vary>,x,y  \ � ������� ������������ ����� ���������� ����� x,y � ���������� <varx> � <vary>
SAGREL       , !SAGREL <newsag>,oldsag,x,y,angolo \ ���������� ������ oldsag � newsag, ��������� ��� ����� x,y  � ���� ����.
PUNTOREL     , !PUNTOREL <varx>,<vary>,oldx,oldy,x,y,angolo \ ���������� ����� (oldx,oldy) oldsag � var, ��������� x,y  � ���� ����.
TREL         , !TREL <varx>,<vary>,oldx,oldy,x,y,angolo \ ���������� ����� (oldx,oldy) oldsag � var, ��������� x,y  � ���� ����.
MAXMIN       , !MAXMIN <mix>,<miy>,<max>,<may>,x,y  \ �����������, ��� x,y ������ MI,MA 
RETTA2       , !RETTA2 id,X1,Y1,X2,Y2,DISTANZA,angolo    \ ������� ����� ������ �����, ������������ ������ � ���������� ����� 2 ����� , ��� � �����
SAGRETTE     , !SAGRETTE <SAGOMA>,<PARAM>,retta1;retta2;..rettan;rettan+1 \ ������� ��������������� ������, ���������� ����� ����� � ���������� ����������� ������������� ������ �����
ANGOLO       , !ANGOLO <var>,x1,z1,x2,z2 \ ���������� ���� ����� 2 ��������� (x1,z1) e (x2,z2)
SAGLEN           , !SAGLEN <var>,sagoma \ ���������� ������ ���� �������
SAGXY            , !SAGXY  <varx>,<vary>,coeff,sagoma \ ���������� ����� x,y ��������������� coeff � ������� (coeff �� 0 �� 1)
SAGMID           , !SAGMID <newsag>,coeff1,coeff2,sagoma \ ���������� ����� ������ �� ������ �������������, ������� � coeff1 �� coeff2 (�������� ����� 0 � 1)
RETTAPERPUNTO    , !RETTAPERPUNTO idnuova,idrif,px,py    \ ������� ����� ������ �����, ������������ idrif, ���������� ����� px � py
RETTAPROJ        , !RETTAPROJ <px>,<py>,idretta,px1,py1  \ ����������� ����� px1,py1 �� ������ � ���������� �������� � ���������� px,py
RETTARUOTA       , !RETTARUOTA idnuova,idrif,angolo      \ ������� ������ idrif �� ���� � ������� ������ idnuova
PARALLELA        , !PARALLELA idnuova,idrif,distanza     \ ������� ����� ������, ������������  idrif �� ����������
CERCHIO          , !CERCHIO centrox,centroy,raggio  ��� !CERCHIO p1x,p1y,p2x,p2y,p3x,p3y \ ������� ����, ������� � ������ � ������� ��� ���������� ����� ��� ����� 
CERCHIORAGGIO    , !CERCHIORAGGIO <raggio>  \ ���������� ������ ������������ �����
CERCHIOCENTRO    , !CERCHIOCENTRO <cx>,<cy> \ impostato ���������� ����� ������������ �����
CERCHIOTANGENTE  , !CERCHIOTANGENTE idlinea,angolo  \ ����������� ������ �����, ����������� � ����� � ������������� ����
CERCHIONORMALE   , !CERCHIONORMALE  idlinea,angolo  \ ���������� �����, ����������� �� ������ ����� � ������������, ������������ �����
CERCHIOPUNTO     , !CERCHIOPUNTO    <px>,<py>,angolo \ ���������� ����� �� ����������, ��������������� ����
RETTAPUNTO       , !RETTAPUNTO    <px>,<py>,id,distanza \ ���������� ����� �� ����� � �����������, ��������������� ������������ ������
RETTALEN         , !RETTALEN   <lunghezza>,id  \ ���������� ����� ��������, ������������ ������ ����� (������ ����� ������ ������������ 2 �������)
ARCORAGGIO       , !ARCORAGGIO <PARCOX>,<PARCOY>,X1,Y1,X2,Y2,RAGGIO,DIR \ ���������� ����������� ����� �� ���� � ����������� (x1, y1) � (x2, y2) � � �������� (r): dir ������ ����������� ����
ARCOCORDA        , !ARCOCORDA  <PARCOX>,<PARCOY>,X1,Y1,X2,Y2,CORDA ,DIR \ ���������� ����������� ����� �� ���� � ����������� (x1,y1) � (x2,y2) � � ������ (c): dir ������ ����������� ����
FRESA            , !FRESA <nomesagoma>,<diml>,<dima>,[<sagoma;lato;puntox;puntoy;rot],... \ ������� ������, ��������� ������� Fresa, �.�. �������� ������� ������� �� ��������������.
LIVAZZERA        , !LIVAZZERA [<nomevar>,valini] \ �������� ���� � ������ ��������� � ���������� _LIV ��� � ��������� ����������
LIVADD           , !LIVADD    [<nomevar>] \ ��������� ������� � ������� � ������ ��������� � ���������� _LIV ��� � ��������� ���������� 
LIVPUSH          , !LIVAZZERA [<nomevar>,valini] \ ��������� ����������� ������� �� ���� ������� � ������ ��������� � ���������� _LIV ��� � ��������� ���������� 
LIVPOP           , !LIVAZZERA [<nomevar>] \ ��������� ������� �� ���� ������� � ������ ��������� � ���������� _LIV ��� � ��������� ����������
LIVSET           , !LIVSET valori \ ����������� ������ �� ���������� �������� (��������� ���� n.n.n... n �������)
GETTIPOFM        , !GETTIPOFM <variabile> \ ����������� �� ���������� �������� ����FM, ��������� � ��������
TIPOFM           , !TIPOFM <tipofm> \ ����������� ������ ��� FM, ��������� � ��������.
SGCLEAR          , !SGCLEAR [livello] \ ������� ��� �����������
SGADD*           , !SGADD<livello> order,code,l,a,p,varianti,codice,note \ ��������� ��� ������ ���  �����������
SGELAB*          , !SGELAB<livello> <nomevariabile> \ ������������ ����, ��������� �����������
SGGET*           , !SGGET<livello> <nomevariabile>  \ ������������ ���� ��� �����������
PUNTOABS         , !PUNTOABS <NOMEVAR>,X,Y,Z    \ ���������� ����� (x,y,z) �� ���������� ����������� �����
RESETQUOTE       , !RESETQUOTE           \ �������� ������� ����������� ��������
DISTABS          , !DISTABS <NOMEVAR>,X1,Y1,Z1,X2,Y2,Z2 \ ������������ ���������� ����� ����� �������         
PUNTOYARCO       , !PUNTOYARCO [<var>],<rifx>,<px1>,<py1>,<px2>,<py2>,<px3>,<py3> \ ���������� ���������� y �� ���� � ������������ ��������� <rifx>

[ENDFUNCTIONS]


[INLINE_FUNCTIONS]	      //������������ � ����� $[	   
SCONTI      , $[SCONTI;par1...parn] \ ��������� ������ � ����� 50+5+2...
SCONTO      , $[SCONTO;par1;par2..] \ ����������  ����������� ��� ��������� ��� �� �������, ����� �������� �������� �� �������
DELTAVAR    , $[DELTAVAR;vara;varb] \ ���������� �������� vara, ������� ��� � varb \ � ����� var1=val1,...
STAMPASPECIALI , $[STAMPASPECIALI;maschera;id] \ ���������� ��� �� �����, ������� �� ������
WEEK        , $[WEEK;<data>]  , ���������� ����� ������ ������� ���� \(1-�� ������ ������)
WEEK1       , $[WEEK;<data>]  , ���������� ����� ������ ������� ���� \(1-�� ������ ������ ���� �� 4 ���)
CLAMP       , $[CLAMP;valore;valminimo;valmassimo] \ ������������, ��� �������� ��������� ����� valminimo � valmassimo
EX          , $[EX;stringa;separatore;id] \ token id ������ �� ��������� � �����������
TK          , $[TK;sep;id;stringa]        \ token id ������ �� ��������� � ������. (����� ��, ��� � � EX, �� � ��������������� �������: ���� ����������� ������. �������� ;
DIM         , $[dim;l;a;p;fm]             \ ���������� ������, ����������������� ��� ��������
DATA        , $[DATA;data;giorni]         \ ��������� �����. ���-�� ���� ����������� ����
LIRE        , $[LIRE;valorelire]          \ ���������� �������� � ����
EURO        , $[EURO;valoreeuro]          \ ���������� �������� � �����
CHOOSE      , $[CHOOSE;nomvariante;valore] \ ������������ #letV, ���������� ������ ������ �������� � ����� <valore> 
TOSEP       , $[TOSEP;sep;stringa]        \ ���������� ����������� <sep> � TAB, ����� ��������� ����������� ��� �������
TOTAB       , $[TOTAB;stringa]            \ ���������� ����������� ; � TAB, ����� ��������� ����������� ��� �������
LOOK        , $[LOOK;variante;valore]     \ ���������� �������� = ��� ���� ���������� �������� (������ DB!)
LOOKB       , $[LOOKB;variante;valore]    \ ���������� �������� = ��� ���� ���������� ��������
INSTR       , $[instr;str1;str2]          \ ���������� ������� STR2 � STR1 (0 ���� �� �������)
IIF         , $[iif;cond;valvero;valfalso] \ ���������� valvero ��� valfalso � ����������� �� ���������� �������
EXIST       , $[exist;nomefile]           \ ���������� 1 ���� ���� ����������, ����� 0
LEN         , $[len;stringa] \ ���������� ����� ������
REPLACE     , $[replace;stringa;strcerca;strsost] \ �������� � ������ strsost � strcerca
CODICEBARRA , $[codicebarra;barra;l;a;p]  \ ���������� ��� � ���������� ���� Barra
MID         , $[mid;stringa;dapos;apos]   \ ������������ ������� VB ��� ��������
ARROTONDA   , $[arrotonda;importo;coeff]  \ ��������� ����� �� ������������� ���������� ������ ����� �������
FORMAT      , $[format;numero;formato]    \ �������������� �����
FORMATV     , $[formatv;numero;formato]   \ �������������� �����
CODVAR      , $[CODVAR;<nomevariante>]    \ ���������� �������� ������������ ���� ��������
VAR         , $[VAR<formato><lingua>;<Num/Nome Variante;<elenco varianti>] \ ���������� �������� ��������� ����� ���������� ������������� �������� 
VAROPZ<n>   , ���������� �������� ��������� ����� ���������� ������������� ��������	
VARIANTE    , $[VARIANTE;<cod.variante=cod.opzione>;<formato>] \ ���������� �������� ��������� ����� ���������� ��������
SQL         , $[SQL;sql] \ ��������� ������� SQL � ���������� ����������� ������ 
DATEFORMAT  , $[DATEFORMAT;<DATA>;<INCREM>;<UNITA'>] \ ����������� ���� , �������� �� �� <increm> unit� \ unit� ?  "d" ����, "m", ����� � "yyyy" ���
DATECOMPARE ; $[DATECOMPARE;<DATA1>;<DATA2>] \ ���������� 2 ����, ��������� -1 ���� d1<d2 , 0 ���� ����� � 1 ���� ����
DATEDIFF    ; $[DATEDIFF;<DATA1>;<DATA2>;<UNITA'>] \ ������������ unit� (d,w,m,yyyy) ������� ����� d1 � d2
QTYFM       ; $[QTYFM;<fm>;<dim1>;<dim2>;<dim3>] \ ���������� ���-�� 1 ��� �.� ��� �? � ����������� �� ������ fm
DISTANZA    ; $[DISTANZA;X1;Y1;Z1;X2;Y2;Z2]      \ ���������� ���������� ����� 2 �������
[ENDINLINE_FUNCTIONS]
 
[FUNCTIONS_2D]
RECT        , !!Rect x,y,w,h,[color],[penStyle],[penw]   \ ������ �������������
LINE        , !!Line x,y,x2,y2,[color],[penStyle],[penw]   \ ������ ����� 	
CIRCLE      , !!Circle x,y,r,[color],[penStyle],[penw] \ ������ ���� 
ARC         , !!Arc x,y,r, startAngle,endAngle,[color],[penStyle],[penW] \ ������ ���� 
PIE         , !!Pie x,y,r, startAngle,endAngle,[color] \ ������ ������ ����
FILLRECT    , !!FillRect x,y,w,h,[color]                \ ������ ������ �������������
FILLCIRCLE  , !!FillCircle x,y,w,h[color]              \ ������ ������ ����
FILLPOLY    , !!FillPoly color,x,y,x1,y1,x2,y2....     \ ������ ������ ������� �����
STRING      , !!String x,y,text,[angle],[color]        \ ����� ����� �� ������
TEXT        , !!Text   x,y,text,[angle],[color]        \ ����� ����� �� ������ 
PUSHFONT    , !!PushFont nomefont,size,bolt,italic     \ ������� ����� � ���������� ���
POPFONT     , !!PopFont                                \ ��������� ������������ �����
PUSH        , !!Push                                   \ �������� � ������ ������� �������� ���������
TRASLA      , !!Trasla x,y,z                           \ ��������� � ������� ��������� �������� ������������� ��������
RUOTA       , !!Ruota angolo                          \ ��������� � ������� ��������� �������� ��������
POP         , !!Pop                                    \ ��������������� ������� ���������, ����� ����������� 
LINETO      , !!LineTo x,y,[color],[penStyle],[penw]   \ ������ ����� �� �������������� ������������� ����� � ������������ �����
MOVETO      , !! MoveTo x,y                            \ ������������� ������� �����
POLY        , !! Poly color,x,y,x1,y1,x2,y2....        \ ������ ������� �����	
DXF         , !!DXF x,y,zoom,NomeFile                  \ ������ ���� DXF
DXF2        , !!DXF2 x,y,scalax,scalay,nomefile        \ ������ ���� DXF
BRECT       , !!Brect x,y,w,h,comando,TipiIcona,angolo,colore,des \ ������ ������
SAG         , !! SAG color,Sagoma                      \ ������
FILLSAG     , !! FILLSAG color,Sagoma                  \ ������
QUOTA       , !! QUOTA x1,y1,x2,y2,distanza,colore,[testo],[scalatesto]
O           , !! Onn                                   \ ��������� �� ������� nn (1 - 999)
PUSHPEN     , !!PushPen <N:D:P><size>                  \ ����������� �����, ������������ �� ���������
POPPEN      , !!PopPen                                 \ ��������� ����� �� ���������

PUSHFILL    , !!PushFill idfill                        \ ������ ��� ���������� �� ���������

POPFILL     , !!Popfill                                \ default ��������������� ��� ���������� �� ���������

[ENDFUNCTIONS_2D]

[VARDECLARER]
#LET       
#LETS       
#LETT       
#LETV     
[ENDVARDECLARER]

[ARG_MACROS]
ARG     , ����������� ������ Ecad \
         ARG param1 param2 param3 param4: \
         param1= ��������� ���������� (Xi,Yi); �������� ���������� ������ ���� ������ �������. \
         param2= �������� ���������� ������� (Xf,Yf); �������� ���������� ������� ���� ����� �������. \
         param3= VNumeroVista, ��� NumeroVista ����� ����� ��������� ��������: \
         0= ��������� ��� (��� �������� ��� � ������ ������) \ 
         1= ��� ������� \
         2= ��� ����� \
         3= ��� ������ \
         4= ���������������� �� \
         5= ���������������� �� \
         6= ����������������� ��� 45� ������ ������ \
         7= ����������������� ��� 45� ����� ������ \
         param4= ����� � ��� ��� �����, ����������� �������� ������ � ���������� ����������������.
AR2    , ���������� ARG ������������
ORIZZONTALE , ����������� �������������� ����������� ��������. 
VERTICALE , ����������� ������������ ����������� ��������. 
PAPERSIZE , ����������� ������� ��������� ����� \
          PAPERSIZE param1: \ 
          �������������� ������������� ���������:\
          1 - Letter, 8 1/2 x 11 in. \
          2 - Legal, 8 1/2 x 14 in. \
          3 - Executive, 7 1/2 x 10 1/2 in. \
          4 - Tabloid, 11 x 17 in. \
          5 - Ledger, 17 x 11 in. \
          6 - Statement, 5 1/2 x 8 1/2 in. \
          7 - Folio, 8 1/2 x 13 in. \
          8 - A3, 297 x 420 mm \
          9 - A4, 210 x 297 mm \
         10 - A5, 148 x 210 mm \
         11 - B4, 250 x 354 mm \
         12 - B5, 182 x 257 mm \
         13 - Quarto, 215 x 275 mm \
         14 - Custom Size 
DIM      , ����������� ������� ������� ��������� ������ \
         DIM param1 param2:\
         param1= �������������� ������ \
         param2= ������������ ������
WMF      , ������������� ����������� ������� � ������������ ������� ����� \ 
         WMF param1 param2 param3: \ 
         param1= ��������� ���������� ������ ���� ������ ������� (Xi,Yi) \ 
         param2= �������� ���������� ������� ���� ����� ������� (Xd,Yd) \
         param3= ������� ��� �����, ����������� ������������ ��������, ��������� ������ ���� \ ������������ ���������� $(.percorso), ������� ���������� ���� ������������� ��������.   
FONT     , ��������� ��� ������, ������� ����� ������������ � ������������ ������ ��������� ������ \ �� �������� ������� ������ \ param1= ��� ������; ����������� ��� ������ Windows
BX       , ������ ������������� ������ ������ \
         BX param1 param2 param3: \ 
         param1= ��������� ���������� �������������� (Xi,Yi) \ 
         param2= �������� ���������� �������������� (Xf,Xf)\
         param3= ���� ����������  
LI       , ������ ����� �������� ���������� \ 
         LI param1 param2 param3: \ 
         param1= ��������� ���������� ����� (Xi,Yi) \ 
         param2= �������� ���������� ����� (Xf,Xf) \ 
         param3= ��� � ������� �����; ������ ��� �������� ������ ��� ����� ��������
TX       , ������ ����� �������� ���������� \ 
         TX param1 param2 param3 param4:\
         param1= �������������� � ������������ ������������ \
         param2= ������ ������ � �������������� \ 
         param3= ��������� ���������� ������ (Xi,Yi) \ 
         param4= ����� ��� ������   
CODE25   , ����������� �����-��� ���� INTERLEAVD 25 \ (��� INTERLEAVD25 ��������� ����) \
         CODE25 param1 param2 param3: \
		 param1= ������� �������� ���������� ��������� ����� ���� ��� ������ ( Xi,Yi) \
         param2= ������� �������� ���������� �����-���� ��� ������ (Xd,Yd) \
         param3= ���; ������� ��� ��� ������. \
         ������: CODE25 (100,100) (50,20) CODE25	
CODE25V  , ����������� �����-��� ���� INTERLEAVD 25 �����������\ (��� INTERLEAVD25 ��������� ����) \
         CODE25 param1 param2 param3: \
         param1= ������� �������� ���������� ��������� ����� ���� ��� ������ ( Xi,Yi) \
         param2= ������� ��������� ������� �����-���� ��� ������ \ param3= ���; ������� ��� ��� ������. \
         ��������: CODE25V (100,100) (50,20) CODE25V	 
CODE39   , ����������� �����-��� ���� 3D9 (��� 3d9 ��������-��������� ����) \
         CODE39 param1 param2 param3: \
         param1= ������� ��������� ���������� ��������� ����� ���� ��� ������ ( Xi,Yi) \
         param2= ������� ��������� ������� �����-���� ��� ������ (Xd,Yd)\
         param3= ���; ������� ��� ��� ������. \
         ��������: CODE39 (100,100) (50,20) 3D9 
CODE39V  , ����������� �����-��� ���� 3D9 ����������� (��� 3d9 ��������-��������� ����) \
         CODE39 param1 param2 param3: \
         param1= ������� ��������� ���������� ��������� ����� ���� ��� ������ ( Xi,Yi) \
         param2= ������� ��������� ������� �����-���� ��� ������ (Xd,Yd)\
         param3= ���; ������� ��� ��� ������. \
         ��������: CODE39V (100,100) (50,20) 3D9V 
CODE39CK , ����������� �����-��� ���� 3D9 � check digit (�������� ������) \
         CODE39CK param1 param2 param3: \
         param1= ������� ��������� ���������� ��������� ����� ���� ��� ������ ( Xi,Yi) \ 
         param2= ������� ��������� ������� �����-���� ��� ������ (Xd,Yd)\ 
         param3= ���; ������� ��� ��� ������. \
         ��������: CODE39CK (100,100) (50,20) 3D9CK 
CODE39CKV, ����������� �����-��� ���� 3D9 � check digit ����������� (�������� ������) \
         CODE39CKV param1 param2 param3: \
         param1= ������� ��������� ���������� ��������� ����� ���� ��� ������ ( Xi,Yi) \ 
         param2= ������� ��������� ������� �����-���� ��� ������ (Xd,Yd)\
         param3= ���; ������� ��� ��� ������. \
         ��������: CODE39CKV (100,100) (50,20) 3D9CKV 
CODEMABRY, ����������� �����-��� ���� CODEMABRY \ ��� ����, ����� ������������ ��������� ���� �������\ ����������� ����������� ��� ���� �����-����\ ������������ ������ ����� ����������� ��������� \ 
         param1= ������� ��������� ���������� ��������� ����� ���� ��� ������ (Xi,Yi) \
         param2= ������� ������� ������ � ������ ���� (DimX,DimY) \
         param3= ��� �����-���� \ 
         param4= ����� ��� ������ \ 
         param5= ����������� �����-���� (�� ��������� � ����) \ 
         param6= ������������ �����-���� ������ �������������� ������ (�� ��������� � ����) \ 
         param7= ������ ���������� ������ (�� ��������� � ����) 
PENCOLOR, ������ ���� ����� \ (�.�. ���� ����� ����������� ��� �������, �������������� � ������) \ 
         PENCOLOR param1 param2 param3: \ 
         param1= ������������� �������� �����; ������� ��������������� �������� ��������; \
         param2= ������������� �������� �����; ������� ��������������� �������� ��������; \ 
         param3= ������������� ������ �����; ������� ��������������� �������� ��������
COLOREPENNA, ������ ���� ����� \ (�.�. ���� ����� ����������� ��� �������, �������������� � ������) \
         COLOREPENNA param1 param2 param3: \ 
         param1= ������������� �������� �����; ������� ��������������� �������� ��������; \ 
         param2= ������������� �������� �����; ������� ��������������� �������� ��������; \
         param3= ������������� ������ �����; ������� ��������������� �������� ��������
COLORERIEMPIMENTO, ������ ���� ���������� �������� � �������� \ 
         COLORERIEMPIMENTO param1 param2 param3: \ 
         param1= ������������� �������� �����; ������� ��������������� �������� ��������; \ 
         param2= ������������� �������� �����; ������� ��������������� �������� ��������; \ 
         param3= ������������� ������ �����; ������� ��������������� �������� ��������
FILLCOLOR, ������ ���� ���������� �������� � �������� \
         FILLCOLOR param1 param2 param3: \ 
         param1= ������������� �������� �����; ������� ��������������� �������� ��������; \ 
         param2= ������������� �������� �����; ������� ��������������� �������� ��������; \ 
         param3= ������������� ������ �����; ������� ��������������� �������� ��������; \ 
TIPORIEMPIMENTO, ������ ���� ���������� �������� ���� BX \ 
         TIPORIEMPIMENTO param1: \
         param1= ���; ��������: \ 
         0= ������ ���������� \ 
         1= ��� ���������� \ 
         2= �������������� ����� \ 
         3= ������������ ����� \
         4= ������������ ����� ������ ������ \ 
         5= ������������ ����� ����� ������� \
         6= ����� \ 
         7= ������������ �����       
FILLSTYLE, ������ ���� ���������� �������� ���� BX \ 
         FILLSTYLE param1: \  
         param1= ���; ��������: \ 
         0= ������ ���������� \
         1= ��� ���������� \ 
         2= �������������� ����� \ 
         3= ������������ ����� \ 
         4= ������������ ����� ������ ������ \ 
         5= ������������ ����� ����� ������� \ 
         6= ����� \ 
         7= ������������ �����        
SPESSORE, ������ ������� ����� ��� ������ \ 
         SPESSORE param1: ��������, ������� ����� �������� �  ���  ��������� �������� �������.
BW      , ������ ����� ������ � �����-����� \ �� ��������� ������ �������. 
MODOBW  , ������ ����� ������ � �����-����� \ �� ��������� ������ �������. 
ORIGINE , ������ ����������� ������� ������������� ����� ������ \ ������� ������������� ������ ���� ������ ����� (0,0). ������������, ��������, ��� ������� \ 
        ORIGINE param1= ��������� ���������� ������ (Xi,Yi)
TOP     , ����������� ������ ���������� ���������� �������� ��������� ���������� \
         TOP param1 param2 param3: \
         param1= ������� ��������� ���������� (Xi,Yi) ��������� ����� ������� ���������� \ 
         param2= ������� ��������� ���������� (Xf,Yf) �������� ����� ������� ���������� \ 
         param3= �����; ��� ��� �����, ����������� �������� ������ � ���������� ����������������
NTAB    , ����������� ������� � �������������� ��������� �������. ��� ������� �������� � �������� SQL. \
         NTAB (<param1>,<param2>,<param3>,<param4>)([param5],[param6],[param7],...) (<param8>) (param9)(param10)...:\
         param1= x1,y1: ����� ������ ������� \
         param2= x2,y2: ������� x � y \
         param3= y1a: ������ ������ ��������, ���� ��� ���������� \
         param4= �����: ���������, ����� �� ������� ������ ������ ������ ���������� \
         param5= scriptgruppovbs: ��������� ������ ������ ������, ��� ����� ���������� ������ ��������� ������ \				
         param6, param7,...= sottotab1, sottotab2,...: sottoquery ��� ������ ������ \
         param8= sql: ������� sql ��� �������: nTab �������� ������ �� ������ SQL, ������ ���� �������� ���������� ���������� � �������������� �� ������ � ������� scriptgruppovbs \
         param9, param10,...= (descampo1,allineamento,,nomecampo),(descampo2,allineamento,,nomecampo),... 
[ENDARG_MACROS]

[VB_GRAMMAR]
AddressOf
And
Eqv
Imp
Is
Like
Mod
Not
Or
Xor
Option 
Base
Compare 
Text
Database
Explicit
Module
Private
Public
Static
Friend
Global
WithEvents
Const
Dim
Type
Function
Sub
Property
Enum
Event
Declare
Lib
Alias
Any
Get
Let
Set
ByRef
ByVal
ParamArray
As
New
With
Implements
DefBool
DefByte
DefCur
DefDate
DefDbl
DefInt
DefLng
DefObj
DefSng
DefStr
DefVar
Binary
Boolean
Byte
Date
Currency
Double
Integer
Long
Object
Single
String
Variant
Call
Exit
GoSub
GoTo
On
Resume
Return
Stop
Error
Debug
RaiseEvent
End
If
Else
Elseif
Then
In
Do
Loop
For
To
Next
Step
Each
Select
Case
While
Wend
CBool
CByte
CCur
CDate
CDbl
CInt
CLng
CSng
CStr
CVar
Open
Close
Input
Output
Random
Read
Write
Len
Line
Print
Name
Get
Put
Seek
Lock
Unlock
Spc
Tab
LBound
UBound
Erase
ReDim
Preserve
Misc
CVErr
LSet
RSet
Nothing
Null
[ENDVB_GRAMMAR]
[OPTIONS]
KeyWordColor,0,0,255
CommentColor,0,155,0
FormulaColor,125,0,125
EsclamativeFunctionColor,128,96,32
VarColor,128,0,0
InlineFunctionColor,0,128,128
MaxRowInList,6
[ENDOPTIONS]

[VB_FUNCTIONS]
wColoreE,wColoreE(a as string, Optional RuotaTex as single = 0) As Long
wrTexAnta,wrTexAnta(dx as single, dy as single, dz as single, Tex as string, incl as single, tx as single, ty as single, p1x as single, p1y as single, p2x as single, p2y as single, Optional cx as single = 0, Optional cy as single = 0, Optional trasp as single = 0, Optional colore = 200, Optional rgb = 0, Optional bumpamp as single = 0, Optional XBUMP as string = ".", Optional texBW as string = "")
wColore,wColore(nC, Optional C& = -1, Optional Tex as string = ".", Optional s1 As Single = 0, Optional s2 As Single = 0, \ Optional trasp as single = -1, Optional BitmapBW As String = "", Optional effetto As Long = 0, \ Optional xAng as single = 0, Optional autobump = -1, Optional XBUMP as string = ".", Optional AmpBump = 1 as single, Optional metallica as single = -1) As Long
zColore,zColore(cc As Variant) \ ��� ����� �����: \ CC= CBIANCO,CGRIGIO,CNERO,CVETRO,H<hex>
wEstrudi,wEstrudi(tipo, Sagoma as string, altezza as single, c1&, c2&, Optional angolo as single = 0, Optional byval c0& , Optional altezzaBase as single = 0)
wlBox,wlBox(px as single, py as single, pz as single, dx as single, dy as single, dz as single, Optional colore&) As String
wr3D,wr3D(file as string, colore&, dx as single, Optional dy as single = 0, Optional dz as single = 0, Optional Colore2& = -1, Optional colore3& = -1, Optional colore4& = -1, Optional colore5& = -1) As String
wr3D2,Wr3d2(file as string, Colori as string, dx as single, dy as single, dz as single, inix as single, iniy as single, iniz as single, finx as single, finy as single, finz as single, Optional ForzaUV = 0) As String
wrAbilitaluce,wrAbilitaluce(Luce, px as single, py as single, pz as single, ox as single, OY as single, OZ as single, Optional cc& = &HFFFFFF)
wrBox,wrBox(px as single, py as single, pz as single, dx as single, dy as single, dz as single, colore&) As String
wrChiudi,wrChiudi()
wrLineBox,wrLineBox(dx as single, dy as single, dz as single)
wrDoppioEstrudi,wrDoppioEstrudi(tipo, Direttrice as string, Sezione as string, c1&, Optional modo = 0, Optional angI As Double = 0, Optional angF As Double = 0, Optional Rapporto As Single = 1, Optional DiffAltezza As Single = 0)
wrTexAntaCurva,wrTexAntaCurva(Sagoma as string, altezza, Spessore, colore, Tex as string, Optional Direzione = 0, Optional rgb = 0, Optional bumpamp as single = 0, Optional XBUMP as string = ".")
wrYSagoma,wrYSagoma(Sagoma as string, X as single) As Single
wrXSagoma,wrXSagoma(Sagoma as string, Y as single) As Single
wrYSagomaMin,wrYSagomaMin(Sagoma as string, X as single) As Single
wrXSagomaMin,wrXSagomaMin(Sagoma as string, Y as single) As Single
wrDrawText,wrDrawText(dx as single, dy as single, dz as single, stringa as string, Optional tipo = 0, Optional tipo2 = 0, Optional dx2 as single = 0, Optional dy2 as single = 0)
wrDrawTextE,wrDrawTextE(dx as single, dy as single, dz as single, stringa as string, tipo, tipo2, dx2 as single, dy2 as single, p1 As Punto3D, p2 As Punto3D, p3 As Punto3D)
wrFaccia,wrFaccia(lato, dx as single, dy as single, dz as single, xCOLORE, Optional LINEBORDO As Boolean = False, Optional sx as single = 1, Optional SY as single = 1)
wrFacciaAlfa,wrFacciaAlfa(dIx as single, dIy as single, dIz as single, colore, tipo, Optional Colore2 = 0)
wrLibreria,wrLibreria(DL as single, DA as single, DP as single, tipo, Optional percorsoLibri as string = "PARETI\FOTO\LIBRI") As String
wrDragger,wrDragger(tipo, dm as single, snap as single, valori as string, accettafm, Des as string, colore, fscala as single, Optional minimo as single, Optional massimo as single, Optional collidiBox As Integer = 0, Optional tipologie As String = "")
wrPannello,wrPannello(ORIENTAMENTO As String, colore&, dIx as single, dIy as single, dIz as single, Optional s1 as single = 0, Optional s2 as single = 0, Optional s3 as single = 0, Optional s4 as single = 0, Optional s5 as single = 0)
wrPannello2,wrPannello2(dIx as single, dIy as single, dIz as single, xCDAVANTI, xCDIETRO, xCDESTRA, xCSINISTRA, xCSOPRA, xCSOTTO, Optional faccia As Long = 0 _
wrParametro,wrParametro(i) As String
wrRivoluzione,wrRivoluzione(px As Single, py As Single, pz As Single, tipo, Sagoma as string, c1&, Optional spicchi = 30)
wrSfera,wrSfera(dx as single, dy as single, dz as single, cc&, Optional spicchi As Long = 16)
wrCilintro,wrCilindro(raggio1 as single, raggio2 as single, Lunghezza as single, spicchi, stacks, colore)
wrToro,WrToro(innr as single, outr as single, rings, sides, idColore)
wrTeapot,WrTeapot(scalax as single, scalay as single, scalaz as single, idColore)
wrTimeSeq,wrTimeSeq(file as string, Optional uRIPETI = 0, Optional Famiglia = 0) As Long
wrTimeReset,wrTimeReset() \ �������� ������������������ ��������
wrTimeAdd, wrTimeAdd(durata!,px,py,pz,optional axy!,axz!,ayz!,ScaleX!,ScaleY!,ScaleZ!) \ ������������� ���� ������������������ ��������
wrTimeGo,wrTimeGo(ByVal key$, Optional ByVal RIPETI = 0, Optional ByVal Famiglia = 0) As Long \ ��������� ������������� ������������������ ��������
wrTrasforma,wrTrasforma(px as single, py as single, pz as single, Optional axz as single = 0, Optional ayz as single = 0, Optional axy as single = 0, Optional scalax as single = 1, Optional scalay as single = 1, Optional scalaz as single = 1, Optional modoAngolo = 0, Optional Layer = 0)
wrTrasformaPannello,wrTrasformaPannello(px as single, py as single, pz as single, Optional axz as single = 0, Optional ayz as single = 0, Optional axy as single = 0,\ Optional scalax as single = 1, Optional scalay as single = 1, Optional scalaz as single = 1,\ Optional tiposim, Optional simx as single, Optional simy as single, Optional simz as single) As Long
xPAr,xPar(i) As String
wSagoma,wSagoma(DATI as string, tipo) As DevFaccia
zParametro,zParametro(i) As String
wrPanExt,wrPanExt(dx as single, dy as single, dz as single, c1&, c2&, modo)
Wr3DSTexAnta,Wr3DSTexAnta(File3DS as string, dx as single, dy as single, dz as single, p1x as single, p1y as single, p2x as single, p2y as single, Tex as string, tx as single, ty as single, Optional ori as string = "XZY", Optional idColore = 200)
wrIniSagoma,wrIniSagoma(Sagoma as string, X1 As Variant, Y1 As Variant)
wrFinSagoma,wrFinSagoma(Sagoma as string, X1 As Variant, Y1 As Variant)
wrMuro,wrMuro(Sagoma as string, Colore1, Colore2, colore3, Spessore, corda as single, Lunghezza as single, angolo1 as single, angolo2 as single)
wrScala,wrScala(dx as single, dy as single, dz as single, Optional fscala as single = 0.8)
wrLineeSagoma,wrLineeSagoma(punti As String, colore, Spessore as single, Sagoma As Long, Optional aggiungiSAgoma As Long = 0)
wrLinea,wrLinea(x0 as single, y0 as single, z0 as single, X1 as single, Y1 as single, z1 as single, Optional colore = 0, Optional Spessore = 0, Optional testo = 0)
w2LineH,w2LineH(x0 as single, y0 as single, X1 as single, Y1 as single, colore, Spessore)
w2LineO,w2LineO(x0 as single, y0 as single, X1 as single, Y1 as single, colore, Spessore)
w2RectH,w2RectH(x0 as single, y0 as single, X1 as single, Y1 as single, colore, Spessore)
w2RectO,w2RectO(x0 as single, y0 as single, X1 as single, Y1 as single, colore, Spessore)
wrLuce,wrLuce(tipo, tox, toy, toz, angolo1, angolo2, temperatura as single)
wrX2,Wrx2(file as string, alfa, Colori as string, dx as single, dy as single, dz as single, funzione, parfunc as string, Optional modomat As Long = 0, Optional pPivotL as single = -1, Optional pPivotA as single = -1, Optional pPivotP as single = -1, Optional skipCalcoloLinee As Boolean = False, Optional skipRicalcoloNormali As Boolean = False, Optional skipOttimizzaMesh As Boolean = False, Optional mantieniStruttura As Boolean = False)
wrXMov,wrXMov(file as string)
wrX,wrX(file as string, alfa, Colori as string, Optional dx as single = 0, Optional dy as single = 0, Optional dz as single = 0, Optional modomat As Long = 0, Optional pPivotL as single = -1, Optional pPivotA as single = -1, Optional pPivotP as single = -1)
wrPannEffetto,WrPannEffetto(idEffetto, dx as single, dy as single, dz as single, NomeFile as string, parametri as string)
wrOggetto2,wrOggetto2(Sagoma as string)
wrSpeciale,WrSpeciale(tipo, ox as single, OY as single, dx as single, dy as single, speed as single, mat as string)
wrParticle,WrParticle(accx as single, accY as single, accZ as single, maxPart, pTime as single, systemType, idMat, size as single)
wrDecalco,wrDecalco(dx as single, dy as single, toll as single, ripetibile, colore, tipologie as string, suBox, Optional spostax as single = 0, Optional ripx = 0, Optional spostay as single = 0, Optional ripy = 0, Optional inverti = 0, Optional extmaxx as single = 0, Optional extmaxy as single = 0)
wrSimbolo,wrSimbolo(file as string, dimx as single, dimy as single, Layer)
wrLog,wrLog(X As String)
wrNewAnta,WrNewAnta(Sagoma as string, h as single, Spessore as single, angolo1 as single, angolo2 as single, \ colore as single, Optional CDIETRO as single = -1, Optional cLato as single = -1, Optional CSOPRA = -1, Optional h1 as single = 0, Optional h2 as single = -1)
wrNewAntaAddUV,wrNewAntaAddUV(nodo, modo, u as single, uValue as single)
wrNodeSetModifier,wrNodeSetModifier(id&, Optional r = 0, Optional g = 0, Optional B = 0, Optional alfa = 255, Optional Spessore As Single = 1)
wrFakeLight,wrFakeLight(ByVal dIx!, ByVal dIy!, ByVal dIz!, ByVal colore, ByVal tipo, ByVal modoFaretto) \ ��������� ���������� (tipo=3, modoFaretto=0 punto, 1 giu, 2 su, 3 fronte)
wrLineaJob , wrLineaJob(byval tipologia,ByVal lunghezza!, Optional ByVal angini! = 0, Optional ByVal angFin! = 0, Optional ByVal raggioEstruso! = 50) \ ��������� ����� �� ������ ��� ��� job ��������� Lato=A e B
brCheck    ,ret=brCheck() \ ��������� � ��������� ���������� ������� 3D
brBox      ,ret=brBox(ByVal DL!, ByVal DA!, ByVal DP!) \���������� id ����� ��� 3dBOOL
brExtrude  ,ret=brExtrude(ff As Variant, ByVal h!, ByVal lato) As Long \���������� id ����������������� ��� 3DBOOL, ���� ���� ������, ������ � �����������
brRevolve  ,ret=brRevolve(ff As Variant, ByVal nseg As Long, ByVal lato, Optional ByVal ang1 As Single = 0, Optional ByVal ang2 As Single = 360) \ ���������� id ���� �������� ������ ������� 
brUnion    ,ret=brUnion(ByVal id1, ByVal id2, ByVal PL!, ByVal PA!, ByVal pp!, ByVal ax!, ByVal ay!, ByVal az!, Optional ByVal sx! = 1, Optional ByVal sy! = 1, Optional ByVal sz! = 1) As Long \ ������ �������� 3d �����������
brSubtract ,ret=brSubtract(ByVal id1, ByVal id2, ByVal PL!, ByVal PA!, ByVal pp!, ByVal ax!, ByVal ay!, ByVal az!, Optional ByVal sx! = 1, Optional ByVal sy! = 1, Optional ByVal sz! = 1) As Long  \ ������ �������� 3d ���������
brIntersect,ret=brIntersect(ByVal id1, ByVal id2, ByVal PL!, ByVal PA!, ByVal pp!, ByVal ax!, ByVal ay!, ByVal az!, Optional ByVal sx! = 1, Optional ByVal sy! = 1, Optional ByVal sz! = 1) As Long  \ ������ �������� 3d �����������
brMove     ,ret=brMove(ByVal id1, ByVal PL!, ByVal PA!, ByVal pp!, ByVal ax!, ByVal ay!, ByVal az!, Optional ByVal sx! = 1, Optional ByVal sy! = 1, Optional ByVal sz! = 1) As Long  \ ������ �������� 3d �����������
brFileExist,ret=brFileExist(ByVal file$) As Boolean \ ���������� �����, ���� ���� ����������, ��� ���� ������ 3D �� �����������
brFileSave ,brFileSave(ByVal file$, ByVal id) \ ��������� � ����� ���������� ������� id � ���������� ������� ��.
brDraw     ,brDraw(ByVal id, ByVal col1) \������ ���������� id �������
brClear    ,brClear() \ ������� ������ ������
brGetDim   ,ret=brGetDim(ByVal id, x0 as Variant,y0 as variant,z0 as variant, x1 As Variant, y1 As Variant, z1 As Variant) \ ���������� ������� �������



[ENDVB_FUNCTIONS]
