var PI=3.14159265;
function fixOptional (arg, defaultValue) {  if (isUndefined(arg) ) return defaultValue; return arg; }
function fixFloat (arg) { var x = parseFloat(arg); if(!isNaN(parseFloat(arg)) || !isFinite(arg)) return 0; return x; }
function tan(x){return Math.tan(x);};      
function sin(x){return Math.sin(x);};
function cos(x){return Math.cos(x);};
function abs(x){if (x<0) return -x; return x;};
function max(x,y){return Math.max(x,y);};  
function min(x,y){return Math.min(x,y);};
function Calcola(x) { return eng.engCalcola(x); }
function Trim(v) { return LTrim(RTrim(v)); }
function LTrim(s) { return CStr(s).replace(/^\s\s*/, ''); }
function RTrim(s) { return CStr(s).replace(/\s\s*$/, ''); }
function Len(v) { return CStr(v).length ; }
function Ubound(v) { return eng.engxBound(v); }
function Xbound(v) { if(!v) return -1; return (v.length-1); }
function Left(str,optLen) { return Mid( str, 1 , optLen); }
function Right(str,optLen) { return Mid( str, 1 + Len(str) - fixOptional ( optLen, Len(str) )  ); }
function Mid (str,optStart,optLen) { 
  var s = CStr(str);
  var start = IsMissing (optStart) ? 0 : optStart - 1;
  start = start < 0 ? 0 : start;
  var length = IsMissing (optLen) ?  Len(s) - start + 1 : optLen ;
  return  s.slice ( start, start + length);
}
//function Split(s,optDelim,optLimit) {
//  var str = CStr(s).split(fixOptional(optDelim,','),fixOptional(optLimit,-1));
//  return str;
//}
function Rept(n,s){
  return n > 0 ?  Array(n+1).join(CStr(fixOptional(s,' '))) : '';
}
function Space(n) { return Rept(n);}
function LCase(s) { return CStr(s).toLowerCase(); }
function UCase(s) { return CStr(s).toUpperCase(); }
function Chr(n) { return String.fromCharCode(n); }
function Asc(s) { return s.charCodeAt(0); }
function InStr(_optStart,_inThisString,_lookFor) {
  var start = 1;
  var inThisString = "";
  var lookFor = "";
  
  if(arguments.length == 2) {
	inThisString = _optStart;
	lookFor = _inThisString;
  } else {
	start = fixOptional (_optStart, 1);
	inThisString = _inThisString;
	lookFor = _lookFor;
  }
  var s = Mid (inThisString, start);
  var p = s.indexOf(lookFor);
  return (s && lookFor) ? (p == -1 ? 0 : p+start ): 0;
}
function InStrRev(inThisString,lookFor,optStart) {
  var start = fixOptional (optStart, -1);
  var s = CStr(inThisString);
  start = start == -1 ? Len(s) : start ;
  return (s && lookFor) ? s.lastIndexOf(lookFor,start-1)+1 : 0;
}
function atn(x) { return Math.atan(x); }
function DateSerial(y,m,d){return new Date(y,m,d); }
function Year(dt){ return dt.getFullYear(); }
function CStr(v) { return v===null || IsMissing(v) ? '' :  v.toString()  ; }
function CDbl(v) { return parseFloat(v) ; }
function CLng(v) { return isTypeNumber(v) ? Math.round(v) : parseInt(v,10) ;}
function Val(v) { return parseFloat(v) ; }
function Xor (a,b) { return a ? !b : b ; }
/*function Abs (x) { return Math.abs(x); }

function IsEmpty(v) { return typeof(v) == 'string' && v == Empty(); }
function IsDate(sDate) {
  var tryDate = new Date(sDate);
  return (tryDate.toString() != 'NaN' && tryDate != 'Invalid Date') ;
}*/
function IsNumeric(s) { return !isNaN(parseFloat(s)) && isFinite(s); }
function IsMissing (x) {return isUndefined(x); }
/*function IsObject (x) { return VarType(x) == 'object'; }
function IsArray (x) {  return isArray(x) ;}
function IsNull (x) { return x===null ; }
function VarType (v) { return typeof v; }

function Empty() {  return '';}
var vbLf='\n';
var vbCrLf='\r\n';*/
var vbCr ='\r';
function Rnd() { return Math.random(); }
function isUndefined ( arg) {   return typeof arg == 'undefined'; }
function toType(obj) { return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()}
function isTypeNumber ( arg) {  return typeof arg == 'number';}
function isTypeString ( arg) {  return typeof arg == 'string';}
function isArray (arg) {  return toType(arg) == 'array';}
//function MakeKey(v) {    return eng.engMakeKey(v);}
function Ex(str,sep,pos) { return eng.engEx(str,sep,pos);  }
function Ex(str,sep,pos,end) { return eng.engEx(str,sep,pos,end);  }
//function Split(str,sep,value) { return eng.engSplit(str,sep,value);  }
function Join(v,sep) { return v.join(sep);  }
function zeroFilledArray(size) {
    return new Array(size + 1).join('0').split('');
}
function Resize(v,len) { 
	if(!v) return zeroFilledArray(len); 
	else if(v.length <= 0) zeroFilledArray(len); 
	else {
		var vx = zeroFilledArray(len);
		var qq = Math.min(len,v.length);
		for(var i = 0;i<qq;i++) {
			vx[i] = v[i];
		}
		return vx;
	}
}
function RGB(r,g,b) { return b << 16 | g << 8 | r; }
//function Fix(xval) { return ~~(xval); }
function Replace(str,search,replace) {return eng.engReplace(str,search,replace); }
function Now() {  return new Date();}
function MsgBox(tex) { eng.engMsgBox(tex); } 
function Split(s,optDelim) {
  var str = CStr(s).split(fixOptional(optDelim,','));
  return str;
}
var Percorso = eng.engPercorso();
var js = {};
js.StrRep = function(stringa,parEle) {
	for (var i = 1; i < arguments.length; i++) {
		var re = new RegExp("%" + i, "g")
		stringa = stringa.replace( re , arguments[i] );
		
	}
	return stringa;
}
js.Ex = function(str,sep,pos,end) { return  Ex(str,sep,pos,end);}
js.engCalcola = function(x) { return  Calcola(x); }
js.xBound = function(v) { return Xbound(v); }
js.Resize = function(v,len) { return Resize(v,len); }
js.Replace = function(str,search,replace) { return Replace(str,search,replace); }
js.MsgBox = function(tex) { return MsgBox(tex); }

