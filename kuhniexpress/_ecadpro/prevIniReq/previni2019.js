﻿var nRighe, Tot;



function Conferma(i) {
	CalcTotRiga(i);
	$("#modriga"+i).text("*");
	calcolaTotale();
	//$("html, body").animate({ scrollTop: $("#riga" + i).offset().top }, 500);
}		

function CalcTotRiga(i) {
	var a,b,c
	a = $("#inputqta" + i).attr("value");
	b = $("#inputpr" + i).attr("value");	
	a = a.replace (",",".");
	b = b.replace (",",".");
	c = eval(a*b);
	c = c.toFixed(2);
	//c = c.toString();
	c = c.replace (".",",");
	$('#totr' + i).text(c);		
}

function calcolaTotale() {
	var a, b
	Tot = 0;
	
	nRighe = $(".prevDataRow").length - 3; //-3 perchè ci sono anche le righe dei totali e di intestazione
	for (i=1;i<=nRighe;i++) {
		if($('#riga'+i).css('display') != 'none' && i!=0){ //calcolo solo quelle visibili, se non vedo le righe vuol dire che sono state cancellate
			a = $("#inputqta" + i).attr("value");
			b = $("#inputpr" + i).attr("value");
			if (a!=undefined && b!=undefined)
				Tot += eval(a*b);
		}
	}		
	$('#IMPONIBILE').text(Tot.toFixed(2));
	Tot = applyDiscountIva(Tot);
	$('#totale').text(Tot.toFixed(2));
} 

function applyDiscountIva(x) {
	//debugger;
	var sc1, sc2, iva, trasp, mon
	sc1 = $("#XSC1").text();
	sc2 = $("#XSC2").text();
	iva = $("#IVA").text();
	trasp = $("#XTRASP").text().replace(",",".");
	mon = $("#XMON").text().replace(",",".");
	sc1 = x*sc1/100;
	x = x - sc1;
	$("#SC1TOT").text("- " + sc1.toFixed(2));
	sc2 = x*sc2/100;
	x = x - sc2;
	$("#SC2TOT").text("- " + sc2.toFixed(2));
	iva = x*iva/100;
	x = x + iva;
	$("#IVATOT").text(iva.toFixed(2));
	trasp = trasp * 1;
	$("#XTRASP").text(trasp.toFixed(2));
	x = x + trasp;
	mon = mon * 1;
	$("#XMON").text(mon.toFixed(2));
	x = x + mon;
	return x;
}

function removeRow(row) {  
	if (confirm("Delete row?")==true){
	 $("#riga" + row).css('display','none'); //la "cancello" nascondendola
	 $("#modriga" + row).text(""); //resetto * cosi non viene salvata
	 $("riga"+row).attr("name") = "";
  }
  calcolaTotale();     
}

function addRow(){
	var id1,id2,id3,id4,id5,e,AppUrl;

	nRighe = $(".prevDataRow").length - 3
	nRighe++
	id1 = "inputdes" + nRighe; id2 = "inputqta" + nRighe; id3 = "riga" + nRighe; id4 = "inputpr" + nRighe; id5 = "modriga" + nRighe; id6 = "totr" + nRighe;
	
	$(".prevDataRowCustom").replaceWith('<div class="prevDataRow" name="manuale" id='+id3+'>' +
		'<div class="prevData"></div>'+
		'<div class="prevData"><textarea id='+id1+' type="text" rows="6" cols="25" /></div>'+
		'<div class="prevData"><input id='+id2+' onClick="this.select();" onfocusout="Conferma(' + nRighe + ')" type="number" maxlength="4" value="1"></div>'+
		'<div class="prevData"><input id='+id4+' onClick="this.select();" onfocusout="Conferma(' + nRighe + ')" type="number" maxlength="7" value="0"></div>'+
		'<div class="prevData" id="'+id6+'">0.00</div>'+
		'<div class="prevData">'+
		'<img src="../PrevIniReq/delete.png" onclick="removeRow('+ nRighe +')" class="imageButton" alt="Rimuovi" onmouseover="this.src=\'../PrevIniReq/delete_h.png\';" onmouseout="this.src=\'../PrevIniReq/delete.png\';">'+
		'<div id="'+id5+'"></div></div></div>');
		
	$('.prevDataTable').append('<div class="prevDataRowCustom"></div>');
	$("#"+id1).focus();
}

function SalvaSrv(utente,catalogo,ordine){	   
	var i,dati,extra1,idBox,CodArt,Des,Qta,Pr,Extra,AppUrl
	
	dati=""
	AppUrl = $("#serverIP").text() + 'gPrev.aspx';
	nRighe = $(".prevDataRow").length - 3;
	//alert (AppUrl);
	for (i=1;i<=nRighe;i++) {
		if ($("#modriga"+i).text() == "*") {
			
			//debugger
			if ($("#riga"+i).attr("name") == "manuale") {
				idBox = -i; 
				CodArt = "ITEM"+i;
				Des = $("#inputdes"+i).val();
			} else {				
				idBox = $("#riga"+i).attr("name");
				CodArt = $("#cod"+i).text();
				Des = $("#des"+i).text();				
			}			             
						
			Qta = $("#inputqta"+i).val();
			Pr = $("#inputpr"+i).val();
			Extra = "*,importo="; //"*,importo="+$("#Totale").html();      
			dati = dati + "\r\n" + idBox + "\t" + CodArt + "\t" + Des + "\t" + Qta + "\t " + Pr + "\t " + Extra;
		}
	}    	
	
	$.post(AppUrl,{ utente: utente, catalogo: catalogo, operazione:'SALVA', ordine:ordine, dati:dati  } ,function (data) {
		if (data==1) {
			alert ("OK!")
		}else{
			alert ("Error saving!")
		}    
	});
}




function ResetSrv(utente,catalogo,ordine){
	var res, AppUrl
	
	AppUrl = $("#serverIP").text() + 'gPrev.aspx';
	res=confirm("Delete all changes?");
	if (res==true){		
		$.post(AppUrl,{ utente: utente, catalogo: catalogo, operazione:'SALVA', ordine:ordine, dati:'' })
	}
}