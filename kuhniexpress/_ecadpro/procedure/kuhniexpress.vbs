option explicit

dim xCRM 
Dim ForceClose



Function init()

    Ambiente.dbarra1.visible = false
    Ambiente.tab1.enabled = false
    set xCrm =   Nothing
    ForceClose = 0

end function

Function RegisterPlugin(xplugin)

    set xCRM =  xplugin
    RegisterPlugin = 1

end function

Function Termina()
    if not xCRM is Nothing then
        set xCRM=Nothing
    end if
end function

Function CercaNumeroOrdine(byval nn)
    if not xCRM is Nothing then
        CercaNumeroOrdine = xCRM.OrderNumber
    else
        CercaNumeroOrdine = nn
    end if
end function 

Function AttivaUploadOrdine( byval UserName, Byval UserCustomerCode, byval formObject)
	AttivaUploadOrdine = 1 
end Function


Function SalvaOrdine(byval xml )
    if not xCRM is Nothing then
        if xCRM.SaveProject(xml) then
            ForceClose =0
        else
            ForceClose =0
        end if
    else
        ForceClose = 2
    end if
end function

function SaltaSalvaOrdine() 
  dim xamb
  set xamb=ambiente.getobject("XAMB")
  xamb.info.info.add "dateversion", Now
  ambiente.mostraintestazione
  ambiente.DrRidisegnaTutto
  SaltaSalvaOrdine=0
end function

Function BeforeSendList(ByVal xAmbiente, ByVal xConn, ByVal catalogo, ByRef xml)
	
	if not xCRM is Nothing then
		if xCRM.isProjectSaved() then
			xamb.info.info.add "_savedproject", "1"
		else 
			xamb.info.info.add "_savedproject", "2"
		end if
	end if

	Dim xdoc,nodeSaved
	Set xdoc = CreateObject("MSXML2.DOMDocument")
	xdoc.loadXML "<DATI>" & xml & "</DATI>"
	

	Set nodeSaved = xdoc.SelectSingleNode("DATI/TESTA/VAR/_SAVEDPROJECT")

	If Not nodeSaved Is Nothing Then
		nodeSaved.text=xamb.info.info.var("_savedproject")
	End If

    xml = xdoc.xml
	set xdoc = nothing
	set nodeSaved = nothing
	
	BeforeSendList=0
end function


Function ChiudiApplicazione()
	DIM result
    'if forceClose then
    '    ChiudiApplicazione = 1 
   ' else 
		if not xCRM is Nothing then
			 if xCRM.SaveProject("") then
				ChiudiApplicazione = 1 
			 else 
				ChiudiApplicazione = 0 
			 end if
		else 
			ChiudiApplicazione = 1 
		end if
		'result = MsgBox ("Вы хотите сохранить проект?", vbYesNo + vbQuestion,"Сохранение проекта")
		'if result=vbYes then 
		'	if not xCRM is Nothing then
		'		xCRM.SaveProject("") 
		'	end if
		'	ChiudiApplicazione = 1 
		'else 
		'	ChiudiApplicazione = 1 
		'end if
    'end if
end function

Sub ControlliAbilitati(nome,obj,regole)
   Select Case ucase(nome)
	  Case "APPLICA" 
		obj.dButt2.visible=False
      Case "ARTICOLO"
		obj.lblQta.visible=False ' label Qta	 
		obj.tQta.visible=False ' text box Qta
		obj.tQta.enabled=False 
		obj.tab1.tabvisible(2)=False' 
		obj.tab1.tabvisible(1)=false
      case "MAIN"
		obj.tCli(0).visible=false
		obj.Command2.visible=false
		obj.tCli(1).visible=false
		obj.tCli(2).visible=false
		obj.tCli(3).visible=false
		obj.tCli(4).visible=false
		obj.tCli(5).visible=false
		obj.tCli(6).visible=false
		obj.tCli(7).visible=false
		obj.tCli(18).visible=false
		obj.tCli(8).visible=false
		obj.tCli(15).visible=false
		obj.Command3.visible=false
		obj.Command9.visible=false
		obj.Command10.visible=false
		obj.tCli(9).visible=false
		obj.tCli(10).visible=false
		obj.tCli(11).visible=false
		obj.tCli(12).visible=false
		obj.tCli(13).visible=false
		obj.tCli(14).visible=false
		obj.tCli(16).visible=false
		obj.tCli(17).visible=false      
		obj.tCli(0).enabled=false
		obj.Command2.enabled=false
		obj.tCli(1).enabled=false
		obj.tCli(2).enabled=false
		obj.tCli(3).enabled=false
		obj.tCli(4).enabled=false
		obj.tCli(5).enabled=false
		obj.tCli(6).enabled=false
		obj.tCli(7).enabled=false
		obj.tCli(18).enabled=false
		obj.tCli(8).enabled=false
		obj.tCli(15).enabled=false
		obj.Command3.enabled=false
		obj.Command9.enabled=false
		obj.Command10.enabled=false
		obj.tCli(9).enabled=false
		obj.tCli(10).enabled=false
		obj.tCli(11).enabled=false
		obj.tCli(12).enabled=false
		obj.tCli(13).enabled=false
		obj.tCli(14).enabled=false
		obj.tCli(16).enabled=false
		obj.tCli(17).enabled=false      
		obj.dBarra1.bEnabled("INFO")=false
		obj.dBarra1.bVisible("INFO")=false

		obj.Command6.visible=false
		obj.Command6.enabled=false
		obj.Command8.visible=false
		obj.Command8.enabled=false

		obj.messaggio.visible=false
		obj.messaggio.enabled=false
		obj.bVaria.visible=false
		obj.bVaria.enabled=false
		obj.lnome(0).visible=false
		obj.lnome(0).enabled=false

		obj.lnome(1).visible=false
		obj.lnome(1).enabled=false
		obj.lnome(2).visible=false
		obj.lnome(2).enabled=false
		obj.lnome(3).visible=false
		obj.lnome(3).enabled=false
		obj.lnome(4).visible=false
		obj.lnome(4).enabled=false
		obj.lnome(5).visible=false
		obj.lnome(5).enabled=false
		obj.lnome(6).visible=false
		obj.lnome(6).enabled=false
		obj.lnome(7).visible=false
		obj.lnome(7).enabled=false
		obj.lnome(8).visible=false
		obj.lnome(8).enabled=false
		obj.lnome(9).visible=false
		obj.lnome(9).enabled=false
		obj.lnome(10).visible=false
		obj.lnome(10).enabled=false
		obj.lnome(11).visible=false
		obj.lnome(11).enabled=false
		obj.lnome(12).visible=false
		obj.lnome(12).enabled=false
		obj.lnome(13).visible=false
		obj.lnome(13).enabled=false
		obj.lnome(14).visible=false
		obj.lnome(14).enabled=false
		obj.lnome(15).visible=false
		obj.lnome(15).enabled=false
		obj.lnome(16).visible=false
		obj.lnome(16).enabled=false
		obj.lnome(17).visible=false
		obj.lnome(17).enabled=false
		obj.lnome(18).visible=false
		obj.lnome(18).enabled=false
		obj.lnome(19).visible=false
		obj.lnome(19).enabled=false
		obj.lnome(20).visible=false
		obj.lnome(20).enabled=false
		obj.lnome(21).visible=false
		obj.lnome(21).enabled=false
		obj.lnome(22).visible=false
		obj.lnome(22).enabled=false
		obj.lnome(23).visible=false
		obj.lnome(23).enabled=false

   End Select
end sub 


function TarghetteCreate(elencobox)
dim xamb1
'tipo = dal 4 in poi
set xamb1=Ambiente.GetObject("XAMB")
xamb1.CalcolaOrdinamento

set xamb1 = nothing

end function

 

function TarghetteBox(box,tipo)


with box

select case tipo
   
        case 4

            if .Ordinamento>=0 then ' escludo i codici "99"
          
                TarghetteBox = .Ordinamento

	            if .gg.gnt<>"" then
	                TarghetteBox = TarghetteBox & "*"
	            end if
            end if

        case 5

            if mid(.gg.cod,8,2)<>"99" then ' escludo i codici "99"

                TarghetteBox = "Cod: " & mid(.gg.Cod,8)

            end if

end select

end with

end function

 

function TarghettaExtra()
    'elenco tipi 4 = Id unico  5 = codice neutro
    TarghettaExtra = "IdUnico;Neutro"
end function

 

function FiltriExtra()
end function 