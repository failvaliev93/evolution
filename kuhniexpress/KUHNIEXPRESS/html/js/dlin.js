﻿/*
 Скрипт для перерасчета длинномеров
 TO DO: внести изменения в массив ГОТОВ, перерендерить массив таблицей вместо текущей mytable
 адаптивная верстка, добавить в body ГОТОВ, класс Длинномер
*/

let dlinContainer = ""; //html форма для флинномеров
let dlinContainerTest = "";
let leftPos = ""; //левая колонка 1750
let data = {};
let tempData = []; //test
let oldArrDlin = [];
let changedArrDlin = [];
/*[
0.массив объектов (object)
1.arrL (array)
2.+ (text)
]*/


//вызываю модуль
function getModulDlin() {
    /*
    window.onresize = function () { //изменение размера окна рисую каждый раз
        if (document.getElementsByTagName("body")[0].clientWidth > 1760) {
            leftPos = "leftPos";
            //console.log(document.getElementsByTagName("body")[0].clientWidth);
        } else {
            leftPos = "";
        }
        load();
        document.getElementsByClassName("dlinContainer")[0].outerHTML = dlinContainer;
    }*/
    //load();
    /*let dlinTop = new DlinClass("Столешница", data, "text");
    let dlinTop2 = new DlinTopClass();
    dlinTop.sayHi();
    //document.querySelector("body").innerHTML += dlinTop2.drawDlin(jsonPrev);
    document.querySelector("body").innerHTML += dlinTop2.drawTop();*/
    mainDlin();
}


//Модуль в целом
function mainDlin() {
    let htmlElemDlin = "";
    const keyVarGroup = ['TOP_MANUFACTURER', 'STOLESH_MAT', 'TOLSHINA', 'KROMKA', 'RADIUS', 'TWO_SIDE_COLOR'];
    //let data = {};//Object.assign({}, jsonPrev); //big data берем из prevIni.js 
    //Object.assign(data, jsonPrev); //не работает
    data = jsonPrev;
    //console.log(data);
    //console.log(copy);

    let ss = _.filter(data.PREV.RIGHE, { 'COD': 'TOP' });//наши длин-ы
    let myBody = document.querySelector("body");

    //сначало рисую, 1 раз после загрузки
    dlinContainer += '<div class="dlinContainer ' + leftPos + '"> \
                        <h2>Длинномеры:</h2>\
                        <div class="dlinContent">\
                        </div >\
                    </div > ';
    //console.log(dlinContainer);
    myBody.innerHTML += dlinContainer; //вывод


    var top = _.filter(data.PREV.RIGHE, { 'COD': 'TOP' });
    //Перебираем массив столешниц
    top = top.map(function (item) {
        //Преобразуем сторку вариантов в массив
        item.VarArr = stringToMap(item.VAR);
        //console.log(item.REAL_L);
        return item;
    });
    //Группируем столешницы по вариантам
    var topGroup = _.groupBy(top, function (item) {
        //подбираем варианты для объединения столшниц
        //Фильтруем только нужные варианты
        var GroupKey = item.VarArr.filter(function (iVar) {
            return _.indexOf(keyVarGroup, iVar[0]) >= 0
        });
        //Преобразуем в строку <вариант>=<Значение>
        GroupKey = GroupKey.map(function (ikey) {
            return _.join(ikey, '=');
        });
        //объединяем все варинты в троку с разделителем ";"
        return _.join(GroupKey, ';');
    });
    //console.log(topGroup);


    let codElem = 0;
    //потом внутрь добавляю
    for (let key in topGroup) {
        //console.log(topGroup[key]);

        oldArrDlin.push(codElem); //запаковываю экземплярами длинномеров
        oldArrDlin[codElem] = [];
        changedArrDlin.push(codElem);
        changedArrDlin[codElem] = [];
        //console.log(codElem); 
        elemDlin("Столешницы", 'TOP', topGroup[key], codElem); //Каждый длинномер
        codElem++; //идентификатор в массиве откуда берется инфа
    }
    //elemDlin("Столешницы", 'TOP', ss); //Каждый длинномер

    //и добавляю кнопку
    document.querySelector(".dlinContent").innerHTML += '\
                        <button disabled class="bAddChanges" id="bAddChanges" name="TOP" onclick="addChanges(event.target)">Внести изменения</button>\
                        <div class="clear"></div>';
    addChanges();
}

//Каждый длинномер
function elemDlin(nameDlin, codDlin, arrDlin, codElem) {

    //Найдено:
    let n = 1; //кол-во кусков
    let sumL = 0; //сумма кусков
    let find = ""; //табличка найденных
    let valAuto = 'value="auto"'; //значение <option> Автоматически по умолчанию
    let valNot = 'value="not"';   //значение <option> Без изменений
    let valHand = 'value="hand"'; //значение <option> Ручной
    let codAuto = "auto"; //ключ Автоматически по умолчанию
    let codNot = "not";   //ключ Без изменений
    let codHand = "hand"; //ключ Ручной
    let inHTML = document.querySelector(".dlinContent");
    let myname = arrDlin[0].VarArr[0][1].split("Т")[0];

    //console.log(document.getElementsByClassName("dlinContent"));
    arrDlin.forEach(function (item, index) {
        let classCss = "odd"; //цвет фона четной
        if (n % 2 == 0) classCss = "even"; //не четная
        find += '<p class="' + classCss + '">' + '1' + ' длиной ' + item.REAL_L + '\
         ('+ item.L + ')</p>';
        n++;
        sumL += item.L;
        //console.log(item);
        oldArrDlin[codElem].push(item); //
    })
    tempData = arrDlin.slice();
    //Object.assign(tempData, arrDlin); //не канает
    //console.log(arrDlin[0].VarArr[0][1]);

    //Автоматически по умолчанию
    //let opt = algoritmDlin(codDlin, valAuto, codAuto)['html']; //html

    //Рисую name="' + codElem + '"
    inHTML.innerHTML += '<div class="gorLine"></div>\
                    <div class="dlinHead" title="' + codElem + '">' + nameDlin + '. Просчёт: \
                        <select id="sel'+ codDlin + '" size="1"  onchange="onChangeSel(event.target)">\
                            <option '+ valAuto + ' selected>Автоматически</option>\
                            <option '+ valNot + '>Без изменений</option>\
                            <option '+ valHand + '>Ручной</option>\
                        </select>\
                        <div class="mynameDlin" id="'+ codElem + 'mynameDlin">' + myname + '</div>\
                    </div>\
                    <div class="tab">\
                        <div class="tabFind">\
                            Найдено:\
                            <div class="tabFindIn" id="'+ codElem + 'tabFindIn">' + find + '</div>\
                        </div>\
                        <div class="tabOpt">\
                            Оптимизировано:\
                            <div class="tabOptIn" id="'+ codDlin + codElem + '" title="' + codElem + '"></div>\
                        </div>\
                        <div class="clear"></div>\
                    </div>\
                    <div class="total">\
                        <div class="tabFind tabFindOut" id="'+ codElem + 'tabFindOut">\
                            <p class="totalBad">Итого: общая длина <span id="' + codElem + 'tabFindOutL">' + sumL + '</span></p>\
                        </div>\
                        <div class="tabOptOut" id="'+ codDlin + codElem + '">\
                            <p class="totalGood">Итого: 1 кусок по 1500</p>\
                        </div>\
                        <div class="clear"></div>\
                    </div>';

    //console.dir(document.querySelector('.dlinHead'));
    //console.dir(document.querySelector('select'));
    //Оптимизировано: 
    algoritmDlin(codDlin, valAuto, codAuto, find, arrDlin, codElem);

    /*
    //переключение просчета
    let select = document.getElementById("sel" + codDlin); // selTOP
    let optionIndex = select.options.selectedIndex; //0
    let optionValue = select.options[optionIndex].value + select.name; //autoTOP
    select.name = codDlin;
    //select.title = codAuto;
    select.onChange = function () {
        console.log("Hi");
        algoritmDlin(codDlin, optionValue, codAuto, find, arrDlin);
    }
    select.addEventListener("click", function () {
        console.log("Hi");
    })*/

}

//переключение просчета //"щука, тля, жеванный крот"//
function onChangeSel(select) {
    //console.log(select);
    //let select = document.getElementById(id);
    let optionValue = select.value + select.name; //notTOP
    let cod = select.value; //значение списка
    let codDlin = "TOP"; //только столы
    let codElem = select.parentNode.title; //.dlinHead
    let find = document.getElementById(codElem + 'tabFindIn').innerHTML;
    let addButton = document.getElementById('bAddChanges');

    addButton.textContent = "Внести изменения";
    addButton.style.color = "black";
    addButton.disabled = false;

    //console.log('codDlin=' + codDlin + '; optionValue=' + optionValue + ';\
    //cod = ' + cod + '; find = ' + find + '; tempData = ' + tempData + '; codElem = ' + codElem);
    //console.dir(select);
    algoritmDlin(codDlin, optionValue, cod, find, oldArrDlin[codElem], codElem);
}


//Исходя из алгоритма просчета в "Оптимизировано:"
function algoritmDlin(codDlin, value, cod, find, arrDlin, codElem) {
    //codElem - кодировка каждого длинномера
    let htmlAlgoritmDlin = "";
    let inHTML = "";//document.querySelector(".tabOptIn"); // после Оптимизировано
    let tabOptOut = "";//document.querySelector(".tabOptOut"); //Итого:
    let opt = ""; //табличка Оптимизировано
    let n = 1;    //кол-во кусков
    let sumL = 0; //сумма кусков
    let arrL = []; //разрешенные длины
    let sumLReal = 0; //сумма настоящих кусков
    let arrLCount = []; //количество кусков
    let sumLOpt = 0; //сумма получившихся кусков
    let newArrDlin = []; //полученный массив длинномеров
    let addButton = document.getElementById('bAddChanges');

    //нужный класс .tabOptOut
    let gg = document.querySelectorAll(".tabOptOut");
    for (let i = 0; i < gg.length; i++) {
        if (gg[i].id === codDlin + codElem) {
            tabOptOut = gg[i];
            //console.log(gg[i]);
        }
    }
    gg = document.querySelectorAll(".tabOptIn");
    for (let i = 0; i < gg.length; i++) {
        if (gg[i].id === codDlin + codElem) {
            inHTML = gg[i];
            //console.log(gg[i]);
        }
    }

    //console.log(tabOptOut);

    arrDlin.forEach(function (item, index) {
        let classCss = "odd"; //цвет фона четной
        if (n % 2 == 0) classCss = "even"; //не четная
        //opt += '<p class="' + classCss + '">' + n + ' длиной ' + item.REAL_L + '\
        // ('+ item.L + ')</p>';
        n++;
        sumL += item.L;
        sumLReal += item.REAL_L;
        arrL = findTag(item.VAR, "TOP_L").split('*'); //потом вынести за функцию
        //console.log(item);
    })
    //console.log(arrDlin);

    for (let i = 0; i < arrL.length; i++) {
        arrL[i] = +arrL[i]; //перевожу в числа
        arrLCount[i] = 0;
    }
    arrL.sort(compareNumeric); //сортирую разрешенные длины от меньшего

    //htmlAlgoritmDlin = '<p class="odd">1 длиной 450 (1500)</p>';


    if (cod === "auto") { //Автоматически
        let countI = 0;
        //поочередно вычитаю куски начиная сбольшего с единого целого
        //sumLReal = 7000;
        for (let i = arrL.length; i >= 0; i--) {
            if (arrL[i] <= sumLReal) { //кусок влезает
                countI = Math.floor(sumLReal / arrL[i]);
                sumLReal -= arrL[i] * countI;
                arrLCount[i] = countI;
                //console.log(i);
                //console.log(sumLReal + ' | ' + arrL[i] + ' * ' + countI);
            }
            //console.log(sumLReal);
        }
        if (sumLReal > 0) { //остался кусочек 
            arrLCount[0] += 1;
            //console.log(sumLReal + ' | ' + arrL[0]);
        }


        //массив новых кусков
        for (let i = 0; i < arrLCount.length; i++) {
            if (arrLCount[i] > 0) {
                let riga = SetRiga(CreateRiga(), arrDlin[0]);
                //console.log(arrDlin[0]);
                riga.L = arrL[i]; //длина
                riga.PZ = arrLCount[i]; //количество               

                //меняю в описание длину
                let newName = findTag(riga.VAR, "MYNAME");
                newName = newName.replace(arrDlin[0].L, riga.L);
                riga.VAR = findTagReplace(riga.VAR, "MYNAME", newName);
                //console.log(newName);
                //console.log(arrDlin[0].L + '; ' + riga.L);

                //console.log(riga);
                newArrDlin.push(riga);
            }
        }

        //Добавляем новые строки и удаляем старые
        let rigaIndex = 0;
        if (newArrDlin.length > 0) {
            //Удаляем  
            for (let key in data.PREV.RIGHE) {
                //Номер строки 
                rigaIndex = _.parseInt(key.replace('RIGA', ''));
                if (data.PREV.RIGHE[key].COD === "TOP") {
                    delete data.PREV.RIGHE[key];
                }
            }
            rigaIndex++;
            newArrDlin.forEach(function (r) {
                data.PREV.RIGHE['RIGA' + rigaIndex] = r;
                rigaIndex++;
            });
        }

        //console.log(arrL);
        //console.log(newArrDlin);
        //console.log(tempArr);

        //вывод, рисую
        n = 1;
        opt = "";
        for (let i = 0; i < arrLCount.length; i++) {
            if (arrLCount[i] > 0) {
                let classCss = "odd"; //цвет фона четной
                if (n % 2 == 0) classCss = "even"; //не четная
                opt += '<p class="' + classCss + '">' + arrLCount[i] + ' длиной ' + arrL[i] + '</p>';
                n++;
                sumLOpt += arrL[i];
            }
        }
        tabOptOut.innerHTML = '<p class="totalGood">Итого: общая длина <span id="' + codElem + 'sumLOpt">' + sumLOpt + '</span></p>';
        //document.querySelector(".bAddChanges").disabled = true; //его еще нет
        //console.dir(document.querySelector(".clear"));

        changedArrDlin[codElem][0] = newArrDlin.slice();
        for (let key in data) { //сохраняем изменения //не раб
            //changedData[key] = data[key];
        }
        /*
        console.log(jsonPrev.PREV.RIGHE.RIGA8.PZ);
        console.log(data.PREV.RIGHE.RIGA8.PZ);
        console.log(changedData.PREV.RIGHE.RIGA9.PZ);
        console.log(copyData.PREV.RIGHE.RIGA5.PZ);
        */

    } else if (cod === "not") { //Без изменений
        opt = find;
        tabOptOut.innerHTML = document.getElementById(codElem + 'tabFindOut').innerHTML;

        addButton.textContent = "Внести изменения";
        addButton.style.color = "black";
        addButton.disabled = false;

        changedArrDlin[codElem][0] = oldArrDlin[codElem].slice();
        //console.log(find);

    } else if (cod === "hand") { //Ручной
        /*
        let addString = ''; //+ для добавления новой строки
        let LSelIn = ""; //внутренности список длин
        let countSelIn = ""; //внутренности список количества
        let newString = "";
        let selected = ""; //выбран по умолч-ю
        
        changedArrDlin[codElem][0] = oldArrDlin[codElem].slice(); //копирую старый массив
        changedArrDlin[codElem][1] = arrL.slice(); //массив разрешенных длин
        
        //строка
        n = 1; //начало
        for (let i = 0; i < changedArrDlin[codElem][0].length; i++) {
            //составляю список количества от 1 до 9
            for (let ii = 0; ii < 9; ii++) {
                let nn = ii + 1;
                if (nn == changedArrDlin[codElem][0][i].PZ) selected = "selected"; //выбран по умолч-ю
                countSelIn += '<option value="' + nn + '" ' + selected + '>' + nn + '</option>';
                selected = "";
            }
            //id = № длинномера + countSel + № списка
            let countSel = '<select id="' + codElem + 'countSel' + n + '" onchange="editCountSelDlin(event.target)">' + countSelIn + '</select>';//список количества

            //составляю список длин
            for (let ii = 0; ii < arrL.length; ii++) {
                if (arrL[ii] == changedArrDlin[codElem][0][i].L) selected = "selected"; //выбран по умолч-ю
                LSelIn += '<option value="' + arrL[ii] + '" ' + selected + '>' + arrL[ii] + '</option>';
                selected = "";
            }
            //id = № длинномера + LSel + № списка
            let LSel = '<select id="' + codElem + 'LSel' + n + '" onchange="editLSelDlin(event.target)">' + LSelIn + '</select>';    //список длин

            classCss = "odd"; //цвет фона четной
            if (n % 2 == 0) classCss = "even"; //не четная
            newString += '<p class="' + classCss + '" id="' + codElem + 'p' + n + '">' + LSel + ' кол.: ' + countSel + '</p>';
            n++;
            LSelIn = "";
            countSelIn = "";
        }

        //строка добавления
        newString += '<p class="' + classCss + ' addNewRow">&nbsp<img onclick=addRowDlin(event.target) title="Добавить" src="..\\..\\' + percorso + '\\HTML\\images\\plus.png" /></p>';

        //console.log(changedArrDlin[codElem]);
        document.querySelector(".bAddChanges").disabled = false;
        opt = newString;
        tabOptOut.innerHTML = "";
        */
        changedArrDlin[codElem][0] = [];
        changedArrDlin[codElem][1] = arrL.slice();
        classCss = "odd";

        addButton.textContent = "Внести изменения";
        addButton.style.color = "black";
        addButton.disabled = false;

        opt = '<p class="' + classCss + ' addNewRow" id="' + codElem + 'addNewRow">&nbsp<img onclick="addRowDlin(event.target)" title="Добавить" src="..\\..\\' + percorso + '\\HTML\\images\\plus.png" /></p>';

        tabOptOut.innerHTML = '<p class="totalGood">Итого: общая длина <span id="' + codElem + 'sumLOpt">0</span></p>';
    }
    //console.log(arrL);
    //console.log(arrLCount);
    //console.log(sumLReal);

    inHTML.innerHTML = opt;
    //console.log(oldArrDlin);
    //console.log(changedArrDlin);
    //console.dir(document.getElementById("0LSel3"));
}

//ивент, добавляю +1 строчку в ручном просчете, +1 в массив
function addRowDlin(target) { //img
    //console.dir(target);
    let codElem = target.parentNode.parentNode.title; //кодировка длинномера //div.p.img
    let codDlin = "TOP"; //название
    let tabOptIn = document.getElementById(codDlin + codElem); //куда добавить строчку
    let arrL = changedArrDlin[codElem][1];
    let LSelIn = "";//не исп.
    let countSelIn = "";//не исп.
    let newRiga = [];
    let selected = ""; //выбран по умолч-ю //не исп.
    let sumLOpt = document.getElementById(codElem + 'sumLOpt'); //Итого: общая длина
    let sumL = 0;
    let addButton = document.getElementById('bAddChanges');
    //console.log(newRiga);
    newRiga = SetRiga(CreateRiga(), oldArrDlin[codElem][0]);

    changedArrDlin[codElem][0].push(newRiga); //+1 в массив
    let n = changedArrDlin[codElem][0].length;
    let classCss = "odd"; //цвет фона четной
    if (n % 2 == 0) classCss = "even"; //не четная

    /*
    if (0) {
        //составляю список количества от 1 до 9
        for (let ii = 0; ii < 9; ii++) {
            let nn = ii + 1;
            if (nn == changedArrDlin[codElem][0][n - 1].PZ) selected = "selected"; //выбран по умолч-ю
            countSelIn += '<option value="' + nn + '" ' + selected + '>' + nn + '</option>';
            selected = "";
        }
        //id = № длинномера + countSel + № списка
        let countSel = '<select id="' + codElem + 'countSel' + n + '" onchange="editCountSelDlin(event.target)">' + countSelIn + '</select>';//список количества

        //составляю список длин
        for (let ii = 0; ii < arrL.length; ii++) {
            if (arrL[ii] == changedArrDlin[codElem][0][n - 1].L) selected = "selected"; //выбран по умолч-ю
            LSelIn += '<option value="' + arrL[ii] + '" ' + selected + '>' + arrL[ii] + '</option>';
            selected = "";
        }
        //id = № длинномера + LSel + № списка
        let LSel = '<select id="' + codElem + 'LSel' + n + '" onchange="editLSelDlin(event.target)">' + LSelIn + '</select>';    //список длин


        //вывод
        let addImg = tabOptIn.lastChild; //+
        tabOptIn.lastChild.remove();
        tabOptIn.innerHTML += '<p class="' + classCss + '" id="' + codElem + 'p' + n + '">' + LSel + ' кол.: ' + countSel + '</p>';
        //tabOptIn.innerHTML += addImg; //вернул //возвр только object
        tabOptIn.appendChild(addImg); //вернул
    }*/

    let newRow = document.createElement('p');
    newRow.className = classCss;
    newRow.id = codElem + 'p' + n; //0p1

    //длина
    let LSel = document.createElement('select');
    LSel.id = codElem + 'LSel' + n;
    for (let i = 0; i < arrL.length; i++) {
        let opt1 = document.createElement("option");
        arrL[i] = topLOut(arrL[i], changedArrDlin[codElem][0][n - 1].VAR);
        opt1.value = opt1.text = arrL[i];
        //if (arrL[i] == changedArrDlin[codElem][0][n - 1].L) LSel.selected = true; //выбран по умолч-ю
        LSel.add(opt1);
    }
    //LSel.onchange = function (event) { editLSelDlin(event.target); };
    //LSel.addEventListener("change", function (event) { editLSelDlin(event.target); });

    //Кол-во
    let countSel = document.createElement('select');
    countSel.id = codElem + 'countSel' + n;
    for (let i = 1; i < 10; i++) {
        let opt2 = document.createElement("option");
        opt2.value = opt2.text = i;
        //if (arrL[i] == changedArrDlin[codElem][0][n - 1].L) LSel.selected = true; //выбран по умолч-ю
        countSel.add(opt2);
    }
    //countSel.onchange = mytest;
    //countSel.onchange = function (event) { editCountSelDlin(event.target); };
    countSel.addEventListener("change", function (event) { editCountSelDlin(event.target); });

    //удалить
    let imgDel = document.createElement('img');
    imgDel.title = "Удалить";
    imgDel.src = '..\\..\\' + percorso + '\\HTML\\images\\minus.png';
    imgDel.width = 16;
    imgDel.style.position = "relative";
    imgDel.style.right = "-5px";
    imgDel.style.top = "-3px";
    imgDel.style.cursor = "pointer";
    imgDel.onclick = delRowDlin; //event

    //вывод
    newRow.appendChild(LSel);
    newRow.innerHTML += " кол.: ";
    newRow.appendChild(countSel);
    newRow.appendChild(imgDel);
    let addImg = tabOptIn.lastChild; //+
    target.parentElement.parentElement.appendChild(newRow);
    target.parentElement.parentElement.appendChild(addImg);
    changedArrDlin[codElem][2] = target.parentElement.parentElement.lastChild.outerHTML;

    $("#" + LSel.id).change(editLSelDlin); //костыль Для ивента

    for (let i = 0; i < changedArrDlin[codElem][0].length; i++) {
        if (changedArrDlin[codElem][0][i] !== "undefined") { //для delete
            sumL += changedArrDlin[codElem][0][i].L * changedArrDlin[codElem][0][i].PZ;
            //console.log(changedArrDlin[codElem][0][i].L + '*' + changedArrDlin[codElem][0][i].PZ);
        }
    }
    sumLOpt.innerHTML = sumL;

    addButton.textContent = "Внести изменения";
    addButton.style.color = "black";
    addButton.disabled = false;

    addRowDlinChanges(codElem);

    //console.log(LSel);
    //console.log(changedArrDlin[codElem][2]);
}

//в ручном общие действия для ивентов
function addRowDlinChanges(codElem) {
    let tabFindOutL = +document.getElementById(codElem + 'tabFindOutL').innerHTML; //общая длина в Найдено
    let addNewRow = document.getElementById(codElem + 'addNewRow'); //+
    let tabOptIn = document.getElementById('TOP' + codElem);
    let sumL = 0;
    let addButton = document.getElementById('bAddChanges');
    //changedArrDlin[codElem][2] = addNewRow.outerHTML;

    //нахожу сумму кусков
    for (let i = 0; i < changedArrDlin[codElem][0].length; i++) {
        if (changedArrDlin[codElem][0][i] !== "undefined") { //для delete
            sumL += changedArrDlin[codElem][0][i].L * changedArrDlin[codElem][0][i].PZ;
            //console.log(changedArrDlin[codElem][0][i].L + '*' + changedArrDlin[codElem][0][i].PZ);
        }
    }

    //удаляю "+" если итого сумма больше пришедшего
    if (tabFindOutL < sumL) {
        addNewRow.outerHTML = "";
        console.log('превышение ' + tabFindOutL + '<' + sumL);
    } else if (tabOptIn.lastChild.id !== codElem + 'addNewRow') {
        tabOptIn.innerHTML += changedArrDlin[codElem][2];
    }
    //console.log(addNewRow);

    //return sumL;
}

function mytest(change) {
    console.log(change.target);
}

//ивент на удаление куска
function delRowDlin(change) {
    //console.log(change.target);
    let myTarget = change.target;
    let codElem = myTarget.parentElement.id.split("p")[0];
    let row = myTarget.parentElement.id.split("p")[1];
    let sumLOpt = document.getElementById(codElem + 'sumLOpt'); //Итого: общая длина
    let sumL = 0;
    let addButton = document.getElementById('bAddChanges');

    //delete changedArrDlin[codElem][0][row - 1];
    changedArrDlin[codElem][0].splice(row - 1, 1);//начиная с позиции 1, удалить 1 элемент
    myTarget.parentElement.outerHTML = "";
    console.log('удаление куска: строка ' + row);
    //myTarget.parentElement.remove(); //гребаное IE
    //myTarget.parentElement.parentElement.removeChild(myTarget.parentElement);

    for (let i = 0; i < changedArrDlin[codElem][0].length; i++) {
        if (changedArrDlin[codElem][0][i] !== "undefined") { //для delete
            sumL += changedArrDlin[codElem][0][i].L * changedArrDlin[codElem][0][i].PZ;
            //console.log(changedArrDlin[codElem][0][i].L + '*' + changedArrDlin[codElem][0][i].PZ);
        }
    }
    sumLOpt.innerHTML = sumL;

    addButton.textContent = "Внести изменения";
    addButton.style.color = "black";
    addButton.disabled = false;

    addRowDlinChanges(codElem);
}

//ивент изменения кол-ва куска
function editCountSelDlin(select) {
    //console.log('изменения кол-ва'); //1countSel2
    let codElem = select.id.split("countSel")[0]; //кодировка длинномера //1
    let row = select.id.split("countSel")[1]; //№ куска //2
    let sumLOpt = document.getElementById(codElem + 'sumLOpt'); //Итого: общая длина
    let sumL = 0;
    let addButton = document.getElementById('bAddChanges');
    console.log('кол-ва куска: строка ' + row + ' с кол-ом ' + select.value);

    changedArrDlin[codElem][0][row - 1].PZ = +select.value;

    for (let i = 0; i < changedArrDlin[codElem][0].length; i++) {
        sumL += changedArrDlin[codElem][0][i].L * +select.value
    }
    sumLOpt.innerHTML = sumL;

    //console.log(changedArrDlin[codElem][0][row - 1]);
    //console.log(changedArrDlin[codElem][0][row - 1].PZ);
    //console.log(sumLOpt);

    addButton.textContent = "Внести изменения";
    addButton.style.color = "black";
    addButton.disabled = false;

    addRowDlinChanges(codElem);
}

//ивент изм-я длины куска
function editLSelDlin(change) {
    //console.log(select);
    let select = change.target;
    let codElem = select.id.split("LSel")[0]; //кодировка длинномера
    let row = select.id.split("LSel")[1]; //№ куска
    let riga = changedArrDlin[codElem][0][row - 1];
    let sumLOpt = document.getElementById(codElem + 'sumLOpt'); //Итого: общая длина
    let sumL = 0;
    let newName = findTag(riga.VAR, "MYNAME");
    let addButton = document.getElementById('bAddChanges');
    newName = newName.replace(riga.L, select.value);
    riga.VAR = findTagReplace(riga.VAR, "MYNAME", newName);
    console.log('длины куска: строка ' + row + ' с длиной ' + select.value);

    changedArrDlin[codElem][0][row - 1].L = +select.value;
    //changedArrDlin[codElem][0][row - 1] = riga;

    for (let i = 0; i < changedArrDlin[codElem][0].length; i++) {
        sumL += changedArrDlin[codElem][0][i].L * changedArrDlin[codElem][0][i].PZ
    }
    sumLOpt.innerHTML = sumL;

    addButton.textContent = "Внести изменения";
    addButton.style.color = "black";
    addButton.disabled = false;

    addRowDlinChanges(codElem);
}

//кнопка внести изменения
function addChanges(target) {
    //console.dir(target);
    let codElem = 0;
    let rigaIndex = 0;
    var isDelete = new Boolean(false); //запрос на удаление столешек
    let addButton = document.getElementById('bAddChanges');
    addButton.textContent = "Готово";
    addButton.style.color = "green";
    addButton.disabled = true;

    for (let i = 0; i < changedArrDlin.length; i++) {
        if (changedArrDlin[i].length > 0) {
            isDelete = true;
        }
    }

    if (isDelete) {        //Удаляем  
        for (let key in data.PREV.RIGHE) {
            rigaIndex = _.parseInt(key.replace('RIGA', ''));
            if (data.PREV.RIGHE[key].COD === "TOP") {
                delete data.PREV.RIGHE[key];
            }
        }
    }

    //Номер строки 
    for (let i = 0; i < changedArrDlin.length; i++) {
        if (changedArrDlin[i][0].length > 0) {
            rigaIndex++;
            changedArrDlin[i][0].forEach(function (r) {
                data.PREV.RIGHE['RIGA' + rigaIndex] = r;
                rigaIndex++;
            });
        }
    }
    //console.log(data);
    postJsonPrev();
    myTable();
    //isDelete = false;
    //console.log('changed!');
    //console.log(jsonPrev.PREV.RIGHE.RIGA6.PZ);
    console.log(data.PREV.RIGHE);
    //console.log(changedData.PREV.RIGHE.RIGA6.PZ);
    //console.log(copyData.PREV.RIGHE.RIGA6.PZ);

}








//console.log(jsonPrev.PREV.RIGHE.RIGA4.REAL_L);

function load() { //рисую модуль без логики
    dlinContainerTest = '<div class="dlinContainer ' + leftPos + '"> \
                        <h2>Длинномеры:</h2>\
                        <div class="dlinContent1">\
                            <div class="gorLine"></div>\
                            <div class="dlinHead">Столешницы. Просчёт: \
                                <select size="1">\
                                    <option selected>Автоматически</option>\
                                    <option>Без изменений</option>\
                                    <option>Ручной</option>\
                                </select>\
                            </div>\
                            <div class="tab">\
                                <div class="tabFind">\
                                    Найдено:\
                                    <p class="odd">1 длиной 150 (1500)</p>\
                                    <p class="even">1 длиной 350 (1500)</p>\
                                </div>\
                                <div class="tabOpt1">\
                                    Оптимизировано:\
                                    <p class="odd">1 длиной 450 (1500)</p>\
                                </div>\
                                <div class="clear"></div>\
                            </div>\
                            <div class="total">\
                                <div class="tabFind">\
                                    <p class="totalBad">Итого: 2 куска по 1500</p>\
                                </div>\
                                <div class="tabOpt1">\
                                    <p class="totalGood">Итого: 1 кусок по 1500</p>\
                                </div>\
                                <div class="clear"></div>\
                            </div>\
                            <button disabled>Внести изменения</button>\
                            <div class="clear"></div>\
                        </div >\
                    </div > ';
    document.querySelector("body").innerHTML += dlinContainerTest; //вывод
}

//Класс модуля
/*
class DlinClass {
    //prop = value; // свойство

    constructor(name, data, text) { //автоматически вызывается метод
        // вызывает сеттер
        this.name = name; //имя
        this.data = data; //длинномеры она пуста
        this.text = text;
    }

    get name() {// геттер, по сути функция и может быть getName()
        return this._name;
    }
    set name(value) {// сеттер  //НЕТ типо берет 1й аргумент из конструктора
        //имя по умолчанию
        if (value === "" || value === "undefined" || typeof (value) === "undefined") {
            return this._name = "Нет имени";
        } else this._name = value;
    }

    //метод для нахождения кусков с таблицы
    get data() {
        return this._data;
    }
    set data(data) {
        let a = _.filter(jsonPrev.PREV.RIGHE, { 'COD': 'TOP' }); //список длин-в
        this._data = a;
        //console.log(typeof (a))
        //console.log(_.filter(data.PREV.RIGHE, { 'COD': 'TOP' }));
    }
    static set_Data(jsonPrev) {//test
        return _.filter(jsonPrev.PREV.RIGHE, { 'COD': 'TOP' });
    }

    //метод для рисования модуля
    drawDlin(jsonPrev) {
        //таб. "найдено"
        let n = 1; //кол-во кусков
        let sumL = 0; //сумма кусков
        let find = "";
        let ss = _.filter(jsonPrev.PREV.RIGHE, { 'COD': 'TOP' });
        ss.forEach(function (item, index) {
            //let nn = jsonPrev.PREV.RIGHE[key];
            let classCss = "odd"; //цвет фона четной
            if (n % 2 == 0) classCss = "even"; //не четная
            find += '<p class="' + classCss + '">' + n + ' длиной ' + item.REAL_L + '\
             ('+ item.L + ')</p>'
            n++;
            sumL += item.L;
            //console.log(item);
        })
        //console.log(set_Data(jsonPrev));

        let container = '<div class="dlinContainer ' + leftPos + '"> \
                            <h2>Длинномеры:</h2>\
                            <div class="gorLine"></div>\
                            <div class="dlinContent">\
                                <div class="dlinHead">'+ this.name + '. Просчёт: \
                                    <select size="1">\
                                        <option selected>Автоматически</option>\
                                        <option>Без изменений</option>\
                                        <option>Ручной</option>\
                                    </select>\
                                </div>\
                                <div class="tab">\
                                    <div class="tabFind">\
                                        Найдено:\
                                        '+ find + '\
                                    </div>\
                                    <div class="tabOpt">\
                                        Оптимизировано:\
                                        <p class="odd">1 длиной 450 (1500)</p>\
                                    </div>\
                                    <div class="clear"></div>\
                                </div>\
                                <div class="total">\
                                    <div class="tabFind">\
                                        <p class="totalBad">Итого: общая длина '+ sumL + '</p>\
                                    </div>\
                                    <div class="tabOpt">\
                                        <p class="totalGood">Итого: 1 кусок по 1500</p>\
                                    </div>\
                                    <div class="clear"></div>\
                                </div>\
                                <button disabled>Внести изменения</button>\
                                <div class="clear"></div>\
                            </div >\
                        </div > ';

        return container;
    }

    get text() {
        return this._text;
    }
    set text(text) { //Для set нужет еще и get
        this._text = text + '!!!';
    }

    //метод для просчета кусков

    //метод проверки результатов (под вопросом)

    //метод для внесения изменений

    sayHi() {// метод
        //console.log(this.name);
        console.log(this.data);
        //console.log(this.text);
    }
}
//console.log(_.filter(data.PREV.RIGHE, { 'COD': 'TOP' }));

// Наследуем от DlinClass указывая "extends DlinClass"
class DlinTopClass extends DlinClass {
    sayHi() {
        super.sayHi();// вызываем родительский метод + можно добавить свое
    }
    drawTop() {
        super.drawDlin(jsonPrev);
    }
}

*/