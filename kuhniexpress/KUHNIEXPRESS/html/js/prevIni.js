﻿var x2js = new X2JS();
//var xml2js = require('./lib/index');
//var des = document.getElementById("description").innerHTML;
//var myname = document.getElementById("myname").innerHTML;
//var allL = document.getElementById("allL").innerHTML;   //длина изделия

var JsData = document.getElementById("JsData").innerHTML;
var JsBFio = document.getElementById("JsBFio").innerHTML;
var JsFioDis = document.getElementById("JsFioDis").innerHTML;
var JsRif = document.getElementById("JsRif").innerHTML;
var JsNumero = document.getElementById("JsNumero").innerHTML;
var JsXML = document.getElementById("JsXML").innerHTML;
//console.log(XML_FILE);

let table = [];
let tableNorm = [];
var tableTr = [];
var tempText = "";
var trStyle;
let pageInfo = [];
var jsonPrev = "";
let temp = {};
jsonPrev = (x2js.xml_str2json(XML_FILE));
//console.log(jsonPrev);

window.onload = function () {
	main(); //
}

function main() {
	//console.log((x2js.xml_str2json(XML_FILE)));
	//Исправляю var
	console.log(jsonPrev);
	var indexRiga = 0;
	delete jsonPrev.PREV.TESTA.VAR.__text;
	//console.log(jsonPrev.PREV.RIGHE.RIGA3.L);

	//Преобразую длинномеры
	getDlin(jsonPrev.PREV, 'PLINTUS', "PLINT_COUNT", "");
	getDlin(jsonPrev.PREV, 'SOCLE', "SOCLE_COUNT_MIN", "SOCLE_COUNT_MAX");
	getDlin(jsonPrev.PREV, 'KARNIZ', "KARNIZ_COUNT", "");
	getDlin(jsonPrev.PREV, 'KARNIZN', "KARNIZN_COUNT", "");
	getDlin(jsonPrev.PREV, 'BALUSTRADA', "BALUSTRADA_COUNT", "");
	//console.log(jsonPrev.PREV.RIGHE.RIGA4);

	//Выбираем только столешницы по полю COD = TOP
	var top = _.filter(jsonPrev.PREV.RIGHE, { 'COD': 'TOP' });

	//Перебираем массив столешниц
	top = top.map(function (item) {
		//Преобразуем сторку вариантов в массив
		item.VarArr = stringToMap(item.VAR);

		//Дописываю свойство - реальный размер
		item.REAL_L = +item.L;
		//console.log(item.REAL_L);
		return item;
	});
	//console.log(top);

	//Группируем столешницы по вариантам
	const keyVarGroup = ['TOP_MANUFACTURER', 'STOLESH_MAT', 'TOLSHINA', 'KROMKA'];
	var topGroup = _.groupBy(top, function (item) {
		//подбираем варианты для объединения столшниц
		// 1. TOP_MANUFACTURER - производитель 
		// 2. STOLESH_MAT - матариал 
		// 3. TOLSHINA - высота
		// 4. KROMKA - кромка по лицу

		//Фильтруем только нужные варианты
		var GroupKey = item.VarArr.filter(function (iVar) {
			return _.indexOf(keyVarGroup, iVar[0]) >= 0
		});
		//Преобразуем в строку <вариант>=<Значение>
		GroupKey = GroupKey.map(function (ikey) {
			return _.join(ikey, '=');
		});
		//объединяем все варинты в троку с разделителем ";"
		return _.join(GroupKey, ';');
	});

	//console.log(topGroup);
	typeAlgoritm = 1;
	if (typeAlgoritm === 1) {
		//Перебираем группы для группировки
		_.forEach(topGroup, function (itemGrp) {
			//Пребираем саму группу 
			//получаем длины хлыстов
			var fulltop = [];
			_.forEach(itemGrp, function (item) {
				var top_l = item.VarArr.filter(function (o) {
					return (o[0] === 'TOP_L')
				});
				fulltop = _.union(fulltop, top_l[0][1].split('*').map(function (item) { return Number(item) }));
				item.L = upInt(fulltop, item.L); //к столешнице присваиваю L
				//item.VAR
				//console.log(item);
			});
		});
		//console.log(topGroup);


	} else if (typeAlgoritm === 2) { //не исп
		//Перебираем группы для группировки
		for (let key in topGroup) {
			itemGrp = topGroup[key];
			//Пребираем саму группу 
			var fulltop = itemGrp.reduce(function (summ, item) {
				return summ + Number(item.L)
			}, 0);
			//console.log(fulltop);
			var arrTop_l = [];
			_.forEach(itemGrp, function (item) {
				var top_l = item.VarArr.filter(function (o) {
					return (o[0] === 'TOP_L')
				});
				arrTop_l = _.union(fulltop, top_l[0][1].split('*').map(function (item) { return Number(item) }));
			});

			var res = [];

			arrTop_l.sort(function (a, b) { return b - a; });
			for (let i = 0; i < arrTop_l.length; i++) {
				var cnt = Math.trunc(fulltop / arrTop_l[i]);
				res.push([arrTop_l[i], cnt, key]);
				fulltop = arrTop_l[i] * cnt;
			}
			if (fulltop > 0) {
				res[res.length - 1][1] = res[res.length - 1][1] + 1;
			}
		};
		//console.log(res);
	}

	//Дописываю в описание длину + ЛЕНТА КРОМКА 
	for (let key in jsonPrev.PREV.RIGHE) {
		let nn = jsonPrev.PREV.RIGHE[key];
		if (nn.COD === "TOP") {
			//console.log(nn.var + ' ' + nn.l)
			let kk = findTag(nn.VAR, "MYNAME");

			//зная уже длину стола хлыста, запоминаю её фактический размер из проекта для кромки
			let top_l = nn.L;

			//console.log(top_l);

			//ищю длину для вывода
			//nn.CALC_L = nn.L; //расчетная длина
			nn.L = topLOut(nn.L, nn.VAR); //перезаписываю длину для 1с
			nn.VAR = findTagReplace(nn.VAR, "MYNAME", kk + nn.L + ". Ш" + nn.A);

			//ЛЕНТА КРОМКА добавляю строчку
			let forLentaWidth = 0; //сумма ленты
			let lentaCount = 1; //количество ленты
			let lentaHeight = 45; //высота ленты
			let lentaLength = 3050; //длина ленты
			let manufacturer = findTag(nn.DETTPREZZO, "TOP_MANUFACTURER");
			let stolesh_mat = findTag(nn.DETTPREZZO, "STOLESH_MAT");

			//собираю длину
			if (findTag(nn.VAR, "KROMKA_L_COD") === "12") forLentaWidth = +nn.A;
			if (findTag(nn.VAR, "KROMKA_R_COD") === "12") forLentaWidth = +nn.A + forLentaWidth;
			if (findTag(nn.VAR, "KROMKA_Z_COD") === "12") forLentaWidth = top_l + forLentaWidth;
			//console.log(forLentaWidth);
			//console.log(findTag(nn.DETTPREZZO, "KROMKA_L_COD"));

			if (forLentaWidth > 0) {
				//считаю
				if (manufacturer === "KEDR" || manufacturer === "SOYUZ") {
					if (manufacturer === "KEDR") lentaHeight = 45;
					if (forLentaWidth > 0) lentaCount = Math.ceil(forLentaWidth / 3000); //округляю в большую
				} else if (manufacturer === "SLOTEX") {
					if (forLentaWidth <= 3000) { } //стандартно
					else if (forLentaWidth <= 4150) lentaLength = 4200;
					else if (forLentaWidth > 4150) lentaCount = Math.ceil(forLentaWidth / 4150);
				}
				//записываю
				let newRiga = CreateRiga();
				newRiga.COD = "KROMKA";
				newRiga.L = lentaLength;
				newRiga.A = lentaHeight;
				newRiga.P = "0.6";
				newRiga.PZ = lentaCount; //количечтво
				newRiga.VAR = "MYNAME=Лента кромка с клеем " + manufacturer + ", " + stolesh_mat + ", Д" + lentaLength + ";KROMKA_MAT=" + stolesh_mat + ";KROMKA_MANUFAKTURER=" + manufacturer;
				newRiga.IDBOX = nn.IDBOX;
				//nn.push(newRiga);
				//console.log(newRiga);
				jsonPrev.PREV.RIGHE['RIGA' + indexRiga + 1] = newRiga;//ЛЕНТА КРОМКА //тут могут быть косяки из-за indexRiga
				//console.log(jsonPrev.PREV.RIGHE);
			}
		}

	}
	//Переписываем XML 
	//Удаляем столешницы 
	for (let key in jsonPrev.PREV.RIGHE) {
		indexRiga = _.parseInt(key.replace('RIGA', '')); //
		if (jsonPrev.PREV.RIGHE[key].COD === "TOP") {
			delete jsonPrev.PREV.RIGHE[key];
		}

	}
	//console.log(indexRiga + 1);

	//Кол-во строк в righe
	//console.log(jsonPrev.PREV.RIGHE.RIGA4);
	//indexRiga = Object.keys(jsonPrev.PREV.RIGHE).length + 1; //
	indexRiga++;
	//Добавляем новые строчки //тут могут быть косяки
	_.forEach(topGroup, function (itemGrp) {
		_.forEach(itemGrp, function (item) {
			delete item['VarArr'];
			//if (!jsonPrev.PREV.RIGHE['riga' + indexRiga]) {
			jsonPrev.PREV.RIGHE['RIGA' + indexRiga] = item;
			indexRiga++;
			//}
		});
	});
	//console.log(jsonPrev.PREV.RIGHE.RIGA1);
	//console.log(topGroup);
	//console.log(Object.keys(jsonPrev.PREV.RIGHE).length);

	//console.log(jsonPrev);
	//getModulDlin(); //рисую модуль по длинномерам
	drawImgPreview($(".addprint"), IMG_LIST.split(';'));
	myTable(); //формируем таблицу
	addBoxInfo(jsonPrev);
	//printImg(); //устарело не исп
	printImg();
	printInfo($('.InfoPrint'), jsonPrev.PREV)
	printTable($('.tablePrint'), jsonPrev.PREV)
	postJsonPrev();//тут постобработка объекта к RIGA даю норм номер
	//console.log(jsonPrev);
	//console.log(jsonPrev.PREV.RIGHE.RIGA4.REAL_L);
	$("#SaveXml").click(this.SaveXml); //сохраняем в XML
}


function getDlin(PREV, cod, findCod, findCod2) {
	var arrRiga = [];
	var rigaIndex = 0;
	//Выбираем только плинтус
	var plint = _.filter(PREV.RIGHE, { 'COD': cod });
	plint.forEach(function (item) {
		//Читаем спецификацию цены
		rDetpresso = item.DETTPREZZO.split("#");
		rDetpresso = _.drop(rDetpresso, 1);//удаляет первый элемент
		//console.log(rDetpresso);
		//console.log(item.VAR);
		rDetpresso.forEach(function (r) {
			let s = [];
			s = r.split(";");
			//console.log(r);
			//console.log(s);
			//if (varVar === item.VAR || (s[1] === findCod || s[1] === findCod2)) { //чувак без вариантов или одинаковым вариантом
			if (s[1] === findCod || s[1] === findCod2) { //чувак без вариантов или одинаковым вариантом
				let riga = CreateRiga();
				riga.COD = cod;
				riga.L = s[7].split(',')[1];
				riga.A = item.A;
				riga.P = item.P;
				riga.PZ = s[6];
				riga.VAR = item.VAR;
				riga.IDBOX = item.IDBOX;
				riga.REAL_L = +item.L; //Дописываю +свойство реальный размер
				arrRiga.push(riga);
			} else {//if (s[1] !== findCod && s[1] !== findCod2) {//чувак с личным вариантом
				let varVar = "";
				if (s.length >= 8) {
					for (let i = 8; i < s.length; i++) { //пересобираю варианты для элемента
						s[i] = s[i].split("=")[0].toUpperCase() + "=" + s[i].split("=")[1];//делаю большими буквами варианты
						if (i === s.length - 1) {
							varVar += s[i];
						} else {
							varVar += s[i] + ";";
						}
					}
				}
				varVar = varVar + ";";
				let riga = CreateRiga();
				riga.COD = s[1];//'ZAGL_COLOR'
				if (typeof s[7] === 'undefined') { riga.L = 0 } else { riga.L = s[7].split(',')[1]; }
				riga.PZ = s[6];
				riga.A = item.A;
				riga.P = item.P;
				riga.VAR = varVar;
				riga.IDBOX = item.IDBOX;
				arrRiga.push(riga);
			}
		});
	});
	//Дописываю в описание длину
	for (let i = 0; i < arrRiga.length; i++) {
		if (arrRiga[i].COD === cod) {
			//console.log(nn.VAR + ' ' + nn.L)
			let kk = findTag(arrRiga[i].VAR, "MYNAME");
			arrRiga[i].VAR = findTagReplace(arrRiga[i].VAR, "MYNAME", kk + ", Д" + arrRiga[i].L);
		}
	}
	//console.log(arrRiga);
	//Добавляем новые строки и удаляем старые
	if (arrRiga.length > 0) {
		//Удаляем плинтус 
		for (let key in PREV.RIGHE) {
			//Номер строки 
			rigaIndex = _.parseInt(key.replace('RIGA', ''));
			if (PREV.RIGHE[key].COD === cod) {
				delete PREV.RIGHE[key];
			}
		}
		rigaIndex++;
		arrRiga.forEach(function (r) {
			PREV.RIGHE['RIGA' + rigaIndex] = r;
			rigaIndex++;
		});
	}

	//ПОСТОБРАБОТКА
	//Цоколя
	soclePostProc(arrRiga);
	//console.log(arrRiga);
}

//Постобработка цоколя
function soclePostProc(arrRiga) {
	//console.log(arrRiga);
	for (let i = 0; i < arrRiga.length; i++) {
		if (arrRiga[i].COD === "SOCLE") {
			let myarticle = findTag(arrRiga[i].VAR, "ARTICLE"); //беру значение тега
			myarticle = myarticle + '\\' + arrRiga[i].L / 1000 + "m"; // \
			arrRiga[i].VAR = arrRiga[i].VAR + ";MYARTICLE=" + myarticle;//и добавляю в варианты
			arrRiga[i].VAR = arrRiga[i].VAR + ";SOCLE_ART=" + myarticle;
		}
	}
	return arrRiga;
}


function topLOut(L_In, variant) { //вывод L_Out длин для 1с для столешек
	let topManuf = findTag(variant, "TOP_MANUFACTURER");
	let topFormCod = findTag(variant, "TOP_FORM_COD");
	let topForm = findTag(variant, "RADIUS");
	let topKromka = findTag(variant, "KROMKA");
	let top_find_l = findTag(variant, "TOP_FIND_L"); //из проги беру размер
	//console.log(top_find_l);
	let L_Out = 0;
	if (topFormCod === "01" || topForm === "Прямоуголная") {
		//console.log(topFormCod);
		if (topManuf === "SOYUZ") {
			if (topKromka === "ABS") {
				L_Out = L_In;
			} else {
				if (L_In == 3000) {
					L_Out = 3050;
				} else if (L_In == 4150) {
					L_Out = 4200;
				} else {
					L_Out = L_In;
				}
			}
		} else if (topManuf === "KEDR") {
			if (L_In == 3000) {
				L_Out = 3050;
			} else if (L_In === 4000) {
				L_Out = 4100;
			} else {
				L_Out = L_In;
			}
		} else if (topManuf === "SLOTEX" || topManuf === "SLOTEXKL") {
			if (topKromka === "ABS") {
				L_Out = L_In;
			} else {
				if (L_In == 3000) {
					L_Out = 3050;
				} else if (L_In == 4150) {
					L_Out = 4200;
				} else {
					L_Out = L_In;
				}
			}
		} else if (topManuf === "SUMSUNG" || topManuf === "HIMACKS") {
			if (L_In == 3000) {
				L_Out = 3680;
			} else if (L_In == 2250) {
				L_Out = 2760;
			} else if (L_In == 1500) {
				L_Out = 1840;
			} else if (L_In == 750) {
				L_Out = 920;
			}
		}
	} else { //фигурные
		if (topManuf === "SLOTEXKL") {
			if (L_In == 3000) {
				L_Out = 3050;
			} else if (L_In == 4150) {
				L_Out = 4200;
			} else {
				L_Out = L_In;
			}
		} else if (topManuf === "SUMSUNG" || topManuf === "HIMACKS") {
			if (L_In == 3000) {
				L_Out = 3680;
			} else if (L_In == 2250) {
				L_Out = 2760;
			} else if (L_In == 1500) {
				L_Out = 1840;
			} else if (L_In == 750) {
				L_Out = 920;
			}
		} else {
			L_Out = top_find_l; //спорный момент
		}
	}
	return L_Out;
}
//Преобразуем строку варинтов в массив [ключ,значение]
function stringToMap(str) {
	return str.split(';').map(function (item) {
		return item.split('=');
	});
}

//Поиск ближайшего большего
function upInt(arr, a) {
	for (let i = 0; i < arr.length; i++) {
		if (arr[i] >= a) {
			res = arr[i];
			return res
		}
	}
}
function SaveXml() {
	var xml = '<?xml version="1.0"?>' + x2js.json2xml_str(jsonPrev);
	//xml = encodeCP1251(xml);
	var blob = new Blob([xml], { type: "text/plain" });
	saveAs(blob, "express.xml");
}
//поиск тега в вариантах
function findTag(string, tagName) {
	if (string.indexOf(tagName) !== -1) {
		let n = string.split(tagName)[1].split("=")[1].split(";")[0];
		return n;
	} else {
		return "";
	}
}
//Поиск тега  в вариантах с изменением содежимого, возвр. VAR
function findTagReplace(string, tagName, textForReplace) {
	if (string.indexOf(tagName) !== -1) {
		let arr = []
		arr = string.split(";");
		for (let i = 0; i < arr.length; i++) {
			const tag = arr[i].split("=")[0];
			//let value = arr[i].split("=")[1];
			if (tag === tagName) {
				arr[i] = tagName + '=' + textForReplace;
			}
		}
		return arr.join(";");
	} else {
		return string;
	}
}


//Добавляю страницы с инфой для печали
function addBoxInfo(jsonPrev) {
	let box = []; //корпуса
	let temp = [];
	let temp2 = [];
	//console.log(jsonPrev);
	//Перебираю строки по <IDBOX> кроме длинномеров
	//var plint = _.filter(PREV.RIGHE, { 'cod': cod });
	let i = 1;
	let lastIdbox = 0;
	let t = "";
	_.forEach(jsonPrev.PREV.RIGHE, function (item) {
		//console.log(item);
		let korpusname = findTag(item.VAR, 'korpusname');
		let myName = findTag(item.VAR, 'MYNAME');
		if (item.IDBOX == lastIdbox) {//в этом <IDBOX>
			t += '<span class="classIndent">' + item.DES + ' ' + myName + '</span><br>';
		} else {//переход к след. <IDBOX>
			t += '<span>' + i + '. ' + item.DES + ' ' + korpusname + myName + '</span><br>';
			i++;
		}

		lastIdbox = item.IDBOX;

	})
	pageInfo = t;
	/*
		//Вывожу box
		for (n = 1; n < box.length; n++) {//корпусa
			let t = "";
			let classIndent = "";
			for (let i = 0; i < box[n].length; i++) {//строки
				if (i > 0) { classIndent = "classIndent"; }//отступ страва
				t += '<span class="' + classIndent + '">' + box[n][i][2] + ' ' + box[n][i][3] + '</span><br>';
			}
			pageInfo += '\
				<div class = "wrapPagelabel">\
					'+ n + '. ' + t + '' + '<br>\
				</div>\
				';
		}
		for (let i = 0; i < temp2.length; i++) {//остальные
			pageInfo += '\
			<div class = "wrapPagelabel">\
				'+ n + '. ' + temp2[i][2] + ' ' + temp2[i][3] + '' + '\
			</div><br>\
			';
			n++;
		}
		let t = "";
		for (let i = 0; i < tableNorm.length; i++) {//Длинномеры
			if (dlinnomer.indexOf(tableNorm[i][1]) !== -1) {
				t += '<span class="classIndent">' + tableNorm[i][2] + ' ' + tableNorm[i][3] + '</span><br>';
			}
		}
		pageInfo += '\
		<div class = "wrapPagelabel">\
			Длинномеры: <br> ' + t + '\
		</div>\
		';*/
	pageInfo = '\
		<div class="wrapPageInfo">\
			'+ pageInfo + '\
			<table class="titleBlock">\
				<tr><td>Заказ № '+ JsNumero + '</td> <td>Дата: ' + JsData + '</td></tr>\
				<tr><td colspan="2">Покупатель: '+ JsBFio + '</td></tr>\
				<tr><td colspan="2">Дизайнер: '+ JsFioDis + '</td></tr>\
				<tr><!--<td colspan="2">Примечание: '+ JsRif + '</td></tr>-->\
			</table>\
		</div>\
		';

	//document.querySelector('#footer').innerHTML = pageInfo;
}

//Таблица//
function showTd(i) {
	document.getElementById(i).style.display = "block";
}

function trTop() {
	let tempTable = [];
	var topL = 0;
	let arrKIn = [];

	//console.log(table.length);
	//проверка свойств столов по сохожести table[n][j]
	for (var n = 0; n < table[1].length; n++) {
		//console.log(table[0][n]);			
		if (table[1][n] === "TOP") {
			//console.log(n);
			for (var j = 0; j < table.length; j++) {
				//tempTable[j][n].shift()
				//tempTable = table[j].slice(n)
			}
			//tempTable
		}
	}
	//console.log(table.length);
	//console.log(table[1].length);
	//console.log(table);	
	//let zzz = [[01,02,03],[04,05,06],[07,08,09]]
	//zzz=zzz.slice(0,2);   //[[01,02,03],[04,05,06]]
	//zzz=zzz[0].slice(0,2);//[01,02]
	//console.log(zzz);

	for (var n = 0; n < tableNorm.length; n++) {
		if (tableNorm[n][1] === "TOP") {
			tempTable.push(tableNorm.slice(n, n + 1)[0]);
		}
	}

	for (var n = 0; n < tempTable.length; n++) {
		topL += + tempTable[n][7]; //общая длина
	}
	//console.log(topL);


	//функция нарезки кусков
	let arrOfL = [4150, 4000, 3600, 3000, 2250, 1750, 1500, 750];
	//let arrOfL = l.split(",");
	//arrK = [0,0,0,0,0,0,0,0];
	let arrK = [];
	for (var n = 0; n < arrOfL.length; n++) { arrK.push("0") };
	let piece = [arrOfL, arrK];
	//var arrOfLIn_ = 4000,3000,1500 //пришедший массив размеров
	//let arrOfLIn = arrOfLIn_.split(",");
	topL = 7500;
	let arrOfLIn = [4000, 3000, 1500]; //пришедший массив размеров
	arrOfLIn.sort(function (a, b) { return b - a; }); //сортировка по убыванию
	//console.log(topL);
	for (var n = 0; n < arrOfLIn.length; n++) {
		arrKIn.push(Math.floor(topL / arrOfLIn[n]));
		topL -= arrKIn[n] * arrOfLIn[n];
		//console.log(topL+' '+arrOfLIn[n]);
	}
	//остаток записываю к мин. длине
	if (topL > 0) {
		arrKIn[arrOfLIn.length - 1] += 1;
	}
	function k() { }


	//console.log(arrKIn);
	//console.log(arrOfLIn); 
	//перебираем бывший массив на наличие в нём элементов нового и сохраняем в многомерном кол-во
	for (var i = 0; i < piece[0].length; i++) {    //большой брат
		for (var j = 0; j < arrOfLIn.length; j++) {//братишка
			if (piece[0][i] == arrOfLIn[j]) {
				//console.log(piece[0][i] + ', ' + arrOfLIn[j]); // [0]-первый массив [i] элемент
				piece[1][i] = 1; //кол. кусков
			}
		}
	}
	//console.log(piece[1]);

	//добавляем сточки в массив table
	for (var i = 0; i < piece[1].length; i++) {
		if (piece[1][i] > 0) {
			//console.log(piece[1][i] +' '+ i);
			//table.push();
		}
	}

	//document.getElementById("allLOut").innerHTML = allL;
	//выводим строку для каждого куска
	//console.log(document.getElementById("mytable").innerHTML);
}

//Длинномеры//
function trDlin(cod, min, max) {
	let tempTable = [];
	//Метод splice() изменяет содержимое массива, удаляя существующие элементы и/или добавляя новые.
	//Метод slice() возвращает новый массив, содержащий копию части исходного массива.
	//пересобираем таблицу исключяя длин.
	for (var n = 1; n < tableNorm.length; n++) {
		if (tableNorm[n][1] === cod) {
			tempTable.push(tableNorm.splice(n, 1)[0]);
		}
	}
	//console.log(tableNorm);	
	//console.log(tempTable[0].length);
	//tableNorm = table2;

	//считываем и записываем мин и макс длины в конец массива
	for (var n = 0; n < tempTable.length; n++) {
		//создаю место для записи
		//console.log(tempTable[n][5].indexOf(min) !== -1 && max !== 0);
		tempTable[n] = tempTable[n].concat([0, 0, 0, 0]);
		//"01;PLINTUS;0;;;;1;,0,0,0;MYNAME=Плинтус вставка,SOYUZ,Венге 1М;PLINT_PRODUCT=SOYUZ;TYPE_OF_PLINT=Плинтус вставка;TYPE_OF_PLINT_COD=PLVstavka;PLINT_COLOR=Венге 1М;ZAGL_COLOR=600<br>01;PLINT_COUNT_MIN;0;;;;1;,1200,0,0;plint_count_min=1<br>01;ZAGLUSHKA;0;;;;1;,0,0,0;ZAGL_COLOR=600"
		//"01;KARNIZ;0;;;;1;,0,0,0;MYNAME=Карниз верхний К3,ВТС   ,Акация белая DM1001 ,Без патины;KARN_PRODUCT=Карниз верхний К3;IZGOT=ВТС;KARN_COLOR=Акация белая DM1001;PATINA=Без патины;KARN_L1=2800<br>01;KARNIZ_COUNT;0;;;;1;,2800,0,0;karniz_count=1"
		//в 10 столбец записываю MIN количество, в 11 длину
		if (tempTable[n][5].indexOf(min) !== -1 && min !== 0) {
			tempTable[n][(tempTable[n].length - 4)] = +((((tempTable[n][5].split(min))[1].split(";"))[5]));//13 количество
			tempTable[n][(tempTable[n].length - 3)] = +((((tempTable[n][5].split(min))[1].split(";"))[6].split(","))[1]);//14 длину L=1200
		}
		//в 12 столбец записываю MAX количество, в 13 длину
		if (tempTable[n][5].indexOf(max) !== -1 && max !== 0) {
			tempTable[n][(tempTable[n].length - 2)] = +((((tempTable[n][5].split(max))[1].split(";"))[5]));//15
			tempTable[n][(tempTable[n].length - 1)] = +((((tempTable[n][5].split(max))[1].split(";"))[6].split(","))[1]);//16 L=1200
		}
	}

	//тут проверка на совпадение свойств для отдельного счета если различны	длин.

	//выводим строки с количеством последними
	for (var n = 0; n < tempTable.length; n++) {
		let cc = tempTable[n][3];
		if (tempTable[n][tempTable[n].length - 4] > 0) {//min
			tableNorm.push(tempTable[n]);
			tempTable[n][3] = cc + ", L=" + tempTable[n][tempTable[n].length - 3];
			tableNorm[tableNorm.length - 1][6] = tempTable[n][tempTable[n].length - 4];
		}
		if (tempTable[n][tempTable[n].length - 2] > 0) {//max
			tableNorm.push(tempTable[n]);
			tempTable[n][3] = cc + ", L=" + tempTable[n][tempTable[n].length - 1];
			tableNorm[tableNorm.length - 1][6] = tempTable[n][tempTable[n].length - 2];
		}
	}
	//console.log(tempTable);
	//console.log(tableNorm);
}

function myTable() {
	//формируем таблицу, отсчёт ячеек начинается с 1
	//%1-$(cod)%2-$(des)%3-$(myname)%4-$(varianti)%5-$(Dettprezzo)%6-$(Pz)%7-$(L)%-8$(A)%9-$(P)
	//%10-$(IDBOX)%11-$(CODNEUTRO)%12-$(IDUNICO)
	//в конце tableNorm.length-n:-1/13 myarticle;
	var n = 0;

	// let tableT = JsString.split("&amp;");
	// for (n = 0; n < tableT.length; n++) { //нормальная таблица
	// 	tableNorm.push(tableT[n].split("%"));
	// }
	// for (n = 1; n < tableNorm.length; n++) { //перевожу текст в число + дополняю
	// 	tableNorm[n][8] = Number(tableNorm[n][8]);
	// 	tableNorm[n][9] = Number(tableNorm[n][9]);
	// 	tableNorm[n][10] = Number(tableNorm[n][10]);
	// 	tableNorm[n][12] = Number(tableNorm[n][12]);
	// 	if (tableNorm[n][4].indexOf('MYARTICLE') !== -1) {//артикул
	// 		tableNorm[n].push(tableNorm[n][4].split('MYARTICLE')[1].split('-')[0].split('=')[1]);
	// 		//console.log(tableNorm[n][4].split('MYARTICLE')[1].split('-')[0].split('=')[1]);
	// 	} else { tableNorm[n].push('') }
	// }

	//console.log(tableNorm);
	//Редактор таблицы
	//trTop();//столешница
	dlinnomer = "TOP, PLINTUS, SOCLE, KARNIZ, KARNIZN, BALUSTRADA";
	// trDlin("PLINTUS", "PLINT_COUNT_MIN", "PLINT_COUNT_MAX");
	// trDlin("SOCLE", "SOCLE_COUNT_MIN", "SOCLE_COUNT_MAX");
	// trDlin("KARNIZ", "KARNIZ_COUNT", 0);
	// trDlin("KARNIZN", "KARNIZN_COUNT", 0);
	// trDlin("BALUSTRADA", "BALUSTRADA_COUNT", 0);

	//выводим строчки таблицы
	// for (n = 1; n < table[1].length; n++) {	
	// //вычисляю box
	// if ( isFinite(table[1][n].slice(0,3))) {trStyle="mainT"} else {trStyle=""};//надеюсь в Code всегда будет не пусто			
	// addTr(n,trStyle);
	// }
	//console.log(jsonPrev.PREV.RIGHE);
	n = 1;
	for (let key in jsonPrev.PREV.RIGHE) {
		//вычисляю box

		let a = jsonPrev.PREV.RIGHE[key];
		//console.log(a.COD);
		if (a.MODELLO !== "") { trStyle = "mainT" } else { trStyle = "" };//надеюсь в Code всегда будет не пусто			
		addTr(n, trStyle, a);
		n++;
	}


	document.getElementById("myTbl").innerHTML = '<table id="mytable">\
			<thead>\
			<tr>\
				<th class="styleTableHead" width="15px">№</th>\
				<th class="styleTableHead" width="50px">Код</th>\
				<th class="styleTableHead" width="525px">Наименование</th>\
				<th class="styleTableHead" width="20px"><span>Количество</span></th>\
				<th class="styleTableHead" width="20px"><span>Артикул</span></th>\
				<!-- <th class="styleTableHead HiddenCol" width="10px"></th> -->\
			<tr>\
			</thead>\
			'+ tableTr + '\
			</table>\
			';
	tableTr = "";
}

function addTr(n, trStyle, key) {
	let myName = findTag(key.VAR, 'MYNAME');
	let korpusname = findTag(key.VAR, 'korpusname');
	//if (key === "TOP") { myName += ' ' + key.L }
	let myArticle = findTag(key.VAR, 'MYARTICLE');
	//console.log(myArticle);
	tableTr += '\
		<tr id="riga' + n + '" class="' + trStyle + '">\
			<td class="number" width="15px">' + n + '</td>\
			<td class="code" width="50px">' + key.COD + '</td>\
			<td class="des" width="525px" onload="loadDes()">\
				' + key.DES + ' ' + myName + korpusname + tempText + '\
			</td>\
			<td class="count" width="20px">' + key.PZ + '</td>\
			<td class="count" width="20px">' + myArticle + '</td>\
		</tr>\
	';
}

function CreateRiga() {
	let obj = Object.create(null);
	obj = {
		A: "",
		COD: "",
		CODDX: "",
		CODNEUTRO: "",
		DES: "",
		DESNEUTRO: "",
		DETTPREZZO: "",
		FLAGS: "",
		IDBOX: "",
		IDUNICO: "",
		L: "",
		MODELLO: "",
		NOTE: "",
		P: "",
		PESOL: "",
		PESON: "",
		PR1: "",
		PR2: "",
		PZ: "",
		QTA: "",
		SC1: "",
		SC2: "",
		SC3: "",
		SC4: "",
		VAR: "",
		VARIANTI: "",
		VOLUME: ""
	}
	return obj;
}

//В связке с CreateRiga()
function SetRiga(newRiga, oldRiga) {
	for (let key in oldRiga) {
		newRiga[key] = oldRiga[key];
	}
	return newRiga;
}

//TODO: 12/12/2019 yurii
//Добавление предпросмотра для формы.
//
function drawImgPreview(elm, ImgList) {
	var htmltext = "";
	//Перебираем массив изображений
	for (var i = 0; i < ImgList.length; i++) {
		if (ImgList[i] && ImgList[i] !== "") {
			htmltext += '<div class="imgBox">\
			<img src="..\\..\\'+ ImgList[i] + '">\
			<input type="checkbox" checked class="checkImgI" onclick="printImg()" value="..\\..\\'+ ImgList[i] + '">\
		</div>';
		}
	}
	//добавляем изображения на форму.
	if (htmltext !== "") {
		elm.html(htmltext);
	}
}

//TODO: 16/12/2019 yurii
//Добавление картинок для печати
//
function printImg() {
	var ImgChekElement = $('.checkImgI')
	var Cod1S = jsonPrev.PREV.TESTA.VAR["1C_COD"];
	var lblData = jsonPrev.PREV.TESTA.DATA;
	var htmltext = "";
	for (var n = 0; n < ImgChekElement.length; n++) {
		if (ImgChekElement[n].checked) {
			if (n == 0) {
				htmltext += '\
				<div class="wrapPageI">\
				    <div calss = "imgArg">\
				    <div class = "txtCentr">Приложение №3 к договору № '+ Cod1S + ' от ' + lblData + '</div>\
					<img higth="99%" src="'+ ImgChekElement[n].value + '">\
					</div>\
				</div>';
			} else {
				htmltext += '\
				<div class="wrapPageI">\
				    <div calss = "imgArg">\
					<img  src="'+ ImgChekElement[n].value + '">\
					</div>\
				</div>';
			}
		}
	}
	$('.imgPrint').html(htmltext);

}

//TODO: 16/12/2019 yurii
//Добавление информации по заказу
function printInfo(elm, PREV) {
	var lblNumber = PREV.TESTA.VAR["C_COD"];//PREV.TESTA.NUMERO;
	var lblData = PREV.TESTA.DATA;
	var lblDis = PREV.TESTA.VAR.FIODIS.split(',')[2];
	var lblPokup = PREV.TESTA.VAR.B_FIO;
	lblDis = dis_fio;
	lblNumber = c_cod;
	//console.log(c_cod);

	var htmltext = '<div><table class="titleBlock">\
				<tr><td width = "150px">Дата: '+ lblData + '</td> <td colspan="3">№ Заказа: ' + lblNumber + '</td></tr>\
				<tr><td>Дизайнер: </td><td colspan="3">'+ lblDis + '</td> </tr>\
				<tr><td colspan="4">Схема проекта (эскиз) мне понятна,мною проверена. С размерами,расположением,декоративными элементами и цветом комплектующих,в том числе:\
                  столешницы,фальш-панели,фасадов (и их фрезеровкой) ознакомлен и согласен</td></tr>\
				<tr><td>Покупатель: </td><td width = "300px">'+ lblPokup + '</td><td width = "100px">Подпись </td><td> </td></tr>\
			</table></div>';
	elm.html(htmltext);

}

//TODO: 16/12/2019 yurii
//
function printTable(elm, PREV) {
	var htmltext = '<div class="lblSpec">Перечень позиций</div>'
	htmltext += '<div><table class="tblInfo">\
	  <thead>\
	  <th>Описание</th>\
	  <th width="10%">Размеры</th>\
	  <th width="5%">Кол-во</th>\
	  </thead>';
	var OrdinaOld, Note;
	var flTop = 0;
	_.forEach(PREV.RIGHE, function (item) {
		if (item.COD !== "WORK") {
			var myName = findTag(item.VAR, 'MYNAME');
			var korpusname = findTag(item.VAR, 'korpusname');
			//console.log(item.ORDINAMENTO);
			if ((OrdinaOld !== item.ORDINAMENTO) && (item.ORDINAMENTO !== '9999') &&
				typeof (item.ORDINAMENTO) !== 'undefined') {

				if ((item.ORDINAMENTO !== '9999') && (Note && Note !== "")) {
					htmltext += '<tr>\
				  <td class="tdBold">Примечание: '+ Note + '</td>\
				  <td></td>\
				  <td></td>\
			     </tr>';
				}

				htmltext += '<tr>\
				  <td class="tdBold">Позиция '+ item.ORDINAMENTO + '</td>\
				  <td class="txtCentr">'+ item.L + 'x' + item.A + 'x' + item.P + '</td>\
				  <td class="txtCentr">'+ item.PZ + '</td>\
			     </tr>';

				if (item.ELMC == 1) {
					htmltext += '<tr>\
					  <td>'+ item.DES + ' ' + myName + korpusname + '</td>\
					  <td class="txtCentr">'+ item.L + 'x' + item.A + 'x' + item.P + '</td>\
					  <td class="txtCentr">'+ item.PZ + '</td>\
				    </tr>'
				}

				Note = item.NOTE;
			} else {
				if ((item.ORDINAMENTO === '9999' || typeof (item.ORDINAMENTO) === 'undefined') && flTop == 0) {
					htmltext += '<tr>\
				  <td class="tdBold">Длинномеры:</td>\
				  <td></td>\
				  <td></td>\
			    </tr>';
					flTop = 1;
				}
				htmltext += '<tr>\
				  <td>'+ item.DES + ' ' + myName + korpusname + '</td>\
				  <td class="txtCentr">'+ item.L + 'x' + item.A + 'x' + item.P + '</td>\
				  <td class="txtCentr">'+ item.PZ + '</td>\
			    </tr>'
			}
			OrdinaOld = item.ORDINAMENTO;
		}
	});
	if (PREV.TESTA.VAR.NOTE && PREV.TESTA.VAR.NOTE !== "") {
		htmltext += '<tr>\
			  <td class="tdBold">Примечание: '+ PREV.TESTA.VAR.NOTE + '</td>\
			  <td></td>\
			  <td></td>\
		     </tr>';
	}
	htmltext += '</table></div>';
	htmltext += '<div class="ftrSpec">Перечень позиций представляет собой пример сборки из комплектующих, не является спецификацией к договору, не отменяет и не изменяет Спецификацию.</div>'
	elm.html(htmltext);
}

//для сортировки чисел в массиве
function compareNumeric(a, b) {
	if (a > b) return 1;
	if (a == b) return 0;
	if (a < b) return -1;
}

//постобработка JsonPrev
function postJsonPrev() {
	//перенумерую
	let a = 1000;
	let b = 1;
	for (let key in jsonPrev.PREV.RIGHE) {
		jsonPrev.PREV.RIGHE['RIGA' + a] = jsonPrev.PREV.RIGHE[key];
		delete jsonPrev.PREV.RIGHE[key];
		//console.log(key);
		a++;
	}
	for (let key in jsonPrev.PREV.RIGHE) {
		jsonPrev.PREV.RIGHE['RIGA' + b] = jsonPrev.PREV.RIGHE[key];
		delete jsonPrev.PREV.RIGHE[key];
		b++;
	}
	//console.log(jsonPrev.PREV.RIGHE);

	//удаляю VarArr
	for (let key in jsonPrev.PREV.RIGHE) {
		delete jsonPrev.PREV.RIGHE[key].VarArr;
	}
	//console.log(jsonPrev.PREV.RIGHE);

	//заменяю #nbsp на пробелы
	for (let key in jsonPrev.PREV.RIGHE) {
		//jsonPrev.PREV.RIGHE[key];
		//console.log(key);
		for (let k in jsonPrev.PREV.RIGHE[key]) {
			if (k === "DETTPREZZO" || k === "VAR") {
				let str = jsonPrev.PREV.RIGHE[key][k];
				//console.log(k);
				str.replace(/\&nbsp\;/gi, ' ');
			}
		}
	}
}